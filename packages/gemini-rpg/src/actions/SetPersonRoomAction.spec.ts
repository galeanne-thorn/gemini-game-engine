import { EmptyGameState, ResultSuccess, setObjectState } from "@galeanne-thorn/gemini-core";
import "jest";
import {
    ACTION_SET_PERSON_ROOM, getPersonState, Person, Room,
    RpgPlugin, SetPersonRoom, setPersonRoom,
    TYPE_PERSON
} from "../";

const hero = new Person({ id: "hero", ui: "hero" });
const room = new Room({ id: "room", ui: "room description", exits: [] });

describe("SetPersonRoom", () => {
    it("creates correct action type", () => {
        const action = SetPersonRoom("hero", "room");
        expect(action.type).toBe(ACTION_SET_PERSON_ROOM);
        expect(action.personId).toBe("hero");
        expect(action.roomId).toBe("room");
    });
});

describe("setPersonRoom", () => {

    const catalogs = {
        actions: null, conditions: null, templates: null
    };
    const testState = setObjectState(EmptyGameState, hero.createState(catalogs, "hero"));

    it("sets person's room", () => {

        const result = setPersonRoom(testState, SetPersonRoom("hero", "room"));
        const personState = getPersonState(result, "hero");

        expect(personState).toBeDefined();
        expect(personState.room).toBe("room");
    });
});

describe("SetPersonRoomAction module", () => {
    it("registers the action in plugin", () => {
        expect(RpgPlugin.actions[ACTION_SET_PERSON_ROOM]).toBe(setPersonRoom);
    });
});
