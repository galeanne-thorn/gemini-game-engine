import "jest";

import { ActionCatalog, EmptyGameState, GameCatalogs, GameState } from "@galeanne-thorn/gemini-core";
import {
    ACTION_DROP_ITEM, dropItem, DropItem, getPlayerRoomState, getPlayerState, PersonState,
    RoomState, RpgPlugin, TYPE_PERSON, TYPE_ROOM
} from "../";

describe("DropItem", () => {
    it("creates correct action type", () => {
        const action = DropItem("itemId");
        expect(action.type).toBe(ACTION_DROP_ITEM);
        expect(action.itemId).toBe("itemId");
    });
});

describe("dropItem", () => {

    const testState: GameState = {
        ...EmptyGameState,
        player: "hero",
        objects: {
            person: {
                hero: {
                    id: "hero",
                    type: TYPE_PERSON,
                    templateId: "hero",
                    room: "room",
                    items: ["itemId"]
                } as PersonState
            },
            room: {
                room: {
                    id: "room",
                    type: TYPE_ROOM,
                    templateId: "room",
                    items: []
                } as RoomState
            }
        }
    };

    const action = DropItem("itemId");
    const actions = new ActionCatalog();
    actions.add(action.type, dropItem);
    const catalogs: GameCatalogs = {
        actions,
        conditions: null,
        templates: null
    };

    it("moves item from player to room", () => {
        const gs = dropItem(testState, DropItem("itemId"));
        const playerState = getPlayerState(gs);
        const roomState = getPlayerRoomState(gs);

        expect(playerState.items).toHaveLength(0);
        expect(roomState.items).toHaveLength(1);
        expect(roomState.items[0]).toBe("itemId");
    });
});

describe("DropItemAction module", () => {
    it("registers the action in plugin", () => {
        expect(RpgPlugin.actions[ACTION_DROP_ITEM]).toBe(dropItem);
    });
});
