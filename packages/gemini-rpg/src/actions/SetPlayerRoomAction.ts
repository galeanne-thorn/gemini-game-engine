/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Action, GameState, IGame, setObjectState } from "@galeanne-thorn/gemini-core";
import { getPlayerPersonId } from "../PlayerPerson";
import { Room, TYPE_ROOM } from "../Room";
import { ACTION_SET_PERSON_ROOM, setPersonRoom } from "./SetPersonRoomAction";

/**
 * Type of the SetPlayerPerson action
 */
export const ACTION_SET_PLAYER_ROOM = "SET_PLAYER_ROOM";

/**
 * SetPlayerRoom Action data
 */
export interface SetPlayerRoomAction extends Action {
    /**
     * Id of room
     */
    roomId: string;
}

/**
 * Creates the SetPlayerRoom action
 */
export function SetPlayerRoom(roomId: string): SetPlayerRoomAction {
    return {
        type: ACTION_SET_PLAYER_ROOM,
        roomId
    };
}

/**
 * Sets player's person
 *
 * @param gs - GameState
 * @param action - Action parameters
 *
 * @returns Updated GameState
 */
export function setPlayerRoom(gs: GameState, action: SetPlayerRoomAction, game: IGame): GameState {
    const template = game.templates.getTemplate<Room>(TYPE_ROOM, action.roomId);
    gs = setPersonRoom(gs, {
        type: ACTION_SET_PERSON_ROOM,
        personId: getPlayerPersonId(gs),
        roomId: action.roomId
    });
    gs = game.perform(gs, template.onEntry);
    return gs;
}
