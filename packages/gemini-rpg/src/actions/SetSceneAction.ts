import {
    Action, GameState, IGame,
    setActiveStage, setObjectState
} from "@galeanne-thorn/gemini-core";
import { Scene, TYPE_SCENE } from "../Scene";

/**
 * Type for Set Scene action
 */
export const ACTION_SET_SCENE = "SET_SCENE";

/**
 * SetSceneAction interface - it is simple set stage action
 */
export interface SetSceneAction extends Action {
    /**
     * Id of the scene to set
     */
    sceneId: string;
}

/**
 * Creates the scene action
 * @param sceneId - Id of the scene
 */
export function SetScene(sceneId: string): SetSceneAction {
    return { type: ACTION_SET_SCENE, sceneId };
}

/**
 * Performs the SetSceneAction
 *
 * @param gs - Game state
 * @param action - Action for setting the scene
 */
export function setScene(gs: GameState, action: SetSceneAction, game: IGame): GameState {
    const template = game.templates.getTemplate<Scene>(TYPE_SCENE, action.sceneId);
    const sceneState = template.createState(game, action.sceneId);
    gs = setObjectState(gs, sceneState);
    gs = setActiveStage(gs, TYPE_SCENE, action.sceneId);
    gs = game.perform(gs, template.onActivation);
    return gs;
}
