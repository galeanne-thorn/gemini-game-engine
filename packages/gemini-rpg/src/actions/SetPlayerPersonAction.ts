/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Action, GameState, setObjectState } from "@galeanne-thorn/gemini-core";
import { PLAYER_PERSON } from "../PlayerPerson";

/**
 * Type of the SetPlayerPerson action
 */
export const ACTION_SET_PLAYER_PERSON = "SET_PLAYER_PERSON";

/**
 * SetPlayerPerson Action data
 */
export interface SetPlayerPersonAction extends Action {
    /**
     * Id of the person that should represent the player
     */
    personId: string;
}

/**
 * Creates the SetPlayerPerson action
 */
export function SetPlayerPerson(personId: string): SetPlayerPersonAction {
    return {
        type: ACTION_SET_PLAYER_PERSON,
        personId
    };
}

/**
 * Sets player's person
 *
 * @param gs - GameState
 * @param action - Action parameters
 *
 * @returns Updated GameState
 */
export function setPlayerPerson(gs: GameState, action: SetPlayerPersonAction): GameState {
    return {
        ...gs,
        [PLAYER_PERSON]: action.personId
    };
}
