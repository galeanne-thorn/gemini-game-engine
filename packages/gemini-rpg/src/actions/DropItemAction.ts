/**
 * @galeanne-thorn/gemini-rpg
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Action, GameState } from "@galeanne-thorn/gemini-core";
import { moveBetweenInventories } from "../Inventory";
import { getPlayerRoomState, getPlayerState } from "../PlayerPerson";

/**
 * Type of Drop Item action
 */
export const ACTION_DROP_ITEM = "DROP_ITEM";

/**
 * Drop Item action interface.
 * Action dorps item with given Id from player and puts it to current room
 */
export interface DropItemAction extends Action {
    /**
     * Id of the item to Drop
     */
    itemId: string;
}

/**
 * Creates new Drop Item action
 * @param itemId - Id of the item
 *
 * @returns New DropItemAction
 */
export function DropItem(itemId: string): DropItemAction {
    return {
        type: ACTION_DROP_ITEM,
        itemId
    };
}

/**
 * Performs the DropItem action
 * @param gs - Game state
 * @param action - action to perform
 *
 * @returns New game state
 */
export function dropItem(gs: GameState, action: DropItemAction): GameState {
    const roomState = getPlayerRoomState(gs);
    const playerState = getPlayerState(gs);
    return moveBetweenInventories(gs, playerState, roomState, action.itemId);
}
