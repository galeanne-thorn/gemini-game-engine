import "jest";

import {
    Action, ActionCatalog, ConditionCatalog, EmptyGameState, getObjectState,
    IGame, ResultSuccess, SetVariable, setVariable, StatePointer, TemplateCatalog
} from "@galeanne-thorn/gemini-core";
import { last } from "lodash";
import { ACTION_SET_SCENE, RpgPlugin, Scene, SetScene, setScene, TYPE_SCENE } from "../";

describe("SetScene", () => {

    it("creates correct action type", () => {
        const action = SetScene("scene");
        expect(action.type).toBe(ACTION_SET_SCENE);
        expect(action.sceneId).toBe("scene");
    });
});

describe("setScene", () => {

    const SCENE_ID = "sceneId";

    const action = SetVariable("Called", true);

    const scene = new Scene({
        id: SCENE_ID,
        initialState: {
            data: "some state data"
        },
        onActivation: action,
        choices: [],
        ui: "scene"
    });

    const templates = new TemplateCatalog();
    const actions = new ActionCatalog();

    templates.add(scene);
    actions.add(action.type, setVariable);

    const igame: IGame = {
        actions,
        conditions: new ConditionCatalog(),
        templates,
        perform: (gs, a) => a ? igame.actions.getAction(a as Action)(gs, a as Action, igame) : gs
    };

    it("sets result to success", () => {
        const gs = setScene(EmptyGameState, SetScene(SCENE_ID), igame);
        expect(gs.lastActionResult).toEqual(ResultSuccess);
    });

    it("sets the scene as active stage", () => {
        const gs = setScene(EmptyGameState, SetScene(SCENE_ID), igame);
        const stage = last(gs.activeStages) as StatePointer;
        expect(stage.type).toBe(TYPE_SCENE);
        expect(stage.id).toBe(SCENE_ID);
    });

    it("creates scene state", () => {
        const gs = setScene(EmptyGameState, SetScene(SCENE_ID), igame);
        const expectedState = scene.createState(igame, scene.id);
        expect(getObjectState(gs, TYPE_SCENE, SCENE_ID)).toEqual(expectedState);
    });

    it("runs the onAction after scene is set", () => {
        const gs = setScene(EmptyGameState, SetScene(SCENE_ID), igame);
        expect(gs.variables.Called).toBe(true);
    });
});

describe("SetSceneAction module", () => {
    it("registers the action in plugin", () => {
        expect(RpgPlugin.actions[ACTION_SET_SCENE]).toBe(setScene);
    });
});
