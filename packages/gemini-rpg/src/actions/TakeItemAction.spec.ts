import "jest";

import { ActionCatalog, EmptyGameState, GameCatalogs, GameState } from "@galeanne-thorn/gemini-core";
import {
    ACTION_TAKE_ITEM, getPlayerRoomState, getPlayerState, PersonState, RoomState, RpgPlugin,
    takeItem, TakeItem, TYPE_PERSON, TYPE_ROOM
} from "../";

describe("TakeItem", () => {
    it("creates correct action type", () => {
        const action = TakeItem("itemId");
        expect(action.type).toBe(ACTION_TAKE_ITEM);
        expect(action.itemId).toBe("itemId");
    });
});

describe("takeItem", () => {

    const testState: GameState = {
        ...EmptyGameState,
        player: "hero",
        objects: {
            person: {
                hero: {
                    id: "hero",
                    type: TYPE_PERSON,
                    templateId: "hero",
                    room: "room",
                    items: []
                } as PersonState
            },
            room: {
                room: {
                    id: "room",
                    type: TYPE_ROOM,
                    templateId: "room",
                    items: ["itemId"]
                } as RoomState
            }
        }
    };

    const action = TakeItem("itemId");
    const actions = new ActionCatalog();
    actions.add(action.type, takeItem);
    const catalogs: GameCatalogs = {
        actions,
        conditions: null,
        templates: null
    };

    it("moves item from room to player inventory", () => {
        const gs = takeItem(testState, TakeItem("itemId"));
        const playerState = getPlayerState(gs);
        const roomState = getPlayerRoomState(gs);

        expect(playerState.items).toHaveLength(1);
        expect(playerState.items[0]).toBe("itemId");
        expect(roomState.items).toHaveLength(0);
    });
});

describe("TakeItemAction module", () => {
    it("registers the action in plugin", () => {
        expect(RpgPlugin.actions[ACTION_TAKE_ITEM]).toBe(takeItem);
    });
});
