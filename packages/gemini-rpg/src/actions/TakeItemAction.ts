/**
 * @galeanne-thorn/gemini-rpg
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Action, GameState } from "@galeanne-thorn/gemini-core";
import { moveBetweenInventories } from "../Inventory";
import { getPlayerRoomState, getPlayerState } from "../PlayerPerson";

/**
 * Type of Take Item action
 */
export const ACTION_TAKE_ITEM = "TAKE_ITEM";

/**
 * Take Item action interface.
 * Action takes item with given Id from player room and gives it to player
 */
export interface TakeItemAction extends Action {
    /**
     * Id of the item to take
     */
    itemId: string;
}

/**
 * Creates new Take Item action
 * @param itemId - Id of the item
 *
 * @returns New TakeItemAction
 */
export function TakeItem(itemId: string): TakeItemAction {
    return {
        type: ACTION_TAKE_ITEM,
        itemId
    };
}

/**
 * Performs the TakeItem action
 * @param gs - Game state
 * @param action - action to perform
 *
 * @returns New game state
 */
export function takeItem(gs: GameState, action: TakeItemAction): GameState {
    const roomState = getPlayerRoomState(gs);
    const playerState = getPlayerState(gs);
    return moveBetweenInventories(gs, roomState, playerState, action.itemId);
}
