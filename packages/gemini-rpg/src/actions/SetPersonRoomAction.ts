/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Action, GameState, setObjectState } from "@galeanne-thorn/gemini-core";
import { getPersonState } from "../Person";

/**
 * Type of the SetPersonRoom action
 */
export const ACTION_SET_PERSON_ROOM = "SET_PERSON_ROOM";

/**
 * SetPersonRoom Action data
 */
export interface SetPersonRoomAction extends Action {
    /**
     * Id of the person
     */
    personId: string;
    /**
     * Id of room
     */
    roomId: string;
}

/**
 * Creates the SetPersonRoom action
 */
export function SetPersonRoom(personId: string, roomId: string): SetPersonRoomAction {
    return {
        type: ACTION_SET_PERSON_ROOM,
        personId,
        roomId
    };
}

/**
 * Sets person's room
 *
 * @param gs - GameState
 * @param action - Action parameters
 *
 * @returns Updated GameState
 */
export function setPersonRoom(gs: GameState, action: SetPersonRoomAction): GameState {
    const person = getPersonState(gs, action.personId);
    const newPerson = {
        ...person,
        room: action.roomId
    };
    return setObjectState(gs, newPerson);
}
