import { EmptyGameState, ResultSuccess, setObjectState } from "@galeanne-thorn/gemini-core";
import "jest";
import {
    ACTION_SET_PLAYER_PERSON, getPlayerPersonId,
    RpgPlugin, SetPlayerPerson, setPlayerPerson
} from "../";

describe("SetPlayerPerson", () => {
    it("creates correct action type", () => {
        const action = SetPlayerPerson("hero");
        expect(action.type).toBe(ACTION_SET_PLAYER_PERSON);
        expect(action.personId).toBe("hero");
    });
});

describe("setPlayerPerson", () => {

    it("sets player's person Id to game state", () => {
        const newGs = setPlayerPerson(EmptyGameState, SetPlayerPerson("hero"));
        const result = getPlayerPersonId(newGs);
        expect(result).toBe("hero");
    });

    it("sets action result to success", () => {
        const newGs = setPlayerPerson(EmptyGameState, SetPlayerPerson("hero"));
        expect(newGs.lastActionResult).toBe(ResultSuccess);
    });

});

describe("SetPlayerPersonAction module", () => {
    it("registers the action in plugin", () => {
        expect(RpgPlugin.actions[ACTION_SET_PLAYER_PERSON]).toBe(setPlayerPerson);
    });
});
