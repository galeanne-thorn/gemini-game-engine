import {
    Action, ActionCatalog, ConditionCatalog, EmptyGameState, IGame, ResultSuccess,
    setObjectState, setVariable, SetVariable, TemplateCatalog
} from "@galeanne-thorn/gemini-core";
import "jest";
import {
    ACTION_SET_PLAYER_ROOM, getPlayerState, Person, Room, RpgPlugin,
    SetPlayerRoom, setPlayerRoom, TYPE_PERSON
} from "../";

const onEntryAction = SetVariable("Called", true);
const hero = new Person({ id: "hero", ui: "hero" });
const room = new Room({
    id: "room",
    ui: "room description",
    exits: [],
    onEntry: onEntryAction
});

const actions = new ActionCatalog();
const templates = new TemplateCatalog();

const igame: IGame = {
    actions,
    conditions: new ConditionCatalog(),
    templates,
    perform: (gs, a) => a ? igame.actions.getAction(a as Action)(gs, a as Action, igame) : gs
};

templates.add(hero);
templates.add(room);
actions.add(onEntryAction.type, setVariable);

describe("SetPlayerRoom", () => {

    it("creates correct action type", () => {
        const action = SetPlayerRoom("room");
        expect(action.type).toBe(ACTION_SET_PLAYER_ROOM);
        expect(action.roomId).toBe("room");
    });
});

describe("setPlayerRoom", () => {

    const testState = setObjectState(
        {
            ...EmptyGameState,
            player: "hero"
        },
        hero.createState(igame, "hero"));

    it("sets player's person room", () => {

        const gs = setObjectState(testState, hero.createState(igame, "hero"));
        const result = setPlayerRoom(gs, SetPlayerRoom("room"), igame);
        const personState = getPlayerState(result);

        expect(personState).toBeDefined();
        expect(personState.room).toBe("room");
    });

    it("runs the onEnter after player enters room", () => {
        const gs = setPlayerRoom(testState, SetPlayerRoom(room.id), igame);
        expect(gs.variables.Called).toBe(true);
    });

});

describe("SetPlayerRoomAction module", () => {
    it("registers the action in plugin", () => {
        expect(RpgPlugin.actions[ACTION_SET_PLAYER_ROOM]).toBe(setPlayerRoom);
    });
});
