import "jest";

import { EmptyGameState, GameCatalogs, getObjectState, TemplateCatalog } from "@galeanne-thorn/gemini-core";
import {
    addToInventory, getItemState, initInventory,
    InventoryState, Item, moveBetweenInventories, removeFromInventory, TYPE_ITEM
} from "./";

const itemTemplateId = "itemType1";
const itemTemplate = new Item({ id: itemTemplateId, ui: "Item TYPE 1" });

const templates = new TemplateCatalog();
templates.add(itemTemplate);

const catalogs: GameCatalogs = {
    actions: null,
    conditions: null,
    templates
};

describe("initInventory", () => {
    it("keeps inventory if items are not specified", () => {
        const inventory: InventoryState = {
            type: "INV",
            id: "inv1",
            templateId: "INV",
            items: []
        };
        const gs = initInventory(EmptyGameState, catalogs, inventory, []);
        const newInventory = getObjectState<InventoryState>(gs, "INV", "inv1");
        expect(newInventory).toBe(inventory);
    });

    it("creates item state and adds it to the inventory", () => {
        const inventory: InventoryState = {
            type: "INV",
            id: "inv1",
            templateId: "INV",
            items: []
        };
        const gs = initInventory(EmptyGameState, catalogs, inventory, [itemTemplateId]);
        const newInventory = getObjectState<InventoryState>(gs, "INV", "inv1");

        expect(newInventory).toBeDefined();
        expect(newInventory.items.length).toBe(1);

        const item = getItemState(gs, newInventory.items[0]);
        expect(item).toBeDefined();
        expect(item.templateId).toBe(itemTemplateId);
        expect(item.count).toBe(1);
    });

    it("creates item state with count and adds it to the inventory", () => {
        const inventory: InventoryState = {
            type: "INV",
            id: "inv1",
            templateId: "INV",
            items: []
        };
        const gs = initInventory(EmptyGameState, catalogs, inventory, [[itemTemplateId, 5]]);
        const newInventory = getObjectState<InventoryState>(gs, "INV", "inv1");

        expect(newInventory).toBeDefined();
        expect(newInventory.items.length).toBe(1);

        const item = getItemState(gs, newInventory.items[0]);
        expect(item).toBeDefined();
        expect(item.templateId).toBe(itemTemplateId);
        expect(item.count).toBe(5);
    });
});

describe("addToInventory", () => {
    it("adds item Id to the inventory", () => {
        const inventory: InventoryState = {
            type: "INV",
            id: "inv1",
            templateId: "INV",
            items: []
        };
        const gs = addToInventory(EmptyGameState, inventory, "X");
        const newInventory = getObjectState<InventoryState>(gs, "INV", "inv1");
        expect(newInventory.items[0]).toBe("X");
    });
});

describe("removeFromInventory", () => {
    it("removes item state from the inventory", () => {
        const inventory: InventoryState = {
            type: "INV",
            id: "inv1",
            templateId: "INV",
            items: ["XXX"]
        };
        const gs = removeFromInventory(EmptyGameState, inventory, "XXX");
        const newInventory = getObjectState<InventoryState>(gs, "INV", "inv1");
        expect(newInventory.items.length).toBe(0);
    });
});

describe("moveBetweenInventories", () => {
    it("removes item from the `from` inventory and adds it to the `to` inventory", () => {
        const inventoryFrom: InventoryState = {
            type: "INV",
            id: "invFrom",
            templateId: "INV",
            items: ["XXX"]
        };
        const inventoryTo: InventoryState = {
            type: "INV",
            id: "invTo",
            templateId: "INV",
            items: []
        };
        const gs = moveBetweenInventories(EmptyGameState, inventoryFrom, inventoryTo, "XXX");

        const fromInventory = getObjectState<InventoryState>(gs, "INV", "invFrom");
        const toInventory = getObjectState<InventoryState>(gs, "INV", "invTo");

        expect(fromInventory.items.length).toBe(0);
        expect(toInventory.items.length).toBe(1);
        expect(toInventory.items[0]).toBe("XXX");
    });

});
