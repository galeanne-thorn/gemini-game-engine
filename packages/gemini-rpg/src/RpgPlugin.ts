/**
 * @galeanne-thorn/gemini-rpg
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { GamePlugin, setObjectState } from "@galeanne-thorn/gemini-core";
import {
    ACTION_DROP_ITEM, ACTION_SET_PERSON_ROOM, ACTION_SET_PLAYER_PERSON, ACTION_SET_PLAYER_ROOM,
    ACTION_SET_SCENE, ACTION_TAKE_ITEM,
    dropItem, initInventory, setPersonRoom, setPlayerPerson, setPlayerRoom, setScene, takeItem
} from "./";
import { COND_HAS_SKILL, hasSkill } from "./conditions/HasSkillCondition";
import { Item, TYPE_ITEM } from "./Item";
import { Person, TYPE_PERSON, } from "./Person";
import { Room, TYPE_ROOM } from "./Room";
import { Scene, TYPE_SCENE } from "./Scene";
import { Skill, TYPE_SKILL } from "./skills/Skill";

export const RpgPlugin: GamePlugin = {

    id: "rpg",
    title: "Gemini Game Default RPG Module",
    author: "Galeanne Thorn",

    templateTypes: {
        [TYPE_ITEM]: Item,
        [TYPE_PERSON]: Person,
        [TYPE_ROOM]: Room,
        [TYPE_SCENE]: Scene,
        [TYPE_SKILL]: Skill
    },

    actions: {
        [ACTION_DROP_ITEM]: dropItem,
        [ACTION_SET_PERSON_ROOM]: setPersonRoom,
        [ACTION_SET_PLAYER_PERSON]: setPlayerPerson,
        [ACTION_SET_PLAYER_ROOM]: setPlayerRoom,
        [ACTION_SET_SCENE]: setScene,
        [ACTION_TAKE_ITEM]: takeItem
    },

    conditions: {
        [COND_HAS_SKILL]: hasSkill
    },

    /**
     * Module initializer
     */
    initializer: (gs, game) => {
        // Initialize persons
        gs.objects[TYPE_PERSON] = {};
        game.templates
            .getAllTemplates<Person>(TYPE_PERSON)
            .forEach(t => {
                const personState = t.createState(game, t.id);
                gs = initInventory(gs, game, personState, t.items);
            });
        // Initialize rooms
        game.templates
            .getAllTemplates<Room>(TYPE_ROOM)
            .forEach(t => {
                const roomState = t.createState(game, t.id);
                gs = initInventory(gs, game, roomState, t.items);
            });

        return gs;
    }
};
