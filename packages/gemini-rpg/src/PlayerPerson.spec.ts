import "jest";

import { EmptyGameState, Game, GameState } from "@galeanne-thorn/gemini-core";
import {
    getPlayer, getPlayerPersonId, getPlayerRoomState, getPlayerState,
    Person, PersonState, TYPE_PERSON, TYPE_ROOM
} from "./";
import { RpgPlugin } from "./RpgPlugin";

// GameState used for tests
const testState: GameState = {
    ...EmptyGameState,
    player: "hero",
    objects: {
        person: {
            hero: {
                id: "hero",
                type: TYPE_PERSON,
                templateId: "hero",
                room: "room"
            } as PersonState
        },
        room: {
            room: {
                id: "room",
                type: TYPE_ROOM,
                templateId: "room"
            }
        }
    }
};

describe("getPlayerPersonId", () => {
    it("gets correct person Id from game state", () => {
        const id = getPlayerPersonId(testState);
        expect(id).toBe("hero");
    });
});

describe("getPlayer", () => {

    const game = new Game([{
        id: "test",
        templateTypes: {
            person: Person
        },
        templates: {
            person: [{ id: "hero", ui: "hero" }]
        }
    }]);
    game.start(testState);

    it("gets correct player template", () => {
        const template = getPlayer(game);
        expect(template.type).toBe(TYPE_PERSON);
        expect(template.id).toBe("hero");
    });
});

describe("getPlayerState", () => {
    it("gets correct state", () => {
        const state = getPlayerState(testState);
        expect(state.id).toBe("hero");
        expect(state.type).toBe(TYPE_PERSON);
        expect(state.templateId).toBe("hero");
    });
});

describe("getPlayerRoomState", () => {
    it("gets state of room the player is in", () => {
        const state = getPlayerRoomState(testState);
        expect(state.id).toBe("room");
        expect(state.type).toBe(TYPE_ROOM);
        expect(state.templateId).toBe("room");
    });
});
