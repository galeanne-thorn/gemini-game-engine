/**
 * @galeanne-thorn/gemini-rpg
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Condition, GameCatalogs, GameState } from "@galeanne-thorn/gemini-core";
import { getPersonState } from "../Person";
import { defaultLevel, Difficulty, experienceToLevel, Level } from "../skills/Levels";
import { Skill, TYPE_SKILL } from "../skills/Skill";

/**
 * HasSkill condition ID
 */
export const COND_HAS_SKILL = "HAS_SKILL";

/**
 * Parameters for the HAS_SKILL condition
 */
export interface HasSkillCondition extends Condition {
    /**
     * Id of the person to check
     */
    person: string;
    /**
     * Id of the skill to check
     */
    skill: string;
    /**
     * Required level of the skill
     */
    level: Level;
}

/**
 * Creates new HasSkillCondition condition
 * The condition succeeds if give person has skill at least at given level
 *
 * @param person - Id of the person to test
 * @param skill  - Id of the skill to test
 * @param level  - [optional] Required level of the skill
 */
export function HasSkill(person: string, skill: string, level: Level = Level.average): HasSkillCondition {
    return {
        type: COND_HAS_SKILL,
        person,
        skill,
        level
    };
}

/**
 * Evaluates the HasSkillCondition
 */
export function hasSkill(gs: GameState, condition: HasSkillCondition, catalogs: GameCatalogs): boolean {
    const skill = catalogs.templates.getTemplate<Skill>(TYPE_SKILL, condition.skill);
    const person = getPersonState(gs, condition.person);
    const skillState = person.skills[condition.skill];
    const level: Level = (skillState)
        ? experienceToLevel(skillState.experience, skill.difficulty)
        : defaultLevel(skill.difficulty);
    return level >= condition.level;
}
