/**
 * @galeanne-thorn/gemini-rpg
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { EmptyGameState, GameCatalogs, GameState, TemplateCatalog } from "@galeanne-thorn/gemini-core";
import "jest";
import {
    COND_HAS_SKILL, defaultLevel, Difficulty, hasSkill, HasSkill,
    HasSkillCondition, Level, Person, RpgPlugin, Skill, TYPE_PERSON
} from "../";

describe("HasSkill", () => {
    it("Creates correct condition", () => {
        const result = HasSkill("hero", "skill", Level.epic);
        expect(result.person).toEqual("hero");
        expect(result.skill).toEqual("skill");
        expect(result.level).toBe(Level.epic);
        expect(result.type).toBe(COND_HAS_SKILL);
    });
});

describe("hasSkill", () => {

    const SKILL_TEST = "skill";
    const templates = new TemplateCatalog();
    const skill = templates.add(new Skill({ id: SKILL_TEST, difficulty: Difficulty.medium }));

    const personNoSkill = templates.add(
        new Person({ id: "noSkill" }));
    const personAverage = templates.add(
        new Person({ id: "exact", skills: { [SKILL_TEST]: Level.average } }));

    const catalogs: GameCatalogs = {
        templates,
        actions: null,
        conditions: null
    };

    const gs: GameState = {
        ...EmptyGameState,
        objects: {
            [TYPE_PERSON]: {
                [personNoSkill.id]: personNoSkill.createState(catalogs, personNoSkill.id),
                [personAverage.id]: personAverage.createState(catalogs, personAverage.id)
            }
        }
    };

    it("returns true if person does not have skill but test is for default level", () => {
        const level = defaultLevel(skill.difficulty);
        expect(
            hasSkill(gs, HasSkill(personNoSkill.id, SKILL_TEST, level), catalogs)
        ).toBe(true);
    });

    const testCases: Array<[Level, boolean]> = [
        [Level.abysmall, true],
        [Level.terrible, true],
        [Level.poor, true],
        [Level.mediocre, true],
        [Level.average, true],
        [Level.fair, false],
        [Level.good, false],
        [Level.great, false],
        [Level.superb, false],
        [Level.epic, false],
        [Level.legendary, false]
    ];

    testCases.forEach(c => it(`returns ${c[1]} for Level ${Level[c[0]]} for person with average skill`, () => {
        expect(
            hasSkill(gs, HasSkill(personAverage.id, SKILL_TEST, c[0]), catalogs)
        ).toBe(c[1]);
    }));
});

describe("module", () => {
    it("registers the condition in module", () => {
        expect(RpgPlugin.conditions[COND_HAS_SKILL]).toBe(hasSkill);
    });
});
