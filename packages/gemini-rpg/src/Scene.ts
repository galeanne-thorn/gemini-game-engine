/**
 * Module Gemini RPG Subsystem
 *
 * Copyright © 2017 Galeanne Thorn
 * License: MIT
 */

import {
    ActionGroup, ChoiceGroupDefinitionItem, ChoicesProvider, GameState,
    Template, TemplateDefinition
} from "@galeanne-thorn/gemini-core";

/**
 * Scene type ID
 */
export const TYPE_SCENE = "scene";

/**
 * Scene definition
 */
export interface SceneDefinition extends TemplateDefinition, ChoicesProvider {
    /**
     * Optional actions to run when scene is set
     */
    onActivation?: ActionGroup;
}

/**
 * Scene template - it is just a template
 */
export interface Scene extends Template, SceneDefinition {
}

/**
 * Scene template class
 */
export class Scene extends Template {

    /**
     * Creates new scene
     * @param definition - Definition of the scene
     */
    public constructor(definition: SceneDefinition) {
        super(TYPE_SCENE, definition);
    }
}
