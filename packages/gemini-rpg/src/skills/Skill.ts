/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { ChildState, Template, TemplateDefinition } from "@galeanne-thorn/gemini-core";
import { Difficulty } from "./Levels";

/**
 * Skill type Id
 */
export const TYPE_SKILL = "skill";

/**
 * Skill payload
 */
export interface SkillPayload {
    /**
     * Experience in the skill
     */
    experience: number;
}

/**
 * Skill state
 */
export type SkillState = ChildState<SkillPayload>;

/**
 * Skill definition
 */
export interface SkillDefinition extends TemplateDefinition<SkillPayload> {
    /**
     * Skill difficulty
     */
    difficulty: Difficulty;
    /**
     * Optional Skill group
     * (think of "attribute", "sport", "social", etc.)
     */
    group?: string;
}

/**
 * Skill template interface
 */
export interface Skill extends Template<SkillPayload>, SkillDefinition {
}

/**
 * Skill template
 */
export class Skill extends Template<SkillPayload> {

    /**
     * Creates new Skill template
     * @param definition - Skill definition
     */
    public constructor(definition: SkillDefinition) {
        super(TYPE_SKILL, definition);
    }
}
