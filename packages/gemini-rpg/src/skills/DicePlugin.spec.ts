import "jest";

import { Action, ActionGroup, EmptyGameState } from "@galeanne-thorn/gemini-core";
import { ActionWithDice, DicePlugin, isActionWithDice } from "../";

describe("DicePlugin", () => {

    it("keeps action without dice request intact", () => {
        let newAction: ActionGroup;
        const reducer = DicePlugin.extender((gs, a, g) => { newAction = a; return gs; });

        const action: Action = { type: "a" };
        reducer(EmptyGameState, action, null);

        expect(newAction).toBe(action);
    });

    it("updates action with thrown dice", () => {
        let newAction: ActionGroup;
        const reducer = DicePlugin.extender((gs, a, g) => { newAction = a; return gs; });

        const action: ActionWithDice = {
            type: "a",
            dice: "1d1"
        };
        reducer(EmptyGameState, action, null);

        expect(isActionWithDice(newAction)).toBe(true);
        if (isActionWithDice(newAction)) {
            expect(newAction.thrownDice).toBe(1);
        }
    });

    it("updates action with multiple thrown dice if required", () => {
        let newAction: ActionGroup;
        const reducer = DicePlugin.extender((gs, a, g) => { newAction = a; return gs; });

        const action: ActionWithDice = {
            type: "a",
            dice: ["1d1", "2d1", "3d1"]
        };
        reducer(EmptyGameState, action, null);

        expect(isActionWithDice(newAction)).toBe(true);
        if (isActionWithDice(newAction)) {
            expect(newAction.thrownDice).toEqual([1, 2, 3]);
        }
    });

    it("updates action group with thrown dice", () => {
        let newAction: ActionGroup;
        const reducer = DicePlugin.extender((gs, a, g) => { newAction = a; return gs; });

        const action: Action[] = [
            {
                type: "a",
                dice: "1d1"
            } as ActionWithDice,
            {
                type: "b"
            },
            {
                type: "c",
                dice: "2d1"
            }
        ];
        reducer(EmptyGameState, action, null);

        expect(newAction).toHaveLength(3);
        expect(isActionWithDice(newAction[0])).toBe(true);
        if (isActionWithDice(newAction[0])) {
            expect(newAction[0].thrownDice).toBe(1);
        }

        expect(isActionWithDice(newAction[1])).toBe(false);
        expect(newAction[1]).toBe(action[1]);

        expect(isActionWithDice(newAction[2])).toBe(true);
        if (isActionWithDice(newAction[2])) {
            expect(newAction[2].thrownDice).toBe(2);
        }
    });
});
