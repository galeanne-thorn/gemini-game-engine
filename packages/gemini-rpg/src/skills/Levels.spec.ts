import "jest";

import { Difficulty, experienceToLevel, Level, levelToExperience, defaultLevel } from "../";

const testData = [
    [0, 0],
    [1, 1],
    [2, 2],
    [3, 4],
    [5, 8],
    [6, 16],
    [7, 32],
    [8, 64],
    [9, 128],
    [10, 256]
];

describe("experienceToLevel", () => {
    it("calculates level correctly based for each difficulty", () => {
        for (const d in Difficulty) {
            if (typeof d === "string") continue;
            testData.forEach(element => {
                const level = element[0];
                const experience = element[1] * d;
                const result = experienceToLevel(experience, d);
                expect(result).toEqual(level);
            });
        }
    });
});

describe("levelToExperience", () => {
    it("calculates experience correctly for each difficulty", () => {
        for (const d in Difficulty) {
            if (typeof d === "string") continue;
            testData.forEach(element => {
                const level = element[0];
                const experience = element[1] * d;
                const result = levelToExperience(level, d);
                expect(result).toEqual(experience);
            });
        }
    });
});

describe("defaultLevel", () => {
    it("returns correct value for each difficutly", () => {
        const data = [
            [Difficulty.easy, Level.mediocre],
            [Difficulty.medium, Level.poor],
            [Difficulty.hard, Level.terrible],
            [Difficulty.extreme, Level.abysmall]
        ];
        data.forEach(testCase => {
            const level = defaultLevel(testCase[0]);
            expect(level).toBe(testCase[1]);
        });
    });
});
