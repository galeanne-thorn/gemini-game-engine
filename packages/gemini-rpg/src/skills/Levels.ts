/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

/**
 * Skill difficulties
 */
export enum Difficulty {
    easy = 1,
    medium = 2,
    hard = 4,
    extreme = 8
}

/**
 * Skill and attribute levels
 */
export enum Level {
    abysmall = 0,
    terrible = 1,
    poor = 2,
    mediocre = 3,
    average = 4,
    fair = 5,
    good = 6,
    great = 7,
    superb = 8,
    epic = 9,
    legendary = 10
}

/**
 * Converts experience to level
 */
export function experienceToLevel(experience: number, difficulty: number | Difficulty): number {
    const base = Math.floor(experience / difficulty);
    return Math.floor(Math.log(base) / Math.LN2);
}

/**
 * Calculates minimal experience needed for given level
 */
export function levelToExperience(level: number, difficulty: number | Difficulty): number {
    const base = Math.round(Math.exp(level * Math.LN2));
    return base * difficulty;
}

/**
 * Gets default skill level depending on skill difficulty.
 * Default level is used when the person does not know the skill
 *
 * @param difficulty - Difficulkty to get the default level for
 * @returns - Default skill level
 */
export function defaultLevel(difficulty: number | Difficulty): Level {
    switch (difficulty) {
        case Difficulty.easy:
            return Level.mediocre;
        case Difficulty.medium:
            return Level.poor;
        case Difficulty.hard:
            return Level.terrible;
        case Difficulty.extreme:
            return Level.abysmall;
        default:
            throw new Error(`Unknown difficulty ${difficulty}`);
    }
}
