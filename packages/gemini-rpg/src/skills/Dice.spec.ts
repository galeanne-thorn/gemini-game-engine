import "jest";

import { Dice, parseDice, throwDice } from "../";

const TEST_COUNT = 1000;

describe("parseDice", () => {
    [
        { text: "1d6", expect: { sides: 6, count: 1 } },
        { text: "4dF", expect: { sides: "F", count: 4 } },
        { text: "12d34", expect: { sides: 34, count: 12 } }
    ]
        .forEach(test =>
            it(`parses ${test.text} correctly`, () => {
                expect(parseDice(test.text)).toEqual(test.expect);
            }));

    ["1", "F", "1d", "dF", "x", "", null]
        .forEach(text =>
            it(`throws when text (${text}) cannot be parsed`, () => {
                expect(() => parseDice(text))
                    .toThrowError(`Wrong dice description "${text}". Use "{count}d{sides}"`);
            })
        );
});

describe("throwDice", () => {

    it("parses textual dice description", () => {
        const result = throwDice("4dF");
        expect(result).toBeDefined();
    });

    it("returns expected values for single fudge/fate dice", () => {
        const dice: Dice = {
            sides: "F",
            count: 1
        };
        // Note this is fragile test
        for (let i = 0; i < TEST_COUNT; ++i) {
            const result = throwDice(dice);
            expect(result).toBeGreaterThanOrEqual(-1);
            expect(result).toBeLessThanOrEqual(1);
        }
    });

    [2, 6, 8, 10, 20]
        .forEach(sides =>
            it(`returns expected values for single ${sides} - sided dice`, () => {
                const dice: Dice = {
                    sides,
                    count: 1
                };
                // Note this is fragile test
                for (let i = 0; i < TEST_COUNT; ++i) {
                    const result = throwDice(dice);
                    expect(result).toBeGreaterThanOrEqual(1);
                    expect(result).toBeLessThanOrEqual(sides);
                }
            })
        );

    it("respects the count of throws", () => {
        // For this test we use the trick of one-sided dice
        const dice: Dice = {
            sides: 1,
            count: 10
        };
        expect(throwDice(dice)).toBe(10);
    });

    it("can throw multiple dice", () => {
        const dice: Dice[] = [
            { sides: "F", count: 1 },
            { sides: 2, count: 1 },
            { sides: 100, count: 1 }
        ];
        for (let i = 0; i < TEST_COUNT; ++i) {
            const result = throwDice(dice);
            expect(result).toHaveLength(dice.length);
            expect(-1 <= result[0] && result[0] <= 1).toBe(true);
            expect(1 <= result[1] && result[1] <= 2).toBe(true);
            expect(1 <= result[2] && result[2] <= 100).toBe(true);
        }
    });
});
