/**
 * @galeanne-thorn/gemini-rpg
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Action, ActionGroup, ActionPerformer, GamePlugin } from "@galeanne-thorn/gemini-core";
import { hasIn, isArray, random } from "lodash";
import { Dice, DiceDefinition, throwDice } from "./Dice";

/**
 * Action that needs die or dice throw
 */
export interface ActionWithDice extends Action {
    /**
     * Defines what dice to throw
     */
    dice: DiceDefinition | DiceDefinition[];
    /**
     * Results of the throws
     */
    thrownDice?: number | number[];
}

/**
 * Tests if object is ActionWithDice
 *
 * @param a - Object to test
 * @returns True if object is ActionWithDice
 */
export function isActionWithDice(a: object): a is ActionWithDice {
    return hasIn(a, "dice");
}

function updateOneAction<T extends Action>(a: T): T {
    if (isActionWithDice(a)) {
        // Due to TS bug cannot use spread here
        return Object.assign(
            {},
            a,
            {
                thrownDice: isArray(a.dice)
                    ? throwDice(a.dice)
                    : throwDice(a.dice)
            }
        ) as T;
    }
    return a;
}

function updateAction(a: ActionGroup): ActionGroup {
    if (isArray(a)) {
        return a.map(x => updateOneAction(x));
    }
    else {
        return updateOneAction(a);
    }
}

/**
 * Dice plugin - modifies action by adding "random" dice throws
 */
export const DicePlugin: GamePlugin = {

    /** Id of the module */
    id: "dice",
    /** Title of the module */
    title: "Gemini Game Dice Plugin",
    /** Author of the module */
    author: "Galeanne Thorn",

    /**
     * Extends action performance
     */
    extender: (original: ActionPerformer): ActionPerformer =>
        (gs, a, g) => {
            const newAction = updateAction(a);
            return original(gs, newAction, g);
        }
};
