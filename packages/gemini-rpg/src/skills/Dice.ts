/**
 * @galeanne-thorn/gemini-rpg
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { isArray, random } from "lodash";

/**
 * Type of Dice
 *  F = fudge/fate dice
 *  number = number of sides
 */
export type DiceSides = "F" | number;

/**
 * Die/Dice throw definition
 */
export interface Dice {
    /**
     * Type of Dice
     *  F = fudge/fate dice
     *  number = number of sides
     */
    sides: DiceSides;
    /**
     * Number of throws
     */
    count: number;
}

/**
 * Dice definition
 */
export type DiceDefinition = string | Dice;

const DiceRegex = /^(\d+)d(F|\d+)$/;

/**
 * Parses string representation of the dice to Dice object
 * @param diceDescription - Dice description using the "CdS" notation (e.g. 1d6, 4dF, etc.)
 * @returns New Dice object
 */
export function parseDice(diceDescription: string): Dice {
    const parts = DiceRegex.exec(diceDescription);
    if (parts) {
        return {
            sides: parts[2] === "F" ? "F" : Number.parseInt(parts[2], 10),
            count: Number.parseInt(parts[1], 10)
        };
    }
    throw new Error(`Wrong dice description "${diceDescription}". Use "{count}d{sides}"`);
}

/**
 * Throws dice and returns the results
 *
 * @param dice - Dice to throw
 * @returns Thrown result
 */
export function throwDice(dice: DiceDefinition): number;
export function throwDice(dice: DiceDefinition[]): number[];
export function throwDice(dice: DiceDefinition | DiceDefinition[]): number | number[] {

    function throwOneDice(d: Dice): number {
        const { lower, higher } = (d.sides === "F")
            ? { lower: -1, higher: +1 }
            : { lower: 1, higher: d.sides };

        let result = 0;
        for (let i = 0; i < d.count; ++i) {
            result += random(lower, higher);
        }
        return result;
    }

    if (isArray(dice)) {
        return dice.map(d => throwDice(d));
    }
    else if (typeof (dice) === "string") {
        return throwOneDice(parseDice(dice));
    }
    else {
        return throwOneDice(dice);
    }
}
