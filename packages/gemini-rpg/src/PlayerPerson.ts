/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Game, GameState } from "@galeanne-thorn/gemini-core";
import { getPersonState, Person, PersonState, TYPE_PERSON } from "./Person";
import { getRoomState, RoomState } from "./Room";

/**
 * Name of the variable that points to player person
 */
export const PLAYER_PERSON = "player";

/**
 * Gets ID of the current player person
 *
 * @param gs - GameState
 * @returns ID of the current player person
 */
export function getPlayerPersonId(gs: GameState): string {
    return gs[PLAYER_PERSON];
}

/**
 * Gets state of the person that represents player
 *
 * @param gs - GameState
 * @returns State of the player person
 */
export function getPlayerState(gs: GameState): PersonState {
    return getPersonState(gs, gs[PLAYER_PERSON]);
}

/**
 * Gets template of player person
 *
 * @param gs - GameState
 * @returns Template of player person
 */
export function getPlayer(g: Game): Person {
    return g.game.templates.getTemplate<Person>(TYPE_PERSON, g.state[PLAYER_PERSON]);
}

/**
 * Gets state of the room the player is in
 * @param gs - GameState
 * @returns State of the room the player is in
 */
export function getPlayerRoomState(gs: GameState): RoomState {
    const playerState = getPlayerState(gs);
    return getRoomState(gs, playerState.room);
}
