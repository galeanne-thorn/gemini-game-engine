/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import {
    ChoiceGroupDefinitionItem, GameCatalogs, GameState, ObjectState,
    ObjectStateGetter, Template, TemplateDefinition
} from "@galeanne-thorn/gemini-core";

/**
 * Item type Id
 */
export const TYPE_ITEM = "item";

/**
 * Item state payload
 */
export interface ItemPayload {
    /**
     * Number of items in stack
     */
    count: number;
}

/**
 * Item state
 */
export type ItemState = ObjectState<ItemPayload>;

/**
 * Item definition
 */
export interface ItemDefinition extends TemplateDefinition<ItemPayload> {
    /**
     * Item UI data
     */
    ui: any;
    /**
     * Optional choices
     */
    choices?: ChoiceGroupDefinitionItem[];
}

/**
 * Item interface
 */
export interface Item extends Template<ItemPayload>, ItemDefinition {
}

/**
 * Item template
 */
export class Item extends Template<ItemPayload> {

    /**
     * Gets list of choices for this item
     */
    public choices: ChoiceGroupDefinitionItem[] = [];

    /**
     * UI field.
     */
    // Note the explicit declaration is needed as Template and ItemDefinition ui properties differ in optionality
    public ui: any;

    /**
     * Creates new Item
     *
     * @param id - Id of the Item
     * @param initialItemState - Initial Item state
     */
    constructor(definition: ItemDefinition) {
        super(TYPE_ITEM, definition);
    }

    /**
     * Creates new ItemState with given id
     *
     * @param catalogs - game catalogs to use
     * @param id - id of the PersonState to create. If not supplied, UUID type 4 is used
     * @returns new PersonState based on this template, with given id
     */
    public createState(catalogs: GameCatalogs, id: string): ItemState {
        return {
            ...super.createState(catalogs, id),
            count: 1
        };
    }

}

/**
 * Item state getter
 */
export const getItemState = ObjectStateGetter<ItemState>(TYPE_ITEM);
