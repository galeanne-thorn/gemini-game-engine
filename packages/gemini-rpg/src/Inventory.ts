/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { GameCatalogs, GameState, ObjectState, setObjectState } from "@galeanne-thorn/gemini-core";
import { Item, TYPE_ITEM } from "./Item";

/**
 * Inventory state payload
 */
export interface InventoryPayload {
    /**
     * Items in the inventory are represented as
     * array of item state Ids
     */
    items: string[];
}

/**
 * Inventory state
 */
export type InventoryState = ObjectState<InventoryPayload>;

/**
 * Inventory definition
 *
 * Either string representing item templateId, or tuple representing item templateId and count
 */
export type InventoryDefinition = string | [string, number];

/**
 * Initializes inventory state from definition.
 * The inventory state is intended to be part of other game object state, like person or room.
 * This method creates state for each item in items and adds it to the inventory
 *
 * @param gs - GameState
 * @param catalogs - Game catalogs to use. It should contain all needed item templates
 * @param items - Items to add to the inventory. Either item templateId (adds 1) or tuple of [templateId, count]
 *
 * @returns New game state with the initialized inventory and item states
 */
export function initInventory(
    gs: GameState,
    catalogs: GameCatalogs,
    inventory: InventoryState,
    items: InventoryDefinition[] = []
): GameState {

    items
        .map<[string, number]>(definition => typeof (definition) === "string" ? [definition, 1] : definition)
        .forEach(definition => {
            const itemTemplateId = definition[0];
            const count = definition[1];

            const itemTemplate = catalogs.templates.getTemplate(TYPE_ITEM, itemTemplateId);
            const itemState = itemTemplate.createState(catalogs);
            itemState.count = count;
            // TODO: Optimize adding items to state
            gs = setObjectState(gs, itemState);
            inventory.items.push(itemState.id);
        });

    return setObjectState(gs, inventory);
}

/**
 * Adds item to inventory
 *
 * @param gs - Game state
 * @param inventory - Inventory to add the item to
 * @param itemId - Id of the item state to add
 *
 * @returns New game state
 */
export function addToInventory(gs: GameState, inventory: InventoryState, itemId: string): GameState {
    // If Item is already in inventory, do nothing
    if (inventory.items.indexOf(itemId) >= 0) return gs;
    // Else add it to the inventory
    const newInventory = {
        ...inventory,
        items: [...inventory.items, itemId]
    };
    return setObjectState(gs, newInventory);
}

/**
 * Removes item from inventory
 *
 * @param gs - Game state
 * @param inventory - Inventory to add the item to
 * @param itemId - Id of the item state to add
 *
 * @returns New game state
 */
export function removeFromInventory(
    gs: GameState,
    inventory: InventoryState,
    itemId: string
): GameState {
    const position = inventory.items.indexOf(itemId);
    // If the item is not in the inventory, do nothing
    if (position < 0) return gs;

    const newInventory = {
        ...inventory,
        items: [
            ...inventory.items.slice(0, position),
            ...inventory.items.slice(position + 1)
        ]
    };
    return setObjectState(gs, newInventory);
}

/**
 * Moves item between inventories.
 *
 * @gs - Game state
 * @param from - Inventory to remove the item from
 * @param to - Inventory to add the item to
 * @param itemId - Id of Item state to move
 *
 * @returns New game state
 */
export function moveBetweenInventories(
    gs: GameState,
    from: InventoryState,
    to: InventoryState,
    itemId: string
): GameState {
    const newGs = removeFromInventory(gs, from, itemId);
    // Proceed only if item was removed first
    if (gs !== newGs) return addToInventory(newGs, to, itemId);
    // Otherwise do nothing
    return gs;
}
