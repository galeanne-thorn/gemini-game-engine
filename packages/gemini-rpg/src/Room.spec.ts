import "jest";
import { Room } from "./";

describe("Room", () => {

    describe("createState", () => {
        it("initializes items as empty array", () => {
            const room = new Room({ id: "room", ui: "room", exits: [] });
            const state = room.createState(null);
            expect(state.items).toBeDefined();
            expect(state.items.length).toBe(0);
        });
    });

    describe("evaluateChoices", () => {
        it.skip("returns correct exits", () => fail());
        it.skip("returns correct item choices", () => fail());
        it.skip("returns correct person choices", () => fail());
    });
});
