import { EmptyGameState, IGame, TemplateCatalog } from "@galeanne-thorn/gemini-core";
import "jest";
import { getItemState, getPersonState, getRoomState, Item, Person, Room, RpgPlugin } from "./";

describe("RpgModule", () => {

    const templates = new TemplateCatalog();
    const igame: IGame = {
        actions: null,
        conditions: null,
        templates,
        perform: null
    };

    const item1 = new Item({ id: "item1", ui: "item" });
    templates.add(item1);

    const person = templates.add(
        new Person({
            id: "testPerson",
            ui: "person",
            items: [["item1", 3]]
        })
    );
    const room = templates.add(
        new Room({
            id: "testRoom",
            ui: "room",
            exits: [],
            items: [["item1", 2]]
        })
    );

    describe("initializer", () => {

        it.skip("initializes Person states", () => fail());

        it("initializes Person inventory and items", () => {
            const gs = RpgPlugin.initializer(EmptyGameState, igame);
            const inventory = getPersonState(gs, person.id);

            expect(inventory).toBeDefined();
            expect(inventory.items.length).toBe(1);

            const item = getItemState(gs, inventory.items[0]);
            expect(item.templateId).toBe("item1");
            expect(item.count).toBe(3);
        });

        it.skip("initializes Room states", () => fail());

        it("initializes Room inventory and items", () => {
            const gs = RpgPlugin.initializer(EmptyGameState, igame);
            const inventory = getRoomState(gs, room.id);

            expect(inventory).toBeDefined();
            expect(inventory.items.length).toBe(1);

            const item = getItemState(gs, inventory.items[0]);
            expect(item.templateId).toBe("item1");
            expect(item.count).toBe(2);
        });
    });
});
