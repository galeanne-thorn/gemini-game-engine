/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import {
    Catalog, GameCatalogs, GameState, ObjectState, ObjectStateGetter, Template, TemplateDefinition
} from "@galeanne-thorn/gemini-core";
import { InventoryDefinition, InventoryPayload } from "./Inventory";
import { Level, levelToExperience } from "./skills/Levels";
import { Skill, SkillState, TYPE_SKILL } from "./skills/Skill";

/**
 * Person type Id
 */
export const TYPE_PERSON = "person";

/**
 * Person state payload
 */
export interface PersonPayload extends InventoryPayload {
    /**
     * Gets current room the person is in
     */
    room: string;
    /**
     * Person skills
     */
    skills: Catalog<SkillState>;
}

/**
 * Person state
 */
export type PersonState = ObjectState<PersonPayload>;

/**
 * Person definition
 */
export interface PersonDefinition extends TemplateDefinition<PersonPayload> {
    /**
     * Initial skill levels for the person.
     */
    skills?: Catalog<Level>;
    /**
     * Initial items
     */
    items?: InventoryDefinition[];
}

/**
 * Person interface
 */
export interface Person extends Template<PersonPayload>, PersonDefinition {
}

/**
 * Person template
 */
export class Person extends Template<PersonPayload> {

    /**
     * Creates new person
     *
     * @param definition - Person definition object
     */
    constructor(definition: PersonDefinition) {
        super(TYPE_PERSON, definition);
    }

    /**
     * Creates new PersonState with given id
     *
     * @param catalogs - game catalogs to use
     * @param id - id of the PersonState to create. If not supplied, UUID type 4 is used
     * @returns new PersonState based on this template, with given id
     */
    public createState(catalogs: GameCatalogs, id: string): PersonState {
        const state = super.createState(catalogs, id);
        state.items = [];
        const skills = this.skills || {};
        state.skills = {};
        for (const key in skills) {
            if (skills.hasOwnProperty(key)) {
                const level = skills[key];
                const skill = catalogs.templates.getTemplate<Skill>(TYPE_SKILL, key);
                state.skills[key] = skill.createChildState(catalogs);
                state.skills[key].experience = levelToExperience(level, skill.difficulty);
            }
        }
        return state;
    }
}

/**
 * Gets state of person with given Id
 */
export const getPersonState = ObjectStateGetter<PersonState>(TYPE_PERSON);
