/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import {
    ActionGroup, GameCatalogs, GameState, ObjectState,
    ObjectStateGetter, Template, TemplateDefinition, UiComponent
} from "@galeanne-thorn/gemini-core";
import { InventoryDefinition, InventoryPayload, InventoryState } from "./Inventory";

/**
 * Room Type Id
 */
export const TYPE_ROOM = "room";

/**
 * Room exit definition
 */
export interface RoomExit extends UiComponent {
    /**
     * Id of the target room
     */
    target: string;
}

/**
 * Room dynamic data
 */
export type RoomPayload = InventoryPayload;

/**
 * Room state
 */
export type RoomState = ObjectState<RoomPayload>;

/**
 * Room definition
 */
export interface RoomDefinition extends TemplateDefinition<RoomPayload> {
    /**
     * Exits from the room
     */
    exits: RoomExit[];
    /**
     * Optional action to perform when player person enters room
     */
    onEntry?: ActionGroup;
    /**
     * Initial items
     */
    items?: InventoryDefinition[];
}

/**
 * Room interface
 */
export interface Room extends Template<RoomPayload>, RoomDefinition {
}

/**
 * Room template
 */
export class Room extends Template<RoomPayload>{

    /**
     * Creates new room template
     *
     * @param id - Id of the room
     * @param initialRoomState - Initial room state
     */
    public constructor(definition: RoomDefinition) {
        super(TYPE_ROOM, definition);
    }

    /**
     * Creates room state
     * @param catalogs - Game catalogs
     * @param id - Optional ID for the state
     */
    public createState(catalogs: GameCatalogs, id?: string): RoomState {
        const state = super.createState(catalogs, id);
        state.items = [];
        return state;
    }
}

/**
 * Gets state of room with given Id
 *
 * @param gs - Game State
 */
export const getRoomState = ObjectStateGetter<RoomState>(TYPE_ROOM);
