Gemini Game Engine - RPG subsystem
==========================================

RPG subsystem implements various game objects for role playing games

Installation
------------

```
npm install @galeanne-thorn/gemini-rpg
```

Classes
-------

* Scenes
* Rooms
* Persons
    * NPC
    * Hero
* Skills
* Items