/**
 * @galeanne-thorn/gemini-core
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { GameState, uuid } from "@galeanne-thorn/gemini-core";
import { padStart } from "lodash";
import { Storage } from "./LocalStorage";

/**
 * Description of the save slot
 */
export interface SlotDescription {
    /** Id of the slot */
    id: string;
    /** Date the slot was written to */
    date: Date;
    /** Name of the slot */
    name: string;
}

/**
 * Information about all saved slots
 */
export interface SlotDescriptions {
    auto: SlotDescription[];
    user: SlotDescription[];
}
/**
 * Information about persisted games
 */
export interface PersistedMetadata extends SlotDescriptions {
    /** Index of last autosave slot */
    lastAutoSave: number;
}

/** Prefix for save slots */
const DATA = "save-data-";
/** Prefix for save slot information */
const INFO = "save-info";

/**
 * Game persister
 *
 * Saves game state to local storage
 *
 * Data are saved in two slots:
 * * SP-save-info = description of the save slots
 * * SP-sdve-data-ID = serialized game state
 *
 * Where SP is Storage.Prefix and ID unique identification of the slot
 */
export class GamePersister {

    private storageInfo: PersistedMetadata;

    /**
     * Creates new GamePersister
     *
     * @param storage - Persistent storage to use
     * @param maxAutosaveSlots - Number of autosave slots. This is only used if the number is not read from storage
     */
    public constructor(private storage: Storage, private maxAutosaveSlots: number = 8) {
        this.storageInfo = storage.get<PersistedMetadata>(INFO) || {
            user: [],
            auto: [],
            lastAutoSave: maxAutosaveSlots - 1
        };
    }

    /**
     * Gets list of existing save slots
     *
     * @returns Array of slots. If the slot is not used, the return[n] will be undefined
     */
    public getSaveSlots(): SlotDescriptions {
        return {
            user: this.storageInfo.user,
            auto: this.storageInfo.auto
        };
    }

    /**
     * Saves game to new slot with given name and slot Id
     *
     * @param gs - Game state to save
     * @param name - Name for UI of the save slot
     * @param id - Optional Id under which to save the game. If it already exist, it will be overwritten
     */
    public save(gs: GameState, name: string, id?: string): void {
        const slotId = id || uuid();
        // Test if slotId already exists
        const slot = this.storageInfo.user.find(s => s.id === slotId);
        if (slot) {
            slot.name = name;
            slot.date = new Date();
        }
        else {
            this.storageInfo.user.push(
                {
                    id: slotId,
                    name,
                    date: new Date()
                }
            );
        }
        this.saveStorageInfo();
        this.storage.set(DATA + slotId, gs);
    }

    /*
    * Auto saves current game state to local storage
     * @param gs - Game state
     * @returns Unmodified game state
     */
    public autoSave(gs: GameState): void {
        const nextSave = (this.storageInfo.lastAutoSave + 1) % this.maxAutosaveSlots;
        this.storageInfo.lastAutoSave = nextSave;
        const id = `auto-${nextSave}`;
        this.storageInfo.auto[nextSave] = {
            id,
            name: `Autosave ${nextSave + 1}`,
            date: new Date()
        };
        this.saveStorageInfo();
        this.storage.set(DATA + id, gs);
    }

    /**
     * Loads game from given slot
     */
    public load(slotId: string): GameState {
        return this.storage.get(DATA + slotId);
    }

    /**
     * Clears game from save slot
     */
    public clear(slotId: string): void {
        this.storage.remove(DATA + slotId);
        const index = this.storageInfo.user.findIndex(s => s.id === slotId);
        if (index < 0) return;

        this.storageInfo = {
            ...this.storageInfo,
            user: [
                ...this.storageInfo.user.slice(0, index),
                ...this.storageInfo.user.slice(index + 1)
            ]
        };
        this.saveStorageInfo();
    }

    private saveStorageInfo(): void {
        this.storage.set(INFO, this.storageInfo);
    }
}
