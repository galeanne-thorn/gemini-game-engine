import "jest";

import { EmptyGameState } from "@galeanne-thorn/gemini-core";
import { GamePersister, PersistedMetadata, SlotDescription, Storage } from "../";

class MockStorage implements Storage {
    public get prefix(): string { return "mock"; }
    public set = jest.fn();
    public get = jest.fn();
    public remove = jest.fn();
    public keys = jest.fn();
    public clearAllFromLocalStorage = jest.fn();
}

const slotInfo: SlotDescription =
    {
        id: "test",
        name: "test name",
        date: new Date("2000-01-01")
    };

describe("GamePersister", () => {
    describe("constructor", () => {
        it("initializes storage info if there is none in storage", () => {
            const storage = new MockStorage();
            storage.get.mockReturnValueOnce(undefined);

            const persister = new GamePersister(storage);

            expect(storage.get).toBeCalledWith("save-info");
            expect(persister.getSaveSlots()).toEqual({ user: [], auto: [] });
        });

        it("uses storage info it got from storage", () => {
            const info: PersistedMetadata = {
                user: [slotInfo],
                lastAutoSave: 0,
                auto: [slotInfo]
            };

            const storage = new MockStorage();
            storage.get.mockReturnValueOnce(info);

            const persister = new GamePersister(storage);

            expect(storage.get).toBeCalledWith("save-info");
            expect(persister.getSaveSlots()).toEqual({ user: info.user, auto: info.auto });
        });
    });

    describe("save", () => {
        it("saves data to storage", () => {
            const storage = new MockStorage();
            const persister = new GamePersister(storage);

            persister.save(EmptyGameState, "testSave");

            expect(storage.set).toBeCalledWith("save-info", expect.anything());
            expect(storage.set).toBeCalledWith(expect.stringMatching(/^save-data-.*$/), expect.anything());
        });

        it("adds new slot info", () => {
            const storage = new MockStorage();
            const persister = new GamePersister(storage);

            persister.save(EmptyGameState, "testSave");

            const slots = persister.getSaveSlots();
            expect(slots.user).toHaveLength(1);
            expect(slots.user[0].date).toBeDefined();
            expect(slots.user[0].id).toBeDefined();
            expect(slots.user[0].name).toEqual("testSave");
        });

        it("overwrites slot if slot with same id already exists", () => {
            const storage = new MockStorage();
            storage.get.mockReturnValueOnce({ user: [slotInfo] });

            const persister = new GamePersister(storage);

            let slots = persister.getSaveSlots();
            expect(slots.user).toHaveLength(1);

            persister.save(EmptyGameState, "new name", "test");

            slots = persister.getSaveSlots();
            expect(slots.user).toHaveLength(1);
            expect(slots.user[0].id).toBe("test");
            expect(slots.user[0].name).toBe("new name");
            // TODO: Figure how to verify this:
            // expect(slots[0].date.valueOf()).toBeGreaterThan(slotInfo.date.valueOf());
        });
    });

    describe("load", () => {
        it("reads slot from storage", () => {
            const persistedState = {
                ...EmptyGameState,
                data: "some data"
            };

            const storage = new MockStorage();
            storage.get
                .mockReturnValueOnce({ user: [slotInfo] })
                .mockReturnValueOnce(persistedState);

            const persister = new GamePersister(storage);
            const loadedState = persister.load("test");

            expect(loadedState).toEqual(persistedState);
        });
    });

    describe("clear", () => {
        it("removes slot from storage", () => {
            const storage = new MockStorage();
            storage.get.mockReturnValueOnce({ user: [slotInfo] });

            const persister = new GamePersister(storage);
            persister.clear("test");

            expect(storage.set).toBeCalledWith("save-info", expect.anything());
            expect(storage.remove).toBeCalledWith("save-data-test");
        });

        it("removes slot info", () => {
            const storage = new MockStorage();
            storage.get.mockReturnValueOnce({
                user: [
                    { id: "slot1", name: "slot1", date: new Date("2018-01-31") },
                    { id: "slot2", name: "slot2", date: new Date("2018-01-31") },
                    { id: "slot3", name: "slot3", date: new Date("2018-01-31") }
                ]
            });

            const persister = new GamePersister(storage);
            persister.clear("slot2");

            const slots = persister.getSaveSlots();

            expect(slots.user).toHaveLength(2);
            expect(slots.user[0].id).toBe("slot1");
            expect(slots.user[1].id).toBe("slot3");
        });
    });
});
