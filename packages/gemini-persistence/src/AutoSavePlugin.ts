/**
 * @galeanne-thorn/gemini-core
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import {
    ACTION_GAME_PERFORM, ActionPerformer, GamePlugin, GameState, isActionType
} from "@galeanne-thorn/gemini-core";
import { padStart } from "lodash";
import { GamePersister } from "./GamePersister";
import { Storage } from "./LocalStorage";

/**
 * AutoSave module
 *
 * It is recommended as a first module in the chain
 * to ensure no one modifies the state before it is saved
 */
export class AutoSavePlugin implements GamePlugin {

    /** Id of the module */
    public readonly id: "autosave";
    /** Title of the module */
    public readonly title: "Gemini Game Autosave Plugin";
    /** Author of the module */
    public readonly author: "Galeanne Thorn";

    /**
     * Creates new auto-save module
     * @param storage - Storage to use
     * @param slotCount - Number of autosave slots, 8 by default
     */
    public constructor(private readonly persister: GamePersister, private readonlyslotCount: number = 8) {
    }

    /** Game persister used for autosaving */
    public get Persister() { return this.persister; }

    /**
     * AutoSave extender
     * @param original - Original GameStateReducer
     * @returns New GameStateReducer for Auto Save feature
     */
    public extender = (original: ActionPerformer): ActionPerformer =>
        (gs, a, g) => {
            if (isActionType(a, ACTION_GAME_PERFORM)) {
                this.persister.autoSave(gs);
            }
            return original(gs, a, g);
        }
}
