/**
 * @galeanne-thorn/gemini-core
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import {
    ACTION_GAME_LOADED, ACTION_GAME_LOADING, ActionGroup, ActionPerformer,
    GamePlugin, GameState, isActionType
} from "@galeanne-thorn/gemini-core";
import { Storage } from "./LocalStorage";

const LOG_ACTION = "log-action";
const LOG_STATE = "log-initial";

export class ActionLogPlugin implements GamePlugin {

    public readonly id = "actionLog";
    public readonly name = "Gemini Action Log Plugin";

    private actionLog: ActionGroup[] = [];

    /**
     * Creates new ActionLogPlugin
     * @param storage - Storage to log the actions to
     */
    public constructor(private readonly storage: Storage) {
    }

    /**
     * Action log extender
     * @param original - Original GameStateReducer
     * @returns New GameStateReducer for Action Log feature
     */
    public extender = (original: ActionPerformer): ActionPerformer =>
        (gs, a, g) => {
            if (isActionType(a, ACTION_GAME_LOADED)) {
                this.logState(gs);
            }
            else {
                this.logAction(a);
            }
            return original(gs, a, g);
        }

    /**
     * Game state initializer.
     * Used to log the initial game state
     */
    public initializer = (gs: GameState): GameState => {
        this.logState(gs);
        return gs;
    }

    private logState(gs: GameState) {
        this.actionLog = [];
        this.storage.set(LOG_STATE, gs);
        this.storage.set(LOG_ACTION, this.actionLog);
    }

    private logAction(a: ActionGroup) {
        this.actionLog.push(a);
        this.storage.set(LOG_ACTION, this.actionLog);
    }
}
