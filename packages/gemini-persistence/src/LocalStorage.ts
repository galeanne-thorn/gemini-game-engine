/**
 * @galeanne-thorn/gemini-core
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { isArray, isNumber, isObject, startsWith } from "lodash";

/**
 * Local storage interface
 */
export interface Storage {
    /**
     * Prefix used by the storage
     */
    readonly prefix: string;

    /**
     * Directly adds a value to local storage
     * If local storage is not available in the browser use cookies
     * Example use: localStorageService.add('library','angular');
     *
     * @param key - Key to store the value under
     * @param value - Value to store
     */
    set(key: string, value: any);

    /**
     * Directly get a value from local storage
     *
     * @param key - Key to look for
     * @returns Value stored under the key
     */
    get<T>(key: string): T;

    /**
     * Remove an item from local storage
     *
     * @param key - Key to remove
     */
    remove(key: string): void;

    /**
     * Returns array of keys this local storage uses
     */
    keys(): string[];

    /**
     * Remove all data for this app from local storage
     * Also optionally takes a regular expression string and removes the matching key-value pairs
     * Should be used mostly for development purposes
     */
    clearAllFromLocalStorage(regularExpression?: string): boolean;
}

/**
 * Local Storage implementation support
 *
 * Lot of code borrowed from https://github.com/grevory/angular-local-storage
 */
export class LocalStorage implements Storage {

    /**
     * Storage prefix
     */
    public get prefix() {
        return this.storagePrefix;
    }

    /**
     * Creates new local storage instance
     *
     * @param storagePrefix - Prefix to indentify records in storage
     */
    public constructor(private storagePrefix: string) {
    }

    /**
     * Directly adds a value to local storage
     * If local storage is not available in the browser use cookies
     * Example use: localStorageService.add('library','angular');
     *
     * @param key - Key to store the value under
     * @param value - Value to store
     */
    public set(key: string, value: any): void {
        value = this.serializeValue(value);
        localStorage.setItem(this.deriveQualifiedKey(key), value);
    }

    /**
     *  Directly get a value from local storage
     *
     * @param key - Key to look for
     * @returns Value stored under the key
     */
    public get<T>(key: string): T {
        const item = localStorage.getItem(this.deriveQualifiedKey(key));
        return this.deserializeValue(item) as T;
    }

    /**
     * Remove an item from local storage
     *
     * @param key - Key to remove
     */
    public remove(key: string): void {
        localStorage.removeItem(this.deriveQualifiedKey(key));
    }

    /**
     * Returns array of keys this local storage uses
     */
    public keys(): string[] {

        const prefixLength = this.storagePrefix.length;
        const keys = [];
        for (const key in localStorage) {
            // Only return keys that are for this app
            if (startsWith(key, this.storagePrefix)) {
                keys.push(key.substr(prefixLength));
            }
        }
        return keys;
    }

    /**
     * Remove all data for this app from local storage
     * Also optionally takes a regular expression string and removes the matching key-value pairs
     * Should be used mostly for development purposes
     */
    public clearAllFromLocalStorage(regularExpression: string = ""): boolean {
        // accounting for the '.' in the prefix when creating a regex
        const tempPrefix = this.storagePrefix.slice(0, -1);
        const testRegex = new RegExp(tempPrefix + "." + regularExpression);

        const prefixLength = this.storagePrefix.length;
        for (const key in localStorage) {
            // Only remove items that are for this app and match the regular expression
            if (testRegex.test(key)) {
                this.remove(key.substr(prefixLength));
            }
        }
        return true;
    }

    /**
     *  Creates key with prefix
     */
    private deriveQualifiedKey(key: string): string {
        return `${this.storagePrefix}.${key}`;
    }

    /**
     * Determines whether the num is string representation of number
     */
    private isStringNumber(num: string): boolean {
        return /^-?\d+\.?\d*$/.test(num.replace(/["']/g, ""));
    }

    /**
     * Serializes value to JSON
     */
    private serializeValue(value: any): string {
        // Let's convert undefined values to null to get the value consistent
        if (value === undefined) {
            value = null;
        }
        else if (isObject(value) || isArray(value) || isNumber(+value || value)) {
            value = JSON.stringify(value);
        }
        return value;
    }

    /**
     * Deserializes value from JSON
     */
    private deserializeValue(value: string): any {
        if (value === null || value === undefined) return null;
        if (value.charAt(0) === "{" || value.charAt(0) === "[" || this.isStringNumber(value)) {
            return JSON.parse(value);
        }
        return value;
    }
}
