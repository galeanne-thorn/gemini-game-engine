import "jest";

import * as ne from "nearley";
import * as grammar from "./gemini-mark";

const geminiMarkGrammar = ne.Grammar.fromCompiled(grammar);

describe("gemini-mark parser", () => {

    it("parses simple sentence", () => {
        const text = "This is a simple sentence.";
        const parser = new ne.Parser(geminiMarkGrammar);
        parser.feed(text);
        const result = parser.finish();

        expect(result).toEqual([[text]]);
    });

    it("parses variable in text", () => {
        const text = "This is a ${simple.sentence}.";
        const parser = new ne.Parser(geminiMarkGrammar);
        parser.feed(text);
        const result = parser.finish();

        expect(result).toEqual([[
            "This is a ",
            {
                tag: "var",
                name: "simple.sentence"
            },
            "."
        ]]);
    });

    it("parses simple if with condition without parameters, ommiting braces", () => {
        const text = "<<if condition>>true result<</if>>";
        const parser = new ne.Parser(geminiMarkGrammar);
        parser.feed(text);
        const result = parser.finish();

        // tslint:disable-next-line:no-console
        // console.log(JSON.stringify(result, null, 2));
        expect(result).toEqual(
            [[
                {
                    tag: "if",
                    condition: {
                        type: "condition"
                    },
                    onTrue: ["true result"],
                    onFalse: null
                }
            ]]
        );
    });

    it("parses simple if with condition without parameters, with braces", () => {
        const text = "<<if condition()>>true result<</if>>";
        const parser = new ne.Parser(geminiMarkGrammar);
        parser.feed(text);
        const result = parser.finish();

        expect(result).toEqual(
            [[
                {
                    tag: "if",
                    condition: {
                        type: "condition"
                    },
                    onTrue: ["true result"],
                    onFalse: null
                }
            ]]
        );
    });

    it("parses simple if with condition with one parameter", () => {
        const text = "<<if condition(par1:5)>>true result<</if>>";
        const parser = new ne.Parser(geminiMarkGrammar);
        parser.feed(text);
        const result = parser.finish();

        expect(result).toEqual(
            [[
                {
                    tag: "if",
                    condition: {
                        type: "condition",
                        par1: 5
                    },
                    onTrue: ["true result"],
                    onFalse: null
                }
            ]]
        );
    });

    it("parses simple if with condition with multiple parameters", () => {
        const text = "<<if condition(par1:5, par2:\"data2\",par3:'data3')>>true result<</if>>";
        const parser = new ne.Parser(geminiMarkGrammar);
        parser.feed(text);
        const result = parser.finish();

        expect(result).toEqual(
            [[
                {
                    tag: "if",
                    condition: {
                        type: "condition",
                        par1: 5,
                        par2: "data2",
                        par3: "data3"
                   },
                    onTrue: ["true result"],
                    onFalse: null
                }
            ]]
        );
    });

    it("parses if with boolean", () => {
        const text = "<<if true>>true result<</if>>";
        const parser = new ne.Parser(geminiMarkGrammar);
        parser.feed(text);
        const result = parser.finish();

        expect(result).toEqual(
            [[
                {
                    tag: "if",
                    condition: true,
                    onTrue: ["true result"],
                    onFalse: null
                }
            ]]
        );
    });

    it("parses if with variable as condition", () => {
        const text = "<<if ${variable}>>true result<</if>>";
        const parser = new ne.Parser(geminiMarkGrammar);
        parser.feed(text);
        const result = parser.finish();

        expect(result).toEqual(
            [[
                {
                    tag: "if",
                    condition: {
                        tag: "var",
                        name: "variable"
                    },
                    onTrue: ["true result"],
                    onFalse: null
                }
            ]]
        );
    });

    it("parses if with variable as condition parameter", () => {
        const text = "<<if condition(x:${variable})>>true result<</if>>";
        const parser = new ne.Parser(geminiMarkGrammar);
        parser.feed(text);
        const result = parser.finish();

        expect(result).toEqual(
            [[
                {
                    tag: "if",
                    condition: {
                        type: "condition",
                        x: {
                            tag: "var",
                            name: "variable"
                        }
                    },
                    onTrue: ["true result"],
                    onFalse: null
                }
            ]]
        );
    });

    it("parses if-else", () => {
        const text = "<<if condition>>true result<<else>> false result <</if>>";
        const parser = new ne.Parser(geminiMarkGrammar);
        parser.feed(text);
        const result = parser.finish();

        expect(result).toEqual(
            [[
                {
                    tag: "if",
                    condition: {
                        type: "condition"
                    },
                    onTrue: ["true result"],
                    onFalse: [" false result "]
                }
            ]]
        );
    });

    it("parses nested if-else", () => {
        const text =
            "<<if condition>> outer true<<if innerCondition>>inner true<</if>> result" +
            "<<else>> <<if falseCondition>> result <</if>>" +
            "<</if>>";

        const parser = new ne.Parser(geminiMarkGrammar);
        parser.feed(text);
        const result = parser.finish();

        expect(result).toEqual(
            [[
                {
                    tag: "if",
                    condition: {
                        type: "condition"
                    },
                    onTrue: [
                        " outer true",
                        {
                            tag: "if",
                            condition: {
                                type: "innerCondition"
                            },
                            onTrue: [
                                "inner true"
                            ],
                            onFalse: null
                        },
                        " result"
                    ],
                    onFalse: [
                        " ",
                        {
                            tag: "if",
                            condition: {
                                type: "falseCondition"
                            },
                            onTrue: [
                                " result "
                            ],
                            onFalse: null
                        }
                    ]
                }
            ]]
        );
    });

    it("parses text, if and following text", () => {
        const text = "Initial text <<if condition>>true result<</if>> after text.";
        const parser = new ne.Parser(geminiMarkGrammar);
        parser.feed(text);
        const result = parser.finish();

        expect(result).toEqual(
            [[
                "Initial text ",
                {
                    tag: "if",
                    condition: {
                        type: "condition"
                    },
                    onTrue: ["true result"],
                    onFalse: null
                },
                " after text."
            ]]
        );
    });
});
