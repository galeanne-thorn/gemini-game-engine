import "jest";

import { GeminiLexer } from "./gemini-lexer";

describe("Lexer", () => {

    it("Lexes simple text", () => {
        const l = new GeminiLexer();
        l.reset("Simple text with\n\nnew line.");

        expect(l.next()).toEqual({ type: "text", value: "Simple text with\n\nnew line.", position: 0 });
        expect(l.next()).toBeUndefined();
    });

    it("Lexes text with variable", () => {
        const l = new GeminiLexer();
        l.reset("Simple ${variable.path} text.");

        expect(l.next()).toEqual({ type: "text", value: "Simple ", position: 0 });
        expect(l.next()).toEqual({ type: "variable", value: "variable.path", position: 7 });
        expect(l.next()).toEqual({ type: "text", value: " text.", position: 23 });
        expect(l.next()).toBeUndefined();
    });

    it("Lexes variable", () => {
        const l = new GeminiLexer();
        l.reset("${variable.path}");

        expect(l.next()).toEqual({ type: "variable", value: "variable.path", position: 0 });
        expect(l.next()).toBeUndefined();
    });

    it("Lexes simple opening tag", () => {
        const l = new GeminiLexer();
        l.reset("<<if>>");

        expect(l.next()).toEqual({ type: "tagOpen", value: "<<", position: 0 });
        expect(l.next()).toEqual({ type: "keyword", value: "if", position: 2 });
        expect(l.next()).toEqual({ type: "tagClose", value: ">>", position: 4 });
        expect(l.next()).toBeUndefined();
    });

    it("Lexes simple closing tag", () => {
        const l = new GeminiLexer();
        l.reset("<</if>>");

        expect(l.next()).toEqual({ type: "tagOpen", value: "<<", position: 0 });
        expect(l.next()).toEqual({ type: "endTagMark", value: "/", position: 2 });
        expect(l.next()).toEqual({ type: "keyword", value: "if", position: 3 });
        expect(l.next()).toEqual({ type: "tagClose", value: ">>", position: 5 });
        expect(l.next()).toBeUndefined();
    });

    it("Lexes tag with content", () => {
        const l = new GeminiLexer();
        l.reset("<<if>>text<</if>>");

        expect(l.next()).toEqual({ type: "tagOpen", value: "<<", position: 0 });
        expect(l.next()).toEqual({ type: "keyword", value: "if", position: 2 });
        expect(l.next()).toEqual({ type: "tagClose", value: ">>", position: 4 });
        expect(l.next()).toEqual({ type: "text", value: "text", position: 6});
        expect(l.next()).toEqual({ type: "tagOpen", value: "<<", position: 10 });
        expect(l.next()).toEqual({ type: "endTagMark", value: "/", position: 12 });
        expect(l.next()).toEqual({ type: "keyword", value: "if", position: 13 });
        expect(l.next()).toEqual({ type: "tagClose", value: ">>", position: 15 });

        expect(l.next()).toBeUndefined();
    });

    it("Lexes tag with condition", () => {
        const l = new GeminiLexer();
        l.reset("<<if condition(a:1)>>");

        expect(l.next()).toEqual({ type: "tagOpen", value: "<<", position: 0 });
        expect(l.next()).toEqual({ type: "keyword", value: "if", position: 2 });
        expect(l.next()).toEqual({ type: "space", value: " ", position: 4 });
        expect(l.next()).toEqual({ type: "identifier", value: "condition", position: 5 });
        expect(l.next()).toEqual({ type: "leftBrace", value: "(", position: 14 });
        expect(l.next()).toEqual({ type: "identifier", value: "a", position: 15 });
        expect(l.next()).toEqual({ type: "colon", value: ":", position: 16 });
        expect(l.next()).toEqual({ type: "number", value: "1", position: 17 });
        expect(l.next()).toEqual({ type: "rightBrace", value: ")", position: 18 });
        expect(l.next()).toEqual({ type: "tagClose", value: ">>", position: 19 });
        expect(l.next()).toBeUndefined();
    });
});
