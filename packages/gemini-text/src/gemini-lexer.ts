/**
 * Lexer token
 */
export interface Token {
    /**
     * Token type
     */
    type: string;
    /**
     * Token value
     */
    value: any;
    /**
     * Current line
     */
    position: number;
}

/**
 * State of the lexer
 */
export interface State {
    /**
     * Processing inside tag
     */
    inTag: boolean;
    /**
     * Current position
     */
    position: number;
}

const regexData: Array<[RegExp, string]> = [
    [/\//, "endTagMark"],
    [/if|else/, "keyword"],
    [/>>/, "tagClose"],
    [/(?:true)|(?:false)\b/, "boolean"],
    [/[a-zA-Z_]\w*/, "identifier"],
    [/\$\{[a-zA-Z_]\w*(?:\.[a-zA-Z_]\w*)*\}/, "variable"],
    [/:/, "colon"],
    [/\(/, "leftBrace"],
    [/\)/, "rightBrace"],
    [/,/, "comma"],
    [/'.*?'/, "quoted"],
    [/".*?"/, "doubleQuoted"],
    [/-?\d+(?:\.\d+)?(?:E(?:\+|-)\d+)?/, "number"],
    [/\s+/, "space"]
];

/**
 * Lexer
 */
export class GeminiLexer {

    private state: State;
    private chunk: string;
    private inTagRegex: RegExp;

    /** Creates new lexer */
    public constructor() {
        this.inTagRegex = new RegExp(
            regexData.map(r => `(${r[0].source})`).join("|"),
            "y"
        );
    }

    /**
     * Returns a token object, which could have fields for line number, etc.
     * Importantly, a token object must have a value attribute.
     */
    public next(): Token {
        if (this.chunk.length === 0) return undefined;
        if (this.state.inTag) {
            return this.nextInTagToken();
        }
        return this.nextToken();
    }

    /**
     * Returns an info object that describes the current state of the lexer.
     * Nearley places no restrictions on this object.
     */
    public save(): State {
        return this.state;
    }

    /**
     * Sets the internal buffer of the lexer to chunk,
     * and restores its state to a state returned by save().
     */
    public reset(chunk: string, info?: State): void {
        this.state = info || {
            inTag: false,
            position: 0
        };
        this.chunk = chunk;
    }

    /**
     * Returns a string with an error message describing a parse error at that token
     * (for example, the string might contain the line and column where the error was found).
     */
    public formatError(token: Token): string {
        return `Error parsing "${this.chunk}" at ${token.position}`;
    }

    /**
     * Returns true if the lexer can emit tokens with that name.
     * This is used to resolve %-specifiers in compiled nearley grammars.
     */
    public has(name: string): boolean {
        switch (name) {
            case "tagOpen":
            case "variable":
            case "text":
            case "endTagMark":
            case "keyword":
            case "tagClose":
            case "boolean":
            case "identifier":
            case "colon":
            case "leftBrace":
            case "rightBrace":
            case "comma":
            case "quoted":
            case "doubleQuoted":
            case "number":
            case "space":
                return true;
            default:
                return false;
        }
    }

    private nextToken(): Token {

        let token: Token = null;

        if (this.state.position >= this.chunk.length) return undefined;

        const tagPosition = this.chunk.indexOf("<<", this.state.position);
        if (tagPosition === this.state.position) {
            // return tag
            token = {
                type: "tagOpen",
                value: "<<",
                position: this.state.position
            };
            this.state.position += 2;
            this.state.inTag = true;
            return token;
        }

        const varPosition = this.chunk.indexOf("${", this.state.position);
        if (varPosition === this.state.position) {
            // return var
            return this.getVarToken();
        }

        if (tagPosition < 0 && varPosition < 0) {
            // Nor tag nor var found, return rest of text
            token = {
                type: "text",
                value: this.chunk.substr(this.state.position),
                position: this.state.position
            };
            this.state.position = this.chunk.length;
            return token;
        }

        // Both tag and var found, return text up the the tag or var, which one is first
        let endPosition: number;
        if (tagPosition < 0) {
            endPosition = varPosition;
        }
        else if (varPosition < 0) {
            endPosition = tagPosition;
        }
        else {
            endPosition = Math.min(tagPosition, varPosition);
        }
        // Nor tag nor var found, return rest of text
        token = {
            type: "text",
            value: this.chunk.substring(this.state.position, endPosition),
            position: this.state.position
        };
        this.state.position = endPosition;

        return token;
    }

    /**
     * Creates var token. Expects variable on current location
     */
    private getVarToken(): Token {
        const end = this.chunk.indexOf("}", this.state.position);
        if (end < 0) {
            throw new Error(`Variable at ${this.state.position} not closed`);
        }
        const token = {
            type: "variable",
            value: this.chunk.substring(this.state.position + 2, end),
            position: this.state.position
        };
        this.state.position = end + 1;
        return token;
    }

    /**
     * Parses data in tag
     */
    private nextInTagToken(): Token {

        this.inTagRegex.lastIndex = this.state.position;
        const result = this.inTagRegex.exec(this.chunk);

        if (result === null) {
            throw new Error(`Unexpected text at position ${this.state.position}`);
        }

        let group = 0;
        for (let i = 1; i < result.length; ++i) {
            if (result[i]) {
                group = i;
                break;
            }
        }

        if (group === 0) {
            throw new Error(`Result not captured at position ${this.state.position}: [${result}]`);
        }

        const token = {
            type: regexData[group - 1][1],
            value: result[0],
            position: this.state.position
        };
        this.state.position = this.inTagRegex.lastIndex;
        switch (token.type) {
            case "tagClose":
                this.state.inTag = false;
                break;
            case "variable":
                token.value = token.value.slice(2, -1);
                break;
        }

        return token;
    }

}
