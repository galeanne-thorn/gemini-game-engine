import "jest";

import { ConditionCatalog, EmptyGameState, GameCatalogs, GameState } from "@galeanne-thorn/gemini-core";
import { evaluateText } from "./";

const gameState: GameState = {
    ...EmptyGameState,
    some: {
        long: {
            path: "result"
        }
    },
    recurse: "Text with ${recursive}",
    recursive: "recursive replacement",
    x: 5,
    nullValue: null
};

describe("evaluateText", () => {

    const conditions = new ConditionCatalog();
    const catalogs: GameCatalogs = {
        actions: null,
        conditions,
        templates: null
    };

    const X_IS_5 = "Xis5";
    conditions.add(X_IS_5, (gs, c) => gs.x === 5);

    it("replaces variable in text", () => {
        const text = "This is the ${some.long.path}.";
        const result = evaluateText(gameState, catalogs, text);

        expect(result).toBe("This is the result.");
    });

    it("replaces all occurrences of variable in text", () => {
        const text = "${x} + ${x} <> ${x}${x}";
        const result = evaluateText(gameState, catalogs, text);

        expect(result).toBe("5 + 5 <> 55");
    });

    it("replaces variable with object value as stringified JSON in text", () => {
        const text = "${some}";
        const result = evaluateText(gameState, catalogs, text);

        expect(result).toBe('{"long":{"path":"result"}}');
    });

    it("replaces variable with null value with NULL string in text", () => {
        const text = "This is ${nullValue}";
        const result = evaluateText(gameState, catalogs, text);

        expect(result).toBe("This is null");
    });

    it("returns correct part when if condition evaluates to true", () => {
        const result = evaluateText(
            gameState,
            catalogs,
            "<<if Xis5>>X is five<<else>>X is not five<</if>>");

        expect(result).toBe("X is five");
    });

    it("returns correct part when if condition evaluates to false", () => {
        const result = evaluateText(
            { ...gameState, x: 0 },
            catalogs,
            "<<if Xis5>>X is five<<else>>X is not five<</if>>");

        expect(result).toBe("X is not five");
    });
});
