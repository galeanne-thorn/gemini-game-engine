/**
 * gemini-game-engine text processing
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Condition } from "@galeanne-thorn/gemini-core";

/** Tag base interface */
export interface Tag {
    /**
     * Tag type
     */
    tag: string;
}

/**
 * Text part - string or tag
 */
export type TextPart = string | Tag;

/**
 * Tag "if"
 */
export interface TagIf extends Tag {
    /**
     * Condition information
     */
    condition: Condition;
    /**
     * Text to use if condition evaluates to true
     */
    onTrue: TextPart[];
    /**
     * Text to use if condition evaluates to false
     */
    onFalse: TextPart[];
}

export interface TagVar extends Tag {
    /**
     * Variable name
     */
    name: string;
}
