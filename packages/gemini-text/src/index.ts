/**
 * gemini-game-engine text processing
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

export * from "./Text";
