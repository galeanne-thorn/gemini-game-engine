/**
 * gemini-game-engine text processing
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Condition, GameCatalogs, GameState } from "@galeanne-thorn/gemini-core";
import { get, isUndefined } from "lodash";
import * as MD from "markdown-it";
import * as nearley from "nearley";
import * as grammar from "./gemini-mark";
import { Tag, TagIf, TagVar, TextPart } from "./Tag";

/**
 * Grammar to use
 */
const geminiMarkGrammar = nearley.Grammar.fromCompiled(grammar);

/**
 * Markdown
 */
const md = new MD();
// TODO: resolve if statements using plugins: md.use(plugin)

/**
 * Renders the Markdown text to HTML
 *
 * @param gs - Game state
 * @param catalogs - Game catalogs
 * @param text - Markdown text to convert
 *
 * @return HTML code rendering the given text
 */
export function getHtmlText(gs: GameState, catalogs: GameCatalogs, text: string): string {
    const updatedText = evaluateText(gs, catalogs, text);
    return md.render(updatedText, gs);
}

/**
 * Evaluates all conditionals in the text
 *
 * @param gs - Game state
 * @param catalogs - Game catalogs
 * @param text - Text to evaluate
 */
export function evaluateText(gs: GameState, catalogs: GameCatalogs, text: string): string {

    /**
     * Reduces the text part array to string
     * @param data - Data to reduce
     * @param initial - Initial state
     */
    function reduceText(data: TextPart[], initial: string): string {
        return data
            ? data.reduce<string>(evalTextPart, initial)
            : initial;
    }

    const resolvedVariables = {};

    /**
     * Evaluates variable
     * @param name - Name of the variable in game state
     */
    function getVariable(name: string): any {
        let value = resolvedVariables[name];
        if (value === undefined) {
            value = get(gs, name);
            resolvedVariables[name] = value;
        }
        return value;
    }

    /**
     * Evaluates condition
     * @param condition - Condition to evaluate
     */
    function evaluateCondition(condition: any): boolean {

        if (condition === "true") return true;
        if (condition === "false") return false;
        if (typeof (condition) === "object") {
            if (condition.tag === "var") {
                // This is variable
                return !!getVariable(condition.name);
            }

            // Normal condition
            const conditionObject: Condition = {
                type: condition.type
            };
            // Copy properties and resolve variables
            for (const key in condition) {
                if (key === "type") continue;
                if (condition.hasOwnProperty(key)) {
                    const element = condition[key];
                    if (typeof (element) === "object" && element.tag === "var") {
                        conditionObject[key] = getVariable(element.name);
                    }
                    else {
                        conditionObject[key] = element;
                    }
                }
            }
            return catalogs.conditions.evaluate(gs, catalogs, conditionObject, false);
        }
        // Here we do not know how to evaluate the condition
        throw new Error(`Unknown condition object ${JSON.stringify(condition)}`);
    }

    function evalTextPart(previous: string, current: string | Tag): string {
        // If null or undefined, return previous
        if (!current) return previous;

        // If current is string, simply concatenate it
        if (typeof (current) === "string") {
            return previous + current;
        }

        // If current is object, assume tag info
        switch (current.tag) {
            case "if":
                const tagIf = current as TagIf;
                // TODO: evaluate condition
                const conditionResult = evaluateCondition(tagIf.condition);
                if (conditionResult) {
                    return reduceText(tagIf.onTrue, previous);
                }
                else {
                    return reduceText(tagIf.onFalse, previous);
                }

            case "var":
                const tagVar = current as TagVar;
                const data: any = getVariable(tagVar.name);
                const value = (typeof data === "string") ? data : JSON.stringify(data);
                return previous + value;

            default:
                // This should not happed as parser should catch this
                throw new Error(`Unknown tag ${current.tag} encountered`);
        }
    }

    const parser = new nearley.Parser(geminiMarkGrammar);
    parser.feed(text);
    const parseResult = parser.results;

    const result = reduceText(parser.results[0], "");
    return result;
}
