@{%
import { flatten, fromPairs } from "lodash";
import { GeminiLexer } from "./gemini-lexer";

const lexer = new GeminiLexer();

function variableTag(d) {
    return {
        tag: "var",
        name: d[0].value
    };
}

function condition(d) {
    return {
        ...fromPairs(d[1]),
        type: d[0].value
    };
}

%}

# Use moo lexer
@lexer lexer
@preprocessor typescript

SENTENCE -> TEXT:*      {% flatten %}
TEXT     -> %text       {% d => d[0].value %}
          | %variable   {% variableTag %}
          | TAG         {% d => d[0][0] %}
TAG      -> IF

DATATAG[X]   -> %tagOpen $X %space CONDITION %tagClose  {% d => { return { tag: d[1][0], data: d[3] }; } %}
SIMPLETAG[X] -> %tagOpen $X %tagClose                   {% d => { return { tag: d[1][0] }; } %}
ENDTAG[X]    -> %tagOpen %endTagMark $X %tagClose

CONDITION     -> %boolean                                   {% d => d[0].value === "true" %}
               | %variable                                  {% variableTag %}
               | %identifier PARAMETERS:?                   {% condition %}
PARAMETERS    -> %leftBrace PARAMETERLIST:? %rightBrace     {% d => d[1] %}
PARAMETERLIST -> PARAMETER (%comma %space:? PARAMETER):*    {% d => [d[0], ...d[1].map(p => p[2])] %}
PARAMETER     -> %identifier %colon DATA                    {% d => [d[0].value, d[2]] %}
DATA          -> %variable                                  {% variableTag %}
               | %number                                    {% d => Number(d[0].value) %}
               | %quoted                                    {% d => d[0].value.slice(1, -1) %}
               | %doubleQuoted                              {% d => d[0].value.slice(1, -1) %}

# Tag definitions
IF -> DATATAG["if"] SENTENCE (SIMPLETAG["else"] SENTENCE):? ENDTAG["if"]
    {% d => {
        return {
            tag: "if",
            condition: d[0].data,
            onTrue: d[1],
            onFalse: (d[2]) ? d[2][1] : null,
            //c : d[0]
        };
    }
    %}
