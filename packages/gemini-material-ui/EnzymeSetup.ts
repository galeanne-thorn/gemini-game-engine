// setup file
import { configure } from "enzyme";
import * as ReactSixteenAdapter from "enzyme-adapter-react-16";
import "raf";

configure({
    adapter: new ReactSixteenAdapter()
});
