/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { evaluateChoices, getObjectState, TemplateCatalog } from "@galeanne-thorn/gemini-core";
import { Scene } from "@galeanne-thorn/gemini-rpg";
import * as React from "react";
import { ButtonChoiceSelector } from "./ButtonChoiceSelector";
import { StageProps } from "./StageViewProvider";
import { ActionsCard } from "./uiComponents/ActionsCard";
import { Text } from "./uiComponents/Text";
import { getUiText } from "./UiTools";

/**
 * React component for displaying Scene
 */
export function SceneView(props: StageProps) {
    const scene = getObjectState(props.gs, props.stage);
    const template = props.catalogs.templates.getTemplate<Scene>(scene);

    // TODO: Show image if defined for ui
    const title = (typeof template.ui === "string") ? template.id : template.ui.title;
    const choices = evaluateChoices(props.gs, props.catalogs, template.choices);

    return (
        <ActionsCard {...props} title={title} choices={choices}>
            <Text {...props} text={getUiText(template.ui)} />
        </ActionsCard>
    );
}
