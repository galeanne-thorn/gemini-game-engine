/**
 * @galeanne-thorn/gemini-material-ui
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { GameState } from "@galeanne-thorn/gemini-core";
import { SlotDescriptions } from "@galeanne-thorn/gemini-persistence";
import AppBar from "@material-ui/core/AppBar";
import Icon from "@material-ui/core/Icon";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import * as React from "react";
import { LoadDialog, LoadDialogProps } from "./persistence/LoadDialog";
import { TextFieldDialog } from "./uiComponents/TextFieldDialog";

/**
 * GameBar properties
 */
export interface GameBarProps {
    /**
     * Title to show
     */
    title: string;
    /**
     * Game persister
     */
    slots: SlotDescriptions;
    /**
     * Load of the game requested
     * @param id - Id of the save slot
     */
    onLoadGame(id: string): void;
    /**
     * Restart of the game requested
     */
    onRestartGame(): void;
    /**
     * Save of the game requested
     */
    onSaveGame(name: string): void;
    /**
     * Delete of save slot requested
     * @param id - Id of the save slot
     */
    onDeleteGame(id: string): void;
}

/**
 * Internal state of GameBar
 */
export interface GameBarState {
    /**
     * Where should the menu be visible
     */
    anchorEl: HTMLElement;
    /**
     * Should the load dialog be visible?
     */
    showLoad: boolean;
    /**
     * Should the save dialog be visible?
     */
    showSave: boolean;
}

/**
 * Predefined GameBar
 */
export class GameBar extends React.Component<GameBarProps, GameBarState> {

    /**
     * Creates new GameBar
     */
    public constructor(props: GameBarProps) {
        super(props);
        this.state = {
            anchorEl: null,
            showLoad: false,
            showSave: false
        };
    }

    /**
     * Renders the GameBar
     */
    public render() {
        return (
            <AppBar>
                <Toolbar>
                    <IconButton onClick={e => this.showMenu(e)}><Icon>menu</Icon></IconButton>
                    <Typography variant="title">{this.props.title}</Typography>
                </Toolbar>
                <Menu
                    id="mainMenu"
                    open={Boolean(this.state.anchorEl)}
                    anchorEl={this.state.anchorEl}
                    onClose={() => this.hideMenu()}
                >
                    <MenuItem onClick={() => this.newGame()}>New game</MenuItem>
                    <MenuItem onClick={() => this.showLoadDialog()}>Load saved game</MenuItem>
                    <MenuItem onClick={() => this.showSaveDialog()}>Save game</MenuItem>
                </Menu>
                <LoadDialog
                    slots={this.props.slots}
                    onLoadGame={id => this.loadGame(id)}
                    onDeleteGame={id => this.deleteGame(id)}
                    open={this.state.showLoad}
                    onClose={() => this.hideLoadDialog()} />
                <TextFieldDialog
                    open={this.state.showSave}
                    caption="Save game"
                    label="Name the save"
                    onCancel={() => this.hideSaveDialog()}
                    onTextSet={t => this.save(t)} />
            </AppBar>
        );
    }

    private showMenu(e: React.MouseEvent<HTMLElement>) {
        this.setState({ anchorEl: e.target as HTMLElement });
    }

    private hideMenu() {
        this.setState({ anchorEl: null });
    }

    private newGame() {
        this.hideMenu();
        this.props.onRestartGame();
    }

    private showLoadDialog() {
        this.setState({
            anchorEl: null,
            showLoad: true
        });
    }

    private hideLoadDialog() {
        this.setState({ showLoad: false });
    }

    private loadGame(id: string) {
        this.hideLoadDialog();
        this.props.onLoadGame(id);
    }

    private deleteGame(id: string) {
        this.props.onDeleteGame(id);
    }

    private showSaveDialog() {
        this.setState({
            anchorEl: null,
            showSave: true
        });
    }

    private hideSaveDialog() {
        this.setState({ showSave: false });
    }

    private save(name: string) {
        this.hideSaveDialog();
        this.props.onSaveGame(name);
    }
}
