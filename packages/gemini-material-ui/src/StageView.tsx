/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { getActiveStage } from "@galeanne-thorn/gemini-core";
import { getPlayerState, getRoomState } from "@galeanne-thorn/gemini-rpg";
import * as React from "react";
import { ActionViewProps } from "./CommonProps";
import { RoomView } from "./RoomView";
import { StageViewProvider } from "./StageViewProvider";

/**
 * StageView properties
 */
export interface StageContainerProps extends ActionViewProps {
    /**
     * Stage view provider
     */
    stageViewProvider: StageViewProvider;
}

/**
 * Stage view
 */
export class StageView extends React.Component<StageContainerProps> {

    /**
     * Creates the component
     * @param props Component properties
     */
    public constructor(props: StageContainerProps) {
        super(props, {});
    }

    /**
     * Renders the component
     */
    public render() {
        const stage = getActiveStage(this.props.gs);
        if (stage) {
            return this.props.stageViewProvider.renderStageView(
                stage.type,
                {
                    gs: this.props.gs,
                    catalogs: this.props.catalogs,
                    perform: this.props.perform,
                    stage
                });
        }
        else {
            const playerState = getPlayerState(this.props.gs);
            const roomState = getRoomState(this.props.gs, playerState.room);
            return <RoomView
                gs={this.props.gs}
                catalogs={this.props.catalogs}
                perform={this.props.perform}
                roomState={roomState} />;
        }
    }
}
