/**
 * Module Gemini UI Subsystem
 *
 * Copyright © 2018 Galeanne Thorn
 * License: MIT
 */

/**
 * Gets text from the UI data
 * @param ui - UI data
 * @returns Text or default value
 */
export function getUiText(ui: any, textProperty: string = "text", notFound?: string): string {

    const text = (typeof (ui) === "string") ? ui : ui[textProperty];
    if (text) return text;
    if (notFound) return notFound;
    return `There is no property ${textProperty} in ${ui}`;
}
