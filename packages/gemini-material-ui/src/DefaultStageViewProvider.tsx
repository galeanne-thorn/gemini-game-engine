/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Catalog } from "@galeanne-thorn/gemini-core";
import { TYPE_SCENE } from "@galeanne-thorn/gemini-rpg";
import * as React from "react";
import { SceneView } from "./SceneView";
import { StageComponent, StageProps, StageViewProvider, StatelessStageComponent } from "./StageViewProvider";

/**
 * Stage view provider class
 */
export class DefaultStageViewProvider implements StageViewProvider {

    private views: Catalog<StageComponent | StatelessStageComponent> = {
        [TYPE_SCENE]: SceneView
    };

    /**
     * Renders the stage
     *
     * @param type - Type of the stage
     * @param props - React props for the component
     */
    public renderStageView(type: string, props: StageProps): JSX.Element | null | false {

        const component = this.views[type];
        if (component) {
            return React.createElement(component, { ...props }, null);
        }
        return <div>Component for stage type {type} is not registered</div>;
    }

    /**
     *
     * @param type - Type of the stage
     * @param component - Component that should be used to render this type of stage
     */
    public register(type: string, component: StageComponent): this {
        this.views[type] = component;
        return this;
    }
}

/**
 * Default instance of DefaultStageViewProvider
 */
export const defaultStageViewProvider = new DefaultStageViewProvider();
