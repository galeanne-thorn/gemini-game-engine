/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { StatePointer } from "@galeanne-thorn/gemini-core";
import { Component } from "react";
import { ActionViewProps } from "./CommonProps";

/**
 * Props for any StageView Component
 */
export interface StageProps extends ActionViewProps {
    /**
     * Current stage
     */
    stage: StatePointer;
}

/**
 * Constructor function for StageViewComponent
 */
export interface StageComponent<P = {}, S = {}> {
    new(props: StageProps): React.Component<StageProps & P, S>;
}

/**
 * Stateless Stage Component interface
 */
export type StatelessStageComponent = (props: StageProps) => JSX.Element;

/**
 * Stage view provider interface
 */
export interface StageViewProvider {
    renderStageView(type: string, props: StageProps): JSX.Element | null | false;
}
