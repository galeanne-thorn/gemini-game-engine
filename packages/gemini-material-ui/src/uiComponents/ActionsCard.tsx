/**
 * @galeanne-thorn/gemini-material-ui
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { ChoiceGroup } from "@galeanne-thorn/gemini-core";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import * as React from "react";
import { ButtonChoiceSelector } from "../ButtonChoiceSelector";
import { ActionProps } from "../CommonProps";

/**
 * ButtonCardActions
 */
export interface ButtonCardActionsProps extends ActionProps {
    /**
     * Choice groups
     */
    choices: ChoiceGroup;
}

/**
 * ButtonCardActions component
 */
export function ButtonCardActions(props: ButtonCardActionsProps) {
    return (<CardActions>
        <ButtonChoiceSelector perform={props.perform} choiceGroup={props.choices} />
    </CardActions>
    );
}

/**
 * Actions Card properties
 *
 * Content has to be specified via children
 */
export interface ActionsCardProps extends ButtonCardActionsProps {
    /**
     * Card title
     */
    title: string;
    /**
     * Children
     */
    children?: any;
}

/**
 * ItemView statelesss component
 */
export function ActionsCard(props: ActionsCardProps) {
    return (
        <Card>
            <CardHeader title={props.title} />
            <CardContent>
                {props.children}
            </CardContent>
            <ButtonCardActions perform={props.perform} choices={props.choices} />
        </Card>
    );
}
