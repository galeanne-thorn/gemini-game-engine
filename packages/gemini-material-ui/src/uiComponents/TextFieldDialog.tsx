/**
 * @galeanne-thorn/gemini-material-ui
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import Button from "@material-ui/core/Button";
import Dialog, { DialogProps } from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import * as React from "react";

/**
 * Properties for the SaveDialog component
 */
export interface TextFieldDialogProps extends DialogProps {
    /**
     * Caption
     */
    caption: string;
    /**
     * Label
     */
    label: string;
    /**
     * OK button text
     */
    okLabel?: string;
    /**
     * Callback when text was confirmed
     */
    onTextSet(text: string): void;
    /**
     * Callback when dialog was cancelled
     */
    onCancel(): void;
}

export interface TextFieldDialogState {
    /**
     * Entered text
     */
    text: string;
}

/**
 * Generic text field dialog
 */
export class TextFieldDialog extends React.Component<TextFieldDialogProps, TextFieldDialogState> {

    /**
     * Creates new SaveDialog
     */
    public constructor(props: TextFieldDialogProps) {
        super(props);
        this.state = {
            text: ""
        };
    }

    /**
     * Renders the component
     */
    public render() {
        const {
            caption,
            label,
            okLabel,
            onTextSet,
            onCancel,
            ...dialogProps
        } = this.props;
        return (
            <Dialog {...dialogProps}>
                <DialogTitle>{caption}</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        required
                        margin="dense"
                        id="text"
                        label={label}
                        value={this.state.text}
                        onChange={e => this.updateText(e)}
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button color="primary"
                        onClick={() => this.props.onTextSet(this.state.text)}>{okLabel || "OK"}</Button>
                    <Button color="secondary"
                        onClick={this.props.onCancel}>Cancel</Button>
                </DialogActions>
            </Dialog>
        );
    }

    private updateText(e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>) {
        this.setState({
            text: e.target.value
        });
    }
}
