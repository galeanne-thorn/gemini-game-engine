/**
 * @galeanne-thorn/gemini-material-ui
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import Typography from "@material-ui/core/Typography";
import * as React from "react";
import { getUiText } from "../UiTools";

/**
 * Properties for UiView
 */
export interface UiViewProps {
    /**
     * UI data
     */
    ui: any;
    /**
     * Title field (optional)
     */
    titleField?: string;
}

/**
 * Simple UiData view
 */
export function UiView(props: UiViewProps) {
    // If ui is null, do not render anything
    if (!props.ui) return null;

    const title = getUiText(props.ui, props.titleField || "title");
    const text = getUiText(props.ui);
    // TODO: Show image if available
    return (
        <>
            <Typography variant="title">{title}</Typography>
            <Typography variant="body1">{text}</Typography>
        </>
    );
}
