import { shallow } from "enzyme";
import "jest";
import * as React from "react";
import { Text } from "../";

describe("Test", () => {
    it("renders text in div", () => {
        const wrapper = shallow(<Text gs={null} catalogs={null} text="Some simple text" />);

        expect(wrapper.html()).toBe("<div><p>Some simple text</p>\n</div>");
    });
});
