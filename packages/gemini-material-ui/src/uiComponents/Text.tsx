/**
 * @galeanne-thorn/gemini-material-ui
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { getHtmlText } from "@galeanne-thorn/gemini-text";
import * as React from "react";
import { ViewProps } from "../";

/**
 * Text component properties
 */
export interface TextProps extends ViewProps {
    /**
     * Text to show
     */
    text: string;
}

/**
 * Simple text display
 */
export const Text: React.SFC<TextProps> = props => {
    const html = {
        __html: getHtmlText(props.gs, props.catalogs, props.text)
    };
    return (
        <div dangerouslySetInnerHTML={html}></div>
    );
};
