/**
 * @galeanne-thorn/gemini-material-ui
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { SlotDescription } from "@galeanne-thorn/gemini-persistence";
import Icon from "@material-ui/core/Icon";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import * as React from "react";

export type Callback = () => void;

/**
 * SaveSlot properties
 */
export interface SaveSlotProps {
    /** Data to show */
    description: SlotDescription;
    /** Load callback */
    onLoad: Callback;
    /** Delete callback */
    onDelete?: Callback;
}

/**
 * SaveSlot component
 */
export const SaveSlot = (props: SaveSlotProps) => {

    const deleteAction = props.onDelete
        ? (
            <ListItemSecondaryAction>
                <Icon onClick={props.onDelete}>delete</Icon>
            </ListItemSecondaryAction>
        )
        : null;

    return (
        <ListItem>
            <ListItemText onClick={props.onLoad}
                primary={props.description.name}
                secondary={props.description.date.toLocaleString()}
            />
            {deleteAction}
        </ListItem>
    );
};
