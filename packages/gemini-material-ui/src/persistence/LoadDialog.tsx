/**
 * @galeanne-thorn/gemini-material-ui
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { GameState } from "@galeanne-thorn/gemini-core";
import { SlotDescriptions } from "@galeanne-thorn/gemini-persistence";
import Dialog, { DialogProps } from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import List from "@material-ui/core/List";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import * as React from "react";
import { SaveSlot } from "./SaveSlot";

/** Properties for the auto save slot list */
export interface LoadDialogProps extends DialogProps {
    /**
     * Game persister
     */
    slots: SlotDescriptions;
    /**
     * Request to load game
     */
    onLoadGame(slotId: string): void;
    /**
     * Request to delete saved game
     */
    onDeleteGame(slotId: string): void;
}

export interface LoadDialogState {
    /**
     * Tab index
     */
    tabIndex: number;
}

/**
 * List of autosave slots
 */
export class LoadDialog extends React.Component<LoadDialogProps, LoadDialogState> {

    /** Creates new LoadDialog */
    public constructor(props: LoadDialogProps) {
        super(props);
        this.state = {
            tabIndex: 0
        };
    }

    /**
     * Renders the component
     */
    public render() {
        const { slots, onLoadGame, onDeleteGame, ...dialogProps } = this.props;

        const userSlots = slots.user
            .map(s =>
                <SaveSlot
                    key={"save-" + s.id}
                    description={s}
                    onLoad={() => this.props.onLoadGame(s.id)}
                    onDelete={() => this.props.onDeleteGame(s.id)}
                />);
        const autoSlots = slots.auto
            .map(s =>
                <SaveSlot
                    key={"autosave-" + s.id}
                    description={s}
                    onLoad={() => this.props.onLoadGame(s.id)}
                />);

        return (
            <Dialog {...dialogProps}>
                <DialogTitle>Load saved game</DialogTitle>
                <DialogContent>
                    <Tabs
                        value={this.state.tabIndex}
                        onChange={(e, v) => this.tabChange(v)}
                        fullWidth>
                        <Tab label="Saves" />
                        <Tab label="Autosaves" />\
                        </Tabs>
                    <List>
                        {this.state.tabIndex === 0 && userSlots}
                        {this.state.tabIndex === 1 && autoSlots}
                    </List>
                </DialogContent>
            </Dialog >
        );
    }

    private tabChange(value) {
        this.setState({
            tabIndex: value
        });
    }
}
