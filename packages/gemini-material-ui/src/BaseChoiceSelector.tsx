/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { ChoiceGroup } from "@galeanne-thorn/gemini-core";
import * as React from "react";
import { ActionProps } from "./CommonProps";

/**
 * Properties for ButtonChoiceSelector
 */
export interface ChoiceSelectorProps extends ActionProps {
    /**
     * Choice group to show
     */
    choiceGroup: ChoiceGroup;
}

/**
 * Choice selector component state
 */
export interface ChoiceSelectorState {
    parents: ChoiceGroup[];
    current: ChoiceGroup;
}

/**
 * Creates array of buttons based on the Choices
 */
export abstract class BaseChoiceSelector extends React.Component<ChoiceSelectorProps, ChoiceSelectorState> {

    /**
     * Creates new selector
     */
    public constructor(props: ChoiceSelectorProps) {
        super(props);
        // Evaluate choices
        this.state = {
            parents: [],
            current: this.props.choiceGroup
        };
    }

    /**
     * Properties are about to be updated
     */
    public componentWillReceiveProps(nextProps: ChoiceSelectorProps) {
        if (this.props.choiceGroup !== nextProps.choiceGroup) {
            this.setState({
                parents: [],
                current: nextProps.choiceGroup
            });
        }
    }

    /**
     * Renders the selector
     */
    public abstract render(): JSX.Element | null | false;

    protected moveDown(choiceGroup: ChoiceGroup) {
        this.setState({
            parents: [...this.state.parents, this.state.current],
            current: choiceGroup
        });
    }

    protected moveUp() {
        const length = this.state.parents.length;
        if (length > 0) {
            // Move to previous choice group
            this.setState({
                parents: [...this.state.parents.slice(0, -1)],
                current: this.state.parents[length - 1]
            });
        }
    }

}
