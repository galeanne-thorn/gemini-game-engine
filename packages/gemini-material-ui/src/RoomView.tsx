/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import {
    ChoiceDefinition, ChoiceGroup, evaluateChoices, GameCatalogs, GameState,
    getObjectState, TemplateCatalog
} from "@galeanne-thorn/gemini-core";
import { getItemState, Item, Room, RoomState, SetPlayerRoom, TakeItem, TYPE_ITEM } from "@galeanne-thorn/gemini-rpg";
import { getHtmlText } from "@galeanne-thorn/gemini-text";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import Typography from "@material-ui/core/Typography";
import * as React from "react";
import { ButtonChoiceSelector } from "./ButtonChoiceSelector";
import { ActionViewProps } from "./CommonProps";
import { ItemView } from "./ItemView";
import { StageProps } from "./StageViewProvider";
import { ButtonCardActions } from "./uiComponents/ActionsCard";
import { Text } from "./uiComponents/Text";
import { UiView } from "./uiComponents/UiView";
import { getUiText } from "./UiTools";

/**
 * Props for Room component
 */
export interface RoomProps extends ActionViewProps {
    /**
     * Room state to render
     */
    roomState: RoomState;
}

export interface RoomViewState {
    /**
     * Current item
     */
    currentItem: string;
}
/**
 * React component for displaying Room
 */
export class RoomView extends React.Component<RoomProps, RoomViewState> {

    /**
     * Creates new RoomView
     * @param props Component properties
     */
    public constructor(props: RoomProps) {
        super(props);
        this.state = {
            currentItem: null
        };
    }

    /**
     * Renders the Scene
     */
    public render() {
        const room = this.props.catalogs.templates.getTemplate<Room>(this.props.roomState);

        const title = (typeof room.ui === "string") ? room.id : room.ui.title;
        const exitChoices = this.getExitChoices(this.props.gs, this.props.catalogs, room);

        const items = this.props.roomState.items.map(itemId => (
            <Button onClick={() => this.showItem(itemId)} key={itemId}>
                {getUiText(
                    this.props.catalogs.templates.getTemplate<Item>(getItemState(this.props.gs, itemId)).ui,
                    "name"
                )}
            </Button>)
        );

        let itemUi = null;
        if (this.state.currentItem) {
            itemUi = this.props.catalogs.templates.getTemplate<Item>(
                getItemState(this.props.gs, this.state.currentItem)
            ).ui;
        }

        return (
            <>
                <Dialog
                    open={!!this.state.currentItem}
                    onClose={() => this.hideItem()}
                >
                    <DialogContent>
                        <UiView ui={itemUi} />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.takeItem()}>Take</Button>
                        <Button onClick={() => this.hideItem()}>Close</Button>
                    </DialogActions>
                </Dialog>
                <Card>
                    <CardHeader title={title} />
                    <CardContent>
                        <Text text={getUiText(room.ui)} {...this.props} />
                    </CardContent>
                    <Typography variant="caption" align="center">Items</Typography>
                    {items}
                    <Typography variant="caption" align="center">Exits:</Typography>
                    <ButtonCardActions perform={this.props.perform} choices={exitChoices} />
                </Card>
            </>
        );
    }

    /**
     * Gets exit choices
     * @param gs - Game state
     * @param catalogs - Game catalogs
     * @param room - Room template
     */
    private getExitChoices(gs: GameState, catalogs: GameCatalogs, room: Room): ChoiceGroup {
        // Room exits
        const exitChoices = room.exits.map<ChoiceDefinition>(e => {
            return {
                action: SetPlayerRoom(e.target),
                visible: e.visible,
                enabled: e.enabled,
                ui: e.ui
            };
        });
        return evaluateChoices(gs, catalogs, exitChoices);
    }

    /**
     * Sets view to item
     * @param itemId - Id of the item in room inventory
     */
    private showItem(itemId: string) {
        this.setState({
            currentItem: itemId
        });
    }

    /**
     * Clear item view
     */
    private hideItem() {
        this.setState({
            currentItem: null
        });
    }

    /**
     * Perform TakeItem
     */
    private takeItem() {
        this.props.perform(TakeItem(this.state.currentItem));
        this.hideItem();
    }
}
