/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Choice, ChoiceGroup } from "@galeanne-thorn/gemini-core";
import Button from "@material-ui/core/Button";
import { hasIn } from "lodash";
import * as React from "react";
import { BaseChoiceSelector, ChoiceSelectorProps } from "./BaseChoiceSelector";
import { getUiText } from "./UiTools";

/**
 * Creates array of buttons based on the Choices
 */
export class ButtonChoiceSelector extends BaseChoiceSelector {

    /**
     * Creates new selector
     */
    public constructor(props: ChoiceSelectorProps) {
        super(props);
    }

    /**
     * Renders the selector
     */
    public render(): JSX.Element | null | false {

        const keyBase = `${this.state.parents.length}-`;
        const choiceButtons = this.state.current.choices.map((ch, i) => {
            const text = getUiText(ch.ui, "text", "?");
            if (hasIn(ch, "action")) {
                const chi = ch as Choice;
                return <Button
                    key={keyBase + i}
                    disabled={!chi.enabled}
                    onClick={() => this.props.perform(chi.action)}>
                    {text}
                </Button>;
            }
            else {
                const chi = ch as ChoiceGroup;
                return <Button
                    key={keyBase + i}
                    disabled={!chi.enabled}
                    onClick={() => this.moveDown(chi)}>
                    {text}
                </Button>;
            }
        });

        if (this.state.parents.length > 0) {
            // Prepend the back button
            choiceButtons.unshift(
                <Button key={"back"} onClick={() => this.moveUp()}>{"<< Back <<"}</Button>
            );
        }

        return <div>{choiceButtons}</div>;
    }
}
