/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { ChoiceGroup, evaluateChoices, getObjectState, TemplateCatalog } from "@galeanne-thorn/gemini-core";
import { getItemState, Item, ItemState } from "@galeanne-thorn/gemini-rpg";
import { getHtmlText } from "@galeanne-thorn/gemini-text";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import Typography from "@material-ui/core/Typography";
import * as React from "react";
import { ButtonChoiceSelector } from "./ButtonChoiceSelector";
import { ActionProps, ActionViewProps } from "./CommonProps";
import { ActionsCard } from "./uiComponents/ActionsCard";
import { Text } from "./uiComponents/Text";
import { getUiText } from "./UiTools";

/**
 * ItemView Container properties
 */
export interface ItemViewProps extends ActionViewProps {
    /**
     * Item to view
     */
    itemId: string;
}

/**
 * Item View
 */
export class ItemView extends React.Component<ItemViewProps> {
    /**
     * Renders the component
     */
    public render() {
        if (!this.props.itemId) return null;

        const state = getItemState(this.props.gs, this.props.itemId);
        const template = this.props.catalogs.templates.getTemplate<Item>(state);

        const title = (typeof (template.ui) === "string") ? template.id : template.ui.name;
        const description = getUiText(template.ui);
        const itemChoices = evaluateChoices(this.props.gs, this.props.catalogs, template.choices);
        return (
            <ActionsCard title={title} choices={itemChoices} perform={this.props.perform}>
                <Text text={description} {...this.props} />
            </ActionsCard>
        );
    }
}
