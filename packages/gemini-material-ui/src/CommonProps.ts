/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { ActionGroup, GameCatalogs, GameState } from "@galeanne-thorn/gemini-core";

/**
 * Props for view-only components
 */
export interface ViewProps {
    /**
     * Game state
     */
    gs: GameState;
    /**
     * Game catalogs
     */
    catalogs: GameCatalogs;
}

/**
 * Props for components that can perform game action
 */
export interface ActionProps {
    /**
     * Callback that performs the game action
     */
    perform(action: ActionGroup): void;
}

/**
 * Combined action and view props
 */
export type ActionViewProps = ViewProps & ActionProps;
