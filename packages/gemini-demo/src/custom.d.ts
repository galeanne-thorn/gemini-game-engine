/** Webpack require function */
//declare function require<T = any>(name: string): T;

/** Allow Webpack to load md as string */
declare module "*.md" {
    const content: string;
    export default content;
}