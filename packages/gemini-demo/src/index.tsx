/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import * as React from "react";
import * as ReactDOM from "react-dom";
import { AppContainer } from "react-hot-loader";

import App from "./App";

// Init game
import { game, persister, startGame } from "./game/game";

declare const module: any;

const rootElement = document.getElementById("root");

const render = Component =>
  ReactDOM.render(
    <AppContainer>
      <Component game={game} persister={persister} />
    </AppContainer>,
    rootElement
  );

startGame();
render(App);
if (module.hot) module.hot.accept("./App", () => render(App));
