/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { ActionGroup, Game, GameState } from "@galeanne-thorn/gemini-core";
import {
    ActionViewProps, defaultStageViewProvider, GameBar, StageView
} from "@galeanne-thorn/gemini-material-ui";
import { GamePersister, SlotDescriptions } from "@galeanne-thorn/gemini-persistence";
import List from "@material-ui/core/List";
import * as React from "react";
import { startGame } from "./game/game";

/**
 * Application properties
 */
export interface AppProperties {
    /**
     * Game to show
     */
    game: Game;
    /**
     * Autosave persister
     */
    persister: GamePersister;
}

export interface AppState {
    /** Game state */
    gs: GameState;
    /** Save slot information */
    slots: SlotDescriptions;
}

/**
 * Application component
 */
export default class App extends React.Component<AppProperties, AppState> {

    /**
     * Creates new prop
     * @param props
     */
    public constructor(props: AppProperties) {
        super(props);
        this.state = {
            gs: this.props.game.state,
            slots: this.props.persister.getSaveSlots()
        };
    }

    public render() {
        return (
            <div>
                <GameBar
                    title="Gemini Game Engine Demo"
                    slots={this.state.slots}
                    onRestartGame={() => this.restart()}
                    onLoadGame={id => this.load(id)}
                    onDeleteGame={id => this.delete(id)}
                    onSaveGame={t => this.save(t)} />
                <StageView
                    stageViewProvider={defaultStageViewProvider}
                    gs={this.state.gs}
                    catalogs={this.props.game.game}
                    perform={this.perform} />
            </div>);
    }

    /**
     * Performs game action and sets new state
     */
    private perform = (action: ActionGroup) => {
        this.setState({
            gs: this.props.game.perform(action)
        });
    }

    /**
     * Restart the game
     */
    private restart() {
        this.setState({
            gs: startGame()
        });
    }

    /**
     * Load the game selected in load dialog
     * @param id Id of the slot
     */
    private load(id: string) {
        this.setState({
            gs: this.props.game.load(this.props.persister.load(id))
        });
    }

    /**
     * Deletes saved game
     * @param id Id of the slot
     */
    private delete(id: string) {
        this.props.persister.clear(id);
        this.setState({
            slots: this.props.persister.getSaveSlots()
        });
    }

    /**
     * Save game
     * @param name Name to use as save slot description
     */
    private save(name: string) {
        this.props.persister.save(this.props.game.state, name);
        this.setState({
            slots: this.props.persister.getSaveSlots()
        });
    }
}
