---
type: scene
id: start
ui:
    title: "Start"
choices:
    - ui: next
      action:
        type: SET_SCENE
        sceneId: next
    - ui:
        text: submenu
      choices:
        - ui:
            text: sub-submenu
          choices:
            - ui:
                text: item in sub-sub menu
              action:
                type: SET_SCENE
                sceneId: next
        - ui:
            text: item in sub-menu
          action:
            type: SET_SCENE
            sceneId: next
---
# Gemini Game Engine Demo

This is *demo* of **Gemini Game Engine**