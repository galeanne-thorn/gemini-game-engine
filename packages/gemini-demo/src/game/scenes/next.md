---
type: scene
id: next
ui:
    title: "Next Scene"
choices:
    - ui: Continue
      action:
        - type: SET_PLAYER_ROOM
          roomId: startRoom
        - type: CLEAR_STAGES
---
This is next scene