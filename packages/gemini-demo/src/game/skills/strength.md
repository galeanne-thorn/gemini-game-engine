---
type: skill
id: STR
group: ATTR
difficulty: 2 # Difficulty.medium,
ui:
    name: Strength
---
Determines how strong the person is