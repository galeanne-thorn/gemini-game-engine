---
type: skill
id: INT
group: ATTR
difficulty: 2 # Difficulty.medium,
ui:
    name: Intelligence
---
Determines how clever the person is