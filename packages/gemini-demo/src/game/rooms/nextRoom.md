---
type: room
id: nextRoom
ui:
    title: Other Room
exits:
    - target: startRoom
      ui: First room
---
This is the second room you have visited in this game.