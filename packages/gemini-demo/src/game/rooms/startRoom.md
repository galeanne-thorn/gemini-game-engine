---
type: room
id: startRoom
ui:
    title: Start Room
exits:
    - target: nextRoom
      ui: Next room
items:
    - KeyNext
---
This is the first room you have visited in this game.