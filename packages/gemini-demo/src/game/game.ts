
import { EmptyGameState, Game, GamePlugin, GameState } from "@galeanne-thorn/gemini-core";
import { ActionLogPlugin, AutoSavePlugin, GamePersister, LocalStorage } from "@galeanne-thorn/gemini-persistence";
import { PLAYER_PERSON, RpgPlugin, setScene, SetScene } from "@galeanne-thorn/gemini-rpg";

const demo: GamePlugin = {
    id: "demo",
    title: "Gemini Game Engine Demo",
    author: "Galeanne Thorn",

    templates: require("./game.json"),

    // Initialize game state by setting the start scene as active stage
    initializer: (gs, c) => setScene(gs, SetScene("start"), c)
};

// Initialize and export persister and autosave module
const storage = new LocalStorage("gemini-demo");
export const persister = new GamePersister(storage);
const autoSave = new AutoSavePlugin(persister);
const logger = new ActionLogPlugin(storage);

// Initialize game
// Note order of the plugins defines order of extenders processing the action
export const game = new Game([
    autoSave,
    logger,
    RpgPlugin,
    demo
]);

// Initial game state
export function startGame(): GameState {
    const initialState = {
        ...EmptyGameState,
        [PLAYER_PERSON]: "hero"
    };

    // Start game
    return game.start(initialState);
}
