var path = require('path');
var webpack = require('webpack');

module.exports = {
  mode: "development",
  entry: [
    './src/index.tsx'
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'static'),
    publicPath: '/static/'
  },
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.md$./,
        use: [
          'raw-loader'
        ]
      },
      {
        test: /\.tsx?$/,
        use: [
          'awesome-typescript-loader', 'babel-loader',
        ],
        exclude: [/node_modules/, /\.spec\.tsx?$/],
      }
    ],
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.json']
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
  ],
  optimization: {
    namedModules: true
  },
  devServer: {
    host: 'localhost',
    port: 3000,
    historyApiFallback: true,
    hot: true,
  },
};