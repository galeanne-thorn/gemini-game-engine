## Naming Conventions

### Actions

* Type Constants - ACTION_*name*
* Action interface - *Name*Action
* Action creation function - *Name*
* Action execution function - *name*

#### Example
```
const ACTION_SET_VARIABLE = "SET_VARIABLE";

interface SetVariableAction<T = any> extends Action {
  ...
}

function SetVariable<T = any>(key: string | string[], data: T): SetVariableAction<T> {
    ...
}

function setVariable(gs: GameState, action: SetVariableAction): GameState {
 ...
}

```

### Conditions

* Type Constants - COND_*name*
* Condition interface - *Name*Condition
* Condition creation function - *Name*
* Condition execution function - *name*

#### Example
```
const COND_AND = "AND";

interface AndCondition extends Condition {
  ...
}

function And(conditions: Condition[]): AndCondition {
    ...
}

function and(gs: GameState, condition: AndCondition, catalogs: GameCatalogs): boolean {
 ...
}

```