/**
 * Module Gemini Game Engine Core
 *
 * Copyright © 2017 Galeanne Thorn
 * License: MIT
 */

import { isArray } from "lodash";
import { Catalog } from "./Catalog";
import { CorePlugin, IdentityActionPerformer } from "./core/CorePlugin";
import {
    Action, ActionGroup, ActionPerformer, ActionPerformerExtender,
    GameCatalogs, GameStateInitializer, IGame
} from "./Definitions";
import { GamePlugin, mergePlugins } from "./GamePlugin";
import { EmptyGameState, GameState } from "./GameState";
import { ACTION_GAME_LOADED, ACTION_GAME_LOADING, ACTION_GAME_PERFORM } from "./InternalActions";
import { resolveReferences } from "./ReferenceResolver";

/**
 * Gets whether the action is (or contains) action of given type(s)
 *
 * @param action - Action group to test
 * @param type - Type or array of types to look for
 *
 * @returns True if any action is of given type
 */
export function isActionType(action: ActionGroup, type: string | string[]): boolean {
    const a = isArray(action) ? action : [action];
    const t = isArray(type) ? type : [type];
    return a.some(x => t.some(y => y === x.type));
}

const actionPerform: Action = { type: ACTION_GAME_PERFORM };

/**
 * Represents Gemini Game
 */
export class Game {

    /**
     * Game plugins
     */
    public readonly plugins: Catalog<GamePlugin> = {};

    private readonly catalogs: GameCatalogs;
    private initializers: GameStateInitializer[] = [];
    private performAction: ActionPerformer;
    private gameState: GameState = EmptyGameState;

    /**
     * Returns the current game state
     */
    public get state(): GameState {
        // Ideal would be to return immutable or deep copy
        // but for performance reasons we are not doing it.
        return this.gameState;
    }

    /**
     * Game representation
     */
    public get game(): IGame {
        return {
            ...this.catalogs,
            perform: (gs, a) => this.internalPerform(gs, a)
        };
    }

    /**
     * Creates new game
     *
     * @param plugins - Gemini Game Engine Plugins to use. Note there is no need to specify the core plugin
     * @param preperformExtenders - Special extenders
     */
    constructor(plugins: GamePlugin[] = []) {

        const extenders: ActionPerformerExtender[] = [];

        // Add CorePlugin to the list
        const allPlugins = [...plugins, CorePlugin];

        this.catalogs = mergePlugins(allPlugins);

        // Register types and methods
        allPlugins.forEach(p => {
            // Remember plugin
            this.plugins[p.id] = p;
            // Add initializer
            if (p.initializer) {
                this.initializers.push(p.initializer);
            }
            // Add extender
            if (p.extender) {
                extenders.push(p.extender);
            }
        });

        // Configure plugins
        plugins
            .filter(p => p.configure)
            .forEach(p => p.configure(this.plugins, this.catalogs));

        // Chain reducers (in reverse order)
        this.performAction = extenders
            .reduceRight<ActionPerformer>((r, p) => p(r), IdentityActionPerformer);
    }

    /**
     * Starts new game
     *
     * @param initialGameState - Optional initial game state. If not used, EmptyGameState is used
     * @returns - New game state.
     */
    public start(initialGameState: GameState = EmptyGameState): GameState {
        this.gameState = this.initializers.reduce((gs, init) => init(gs, this.game), initialGameState);
        return this.gameState;
    }

    /**
     * Sets new game state. Use this for loading saved games
     *
     * @param gs - Game State to load
     * @returns - Loaded game state.
     */
    public load(gs: GameState): GameState {
        this.performSystemAction(ACTION_GAME_LOADING);
        this.gameState = gs;
        return this.performSystemAction(ACTION_GAME_LOADED);
    }

    /**
     * Performs actions
     *
     * @param actions - Actions to perform
     * @returns - New game state
     */
    public perform(actions: ActionGroup): GameState {
        // If actions are null or undefined, do nothing
        if (!actions) return this.gameState;

        let initial: ActionGroup;
        if (isArray(actions)) {
            initial = [actionPerform, ...actions];
        }
        else {
            initial = [actionPerform, actions];
        }

        const newState = this.internalPerform(this.gameState, initial);

        // Test result
        if (newState.lastActionResult.success) {
            // If success, return new state
            this.gameState = newState;
        }
        else {
            // If failure, create new game state with only the result updated
            this.gameState = {
                ...this.gameState,
                lastActionResult: newState.lastActionResult
            };
        }
        return this.gameState;
    }

    /**
     * Performs the action internaly
     *
     * @param actions - Actions to perform
     * @returns - New game state
     */
    private internalPerform(gameState: GameState, actions: ActionGroup): GameState {
        if (!actions) return gameState;
        return this.performAction(gameState, this.resolveReferences(actions), this.game);
    }

    /**
     * Resolve reference
     * @param actions - Actions to resolve the references for
     */
    private resolveReferences(actions: ActionGroup): ActionGroup {
        return isArray(actions)
            ? actions.map(a => resolveReferences(this.gameState, this.catalogs, a))
            : resolveReferences(this.gameState, this.catalogs, actions);
    }

    /**
     * Performs system action
     *
     * @param type - Type of action to perform
     * @returns new game state
     */
    private performSystemAction(type: string): GameState {
        return this.internalPerform(this.gameState, { type });
    }
}
