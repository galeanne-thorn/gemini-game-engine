import "jest";
import { isArray } from "lodash";
import {
    ACTION_GAME_LOADED, ACTION_GAME_LOADING, ACTION_GAME_PERFORM,
    AlwaysConditionFunction,
    EmptyGameState, Game, GamePlugin, GameState,
    IdentityActionFunction, isActionType,
    ResultFail, ResultSuccess, Template
} from "./";
import { } from "./core/CorePlugin";

/**
 * Template implementation
 */
class TestTemplate extends Template {
    constructor(definition) {
        super("T", definition);
    }
}

const gamePlugin: GamePlugin = {
    id: "test",
    templateTypes: {
        T: TestTemplate
    },
    templates: {
        T: [
            { id: "T1", ui: "TestTemplate" }
        ]
    },
    actions: {
        A1: IdentityActionFunction
    },
    conditions: {
        C1: AlwaysConditionFunction
    },
    initializer: gs => ({ ...gs, variables: { ...gs.variables, initialized: true } })
};

const otherPlugin: GamePlugin = {
    id: "other",
    templates: {
        T: [
            { id: "T2", ui: "SecondTestTemplate" }
        ]
    }
};

describe("isActionType", () => {

    it("returns false if action is not of given type", () => {
        const result = isActionType({ type: "X" }, "Y");
        expect(result).toBe(false);
    });

    it("returns false if no action is of given types", () => {
        const result = isActionType(
            [{ type: "X" }, { type: "Y" }],
            ["A", "B"]
        );
        expect(result).toBe(false);
    });

    it("returns true if action is not of given type", () => {
        const result = isActionType({ type: "X" }, "X");
        expect(result).toBe(true);
    });

    it("returns true if some action is of given types", () => {
        const result = isActionType(
            [{ type: "X" }, { type: "Y" }],
            ["A", "Y"]
        );
        expect(result).toBe(true);
    });
});

describe("Game", () => {

    describe("constructor", () => {

        it("registers template types from module", () => {
            const game = new Game([gamePlugin]).game;
            expect(game.templates.getTemplate("T", "T1").type).toBe("T");
        });

        it("registers templates after all template types were registered", () => {
            const game = new Game([otherPlugin, gamePlugin]).game;
            expect(game.templates.getTemplate("T", "T1").id).toBe("T1");
            // Note otherPlugin depends on gamePlugin so it will not register the template T2
            // if the TemplateType will not get registered first
            expect(game.templates.getTemplate("T", "T2").id).toBe("T2");
        });

        it("adds actions from module", () => {
            const game = new Game([gamePlugin]).game;
            expect(game.actions.getAction("A1")).toBe(IdentityActionFunction);
        });

        it("adds conditions from module", () => {
            const game = new Game([gamePlugin]).game;
            expect(game.conditions.getCondition("C1")).toBe(AlwaysConditionFunction);
        });

        it.skip("applies extenders", () => fail());

        it("configures plugins", () => {
            let called = false;
            const plugin: GamePlugin = {
                id: "testConfigure",
                configure: (p, c) => { called = true; }
            };

            const game = new Game([plugin]);
            expect(called).toBe(true);
        });
    });

    describe("start", () => {

        it("sets initial game state correctly", () => {
            const initialState: GameState = {
                ...EmptyGameState,
                lastActionResult: ResultSuccess,
                data: "X"
            };

            const game = new Game();
            const state = game.start(initialState);

            expect(state).toEqual(initialState);
        });

        it("runs the game state initializers from plugins", () => {
            const game = new Game([gamePlugin]);
            const state = game.start(EmptyGameState);

            expect(state.variables.initialized).toBe(true);
        });
    });

    describe("load", () => {

        it("loads game state correctly", () => {
            const initialState: GameState = {
                ...EmptyGameState,
                lastActionResult: ResultSuccess,
                data: "X"
            };

            const game = new Game();
            const state = game.start(initialState);

            expect(state).toEqual(initialState);
        });

        it("performs the GAME_LOADING action", () => {
            let called = false;
            const plugin: GamePlugin = {
                id: "testLoad",
                extender: o => (gs, a, g) => {
                    if (!isArray(a) && a.type === ACTION_GAME_LOADING) called = true;
                    return o(gs, a, g);
                }
            };
            const game = new Game([plugin]);
            const state = game.load(EmptyGameState);

            expect(state).toEqual(EmptyGameState);
            expect(called).toBe(true);
        });

        it("performs the GAME_LOADED action", () => {
            let called = false;
            const plugin: GamePlugin = {
                id: "testLoad",
                extender: o => (gs, a, g) => {
                    if (!isArray(a) && a.type === ACTION_GAME_LOADED) called = true;
                    return o(gs, a, g);
                }
            };
            const game = new Game([plugin]);
            const state = game.load(EmptyGameState);

            expect(state).toEqual(EmptyGameState);
            expect(called).toBe(true);
        });
    });

    describe("perform", () => {

        it("does nothing if action is not defined", () => {
            const game = new Game();
            const oldState = game.start();
            const newState = game.perform(undefined);
            expect(newState).toBe(oldState);
        });

        it("includes the ACTION_GAME_PERFORM", () => {
            const game = new Game([
                {
                    id: "testPlugin",
                    actions: {
                        TEST: IdentityActionFunction
                    },
                    extender: o => (gs, a, g) => ({ ...gs, called: isActionType(a, ACTION_GAME_PERFORM) })
                }
            ]);

            game.start();
            const newState = game.perform({ type: "TEST" });
            expect(newState.called).toBeTruthy();
        });

        it("calls registered action", () => {
            const game = new Game([
                {
                    id: "testPlugin",
                    actions: {
                        TEST: (gs, a) => ({ ...gs, called: true })
                    }
                }
            ]);
            game.start();
            const newState = game.perform({ type: "TEST" });
            expect(newState.called).toBeTruthy();
        });

        it("changes only lastActionResult if action fails", () => {
            const ACTION_FAIL = "FAIL";
            const actionFailFunction =
                (gs, a) => ({ ...gs, lastActionResult: ResultFail(a.error), changedData: "z" });
            const createActionFail = (error: string) => {
                return {
                    type: ACTION_FAIL,
                    error
                };
            };

            const game = new Game([
                {
                    id: "testPlugin",
                    actions: {
                        FAIL: actionFailFunction
                    }
                }
            ]);

            const errorMessage = "Some error message";
            const action = createActionFail(errorMessage);

            const oldState = {
                ...EmptyGameState,
                lastActionResult: ResultSuccess,
                changedData: null
            };
            game.start(oldState);

            const newState = game.perform(action);

            expect(newState).toEqual({
                lastActionResult: {
                    success: false,
                    reason: errorMessage
                },
                variables: {},
                activeStages: [],
                objects: {},
                changedData: null
            });
        });

        it("performs all actions in the group", () => {
            const action1 = (gs, a) => ({ ...gs, test: 5 });
            const action2 = (gs, a) => ({ ...gs, test: gs.test * 10 });

            const game = new Game([
                {
                    id: "testPlugin",
                    actions: {
                        A1: action1,
                        A2: action2
                    }
                }
            ]);

            game.start();
            const newState = game.perform([{ type: "A1" }, { type: "A2" }]);

            expect(newState.test).toEqual(50);
        });
    });

});
