/**
 * Gemini Game Engine
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { forOwn } from "lodash";

/**
 * Interface for catalog (dictionary) of objects of type Template
 *
 * Key is string
 */
export interface Catalog<T> {
    [key: string]: T;
}

/**
 * Typed catalog
 */
export class SimpleCatalog<TValue = {}> {

    /** Internal catalog */
    public readonly I: Catalog<TValue> = {};

    /**
     * Registers new value
     *
     * @param key - Unique Key
     * @param value - Value
     */
    public add(key: string, value: TValue): void {
        if (!key) {
            throw new Error("Key must be specified");
        }
        if (!value) {
            throw new Error(`Value for ${key} must be specified`);
        }
        if (this.I[key]) {
            throw new Error(`Key ${key} is already registered`);
        }
        this.I[key] = value;
    }

    /**
     * Registers all values in given catalog using the keys from the catalog
     *
     * @param catalog Object with action functions
     */
    public addRange(catalog: Catalog<TValue>): void {
        forOwn(catalog, (value, key) => this.add(key, value));
    }

    /**
     * Gets item for given key.
     *
     * @param key - Key
     * @return Item stored under given key
     */
    public getItem(key: string): TValue {
        return this.I[key];
    }

    /**
     * Gets all registered keys
     */
    public listKeys(): string[] {
        return Object.keys(this.I).sort();
    }

}
