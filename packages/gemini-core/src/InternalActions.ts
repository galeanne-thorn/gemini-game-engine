/**
 * @galeanne-thorn/gemini-core
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

/**
 * Action used to signal game is about to update the game state
 * via the perform operation.
 * Note tha action does nothing itself.
 */
export const ACTION_GAME_PERFORM = "GAME_PERFORM";

/**
 * Action used to signal game state will change via load of persisted state
 * to game extenders
 * Note the action does nothing itself and cannot be used to load the game.
 * When the action is performed, the old state is present.
 */
export const ACTION_GAME_LOADING = "GAME_LOADING";

/**
 * Action used to signal game state changed via load of persisted state
 * to game extenders.
 * Note the action does nothing itself and cannot be used to load the game.
 * When the action is performed, the new game state is already loaded.
 */
export const ACTION_GAME_LOADED = "GAME_LOADED";
