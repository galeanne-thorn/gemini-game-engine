/**
 * Module Gemini Game Engine Core
 *
 * Copyright © 2017 Galeanne Thorn
 * License: MIT
 */

import { isString } from "lodash";

/**
 * Result of game action or condition
 */
export interface Result {
    /** Result of the condition or action */
    success: boolean;
    /** Optional reason description for the result */
    reason?: string | string[];
}

/**
 * Convenient constant to return simple success
 */
export const ResultSuccess: Result = { success: true };

/**
 * Function to easily create failure result with reason
 *
 * @param reason - description of the failure
 */
export function ResultFail(reason: string) {
    return { success: false, reason };
}

/**
 * Combines two results together
 *
 * Final success of the result is logical and of successes of each result
 * Final reason of the result is concatenation of the result reasons
 */
export function mergeResults(result1: Result, result2: Result): Result {
    return {
        success: result1.success && result2.success,
        reason: [
            ...(isString(result1.reason) ? [result1.reason] : result1.reason || []),
            ...(isString(result2.reason) ? [result2.reason] : result2.reason || [])
        ]
    };
}
