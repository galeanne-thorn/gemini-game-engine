/**
 * Gemini Game Engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { GameState } from "./GameState";

/**
 * Interface for object with id
 */
export interface Unique {
    /**
     * Unique Id of the object
     */
    id: string;
}

/**
 * Interface for object with type
 */
export interface Typed {
    /**
     * Type of the object
     */
    type: string;
}

/**
 * Interface for object with both type and id
 */
export type StatePointer = Unique & Typed;

/**
 * Interface for template pointer
 */
export interface TemplatePointer extends Typed {
    /**
     * Id of the template this pointer points to
     */
    templateId: string;
}

/**
 * Part of game object state that is created from some template.
 * ChildState is never stored directly in GameState but is part of some other state.
 * Think of skill of person.
 */
export type ChildState<TPayload extends object = object> = TemplatePointer & TPayload;

/**
 * Game object state - it is unique (has Id) and points to template
 *
 * This represents the dynamic part of game data.
 * It should not include methods, only pure data.
 */
export type ObjectState<TPayload extends object = object> = Unique & TemplatePointer & TPayload;

/**
 * Action that user can perform in game
 */
export interface Action {
    /**
     * Action type.
     * Determines the function that will modify the game state
     */
    type: string;
}

/**
 * Action group is either one or more actions
 */
export type ActionGroup = Action | Action[];

/**
 * Action function signature
 */
export type ActionFunction = (gs: GameState, action: Action, game: IGame) => GameState;

/**
 * Catalog of game actions
 */
export interface IActionCatalog {
    /**
     * Gets reducer for given action type
     *
     * @param actionOrActionType - Action or action type to get the action function for
     * @return Action function
     */
    getAction(actionOrActionType: Action | string): ActionFunction;
}

/**
 * Condition that user can perform in game
 */
export interface Condition {
    /**
     * Condition type.
     * Determines the function that will evaluate the condition
     */
    type: string;
}

/**
 * Condition function
 */
export type ConditionEvaluator = (gs: GameState, condition: Condition, catalogs: GameCatalogs) => boolean;

/**
 * Catalog of game conditions
 */
export interface IConditionCatalog {
    /**
     * Gets reducer for given Condition type
     *
     * @param conditionOrConditionType - Condition or condition type to get the condition evaluator for
     */
    getCondition(conditionOrConditionType: Condition | string): ConditionEvaluator;
    /**
     * Evaluates condition
     *
     * @param gs            - Game state
     * @param catalogs      - Game catalogs to use in the evaluation
     * @param condition     - Condition to evaluate
     * @param defaultResult - Value to return if function is not registered or condition is undefined
     *
     * @returns Value returned from registered condition function or defaultResult
     *          if condition function is not specified or not registered
     */
    evaluate(
        gs: GameState,
        catalogs: GameCatalogs,
        condition: Condition,
        defaultResult?: boolean
    ): boolean;
}

/**
 * Template definition.
 */
export interface TemplateDefinition<TPayload extends object = any> extends Unique {
    /**
     * Optional initial state of game object created from the template
     */
    initialState?: TPayload;
    /**
     * UI data
     */
    ui?: any;
}

/**
 * Template interface
 */
export interface ITemplate<TPayload extends object = any> extends Typed, TemplateDefinition<TPayload> {
    /**
     * Creates new GameObject derived from this template with given object id
     * @param catalogs - Game catalogs to use when creating state
     * @param id - id of the GameObject  to create. If not supplied, UUID type 4 is used
     * @returns new GameObject based on this template, with given id
     */
    createState(catalogs: GameCatalogs, id?: string): ObjectState<TPayload>;

    /**
     * Creates new ChildState derived from this template.
     * @param catalogs - Game catalogs to use when creating state
     * @returns new ChildState based on this template
     */
    createChildState(catalogs: GameCatalogs): ChildState<TPayload>;
}

/**
 * Catalog of templates
 */
export interface ITemplateCatalog {
    /**
     * Gets template from the catalog
     *
     * @param type - Type of the template or template pointer
     * @param id   - Id of the template. Required for template type, ignored for template pointer
     * @param gameObject - Game object its template you want
     *
     * @returns Template
     * @throws Error if template type of id is not found
     */
    getTemplate<TTemplate extends ITemplate>(type: string, id: string): TTemplate;
    getTemplate<TTemplate extends ITemplate>(templatePointer: TemplatePointer): TTemplate;
    getTemplate<TTemplate extends ITemplate>(
        typeOrPointer: string | TemplatePointer,
        id?: string): TTemplate;

    /**
     * Gets all registered templates of given type
     *
     * @param type - Type of templates to get
     *
     * @returns Array of templates of given type
     */
    getAllTemplates<TTemplate extends ITemplate>(type: string): TTemplate[];
}

/**
 * All game catalogs
 */
export interface GameCatalogs {
    /**
     * Registered templates
     */
    readonly templates: ITemplateCatalog;
    /**
     * Registered actions
     */
    readonly actions: IActionCatalog;
    /**
     * Registered conditions
     */
    readonly conditions: IConditionCatalog;
}

/**
 * Game interface including the perform action
 */
export interface IGame extends GameCatalogs {
    /**
     * Performs action
     */
    perform(gs: GameState, action: ActionGroup): GameState;
}

/**
 * GameStateInitializer signature.
 * GameStateInitializer is function that initializes the game state on the game start
 */
export type GameStateInitializer = (gs: GameState, game: IGame) => GameState;

/**
 * ActionPerformer signature.
 * ActionPerformer is function that takes ActionGroup and applies it to GameState
 *
 * @param gs - GameState to modify
 * @param action - Action to modify the GameState with
 * @param game - Game catalogs and perform function to use
 */
export type ActionPerformer = (gs: GameState, action: ActionGroup, game: IGame) => GameState;

/**
 * ActionPerformer extender signature
 * ActionPerformerExtender takes original ActionPerformer and returns new, extended one
 *
 * This allows to alter the action and/or action result during the processing chain
 *
 * @param original - Original action performer to be extended
 * @return - Extended game reducer
 *
 */
export type ActionPerformerExtender = (original: ActionPerformer) => ActionPerformer;
