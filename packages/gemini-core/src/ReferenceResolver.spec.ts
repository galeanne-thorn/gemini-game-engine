import "jest";
import { EmptyGameState, GameCatalogs, resolve, resolveReferences, TemplateCatalog } from "./";

const state = {
    ...EmptyGameState,
    objects: {
        room: {
            kitchen: {
                type: "room",
                id: "kitchen",
                templateId: "kitchen",
                name: "Kitchen"
            }
        }
    },
    variables: {
        data: {
            x: 5
        }
    }
};

const templates = new TemplateCatalog();
const catalogs: GameCatalogs = {
    templates,
    actions: null,
    conditions: null
};
const template = {
    id: "tmp",
    type: "test",
    data: "x",
    createState: () => null,
    createChildState: () => null
};
templates.add(template);

describe("resolve", () => {
    it("leaves text as is if it does not start with reference tag", () => {
        const text = "Some text";
        const result = resolve(null, null, text);
        expect(result).toEqual(text);
    });

    it("resolves variable reference", () => {
        const result = resolve(state, null, "$data.x");
        expect(result).toBe(5);
    });

    it("resolves object state reference", () => {
        const result = resolve(state, null, "@room.kitchen.name");
        expect(result).toBe("Kitchen");
    });

    it("resolves template reference", () => {
        const result = resolve(null, catalogs, "#test.tmp.data");
        expect(result).toBe("x");
    });
});

describe("resolveReferences", () => {
    it("resolves strings", () => {
        const result = resolveReferences(state, catalogs, "$data.x");
        expect(result).toBe(5);
    });

    it("resolves objects", () => {
        const test = {
            num: 4,
            var: "$data.x",
            state: "@room.kitchen.name",
            template: "#test.tmp.data",
            string: "text"
        };

        const result = resolveReferences(state, catalogs, test);

        expect(result.num).toBe(test.num);
        expect(result.var).toBe(5);
        expect(result.state).toBe("Kitchen");
        expect(result.template).toBe("x");
        expect(result.string).toBe(test.string);
    });

    it("resolves arrays", () => {
        const test = [4, "$data.x", "@room.kitchen.name", "#test.tmp.data", "text"];
        const result = resolveReferences(state, catalogs, test);

        expect(result).toMatchObject([4, 5, "Kitchen", "x", "text"]);
    });

    it("keeps functions", () => {
        const f = () => { throw new Error("Do not call me"); };
        expect(resolveReferences(null, null, f)).toBe(f);
    });

    it("keeps nulls and undefined", () => {
        expect(resolveReferences(null, null, null)).toBeNull();
        expect(resolveReferences(null, null, undefined)).toBeUndefined();
    });
});
