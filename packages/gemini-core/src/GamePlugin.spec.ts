// tslint:disable:max-classes-per-file
import "jest";
import { GamePlugin, Template } from ".";
import { mergePlugins } from "./GamePlugin"; // For some reason import from "./" does not work for mergePlugins

// tslint:disable-next-line:no-console
console.log(mergePlugins);

/**
 * Test Template Type 1
 */
class TestTemplate1 extends Template {
    constructor(definition) {
        super("Type1", definition);
    }
}

/**
 * Test Template Type 2
 */
class TestTemplate2 extends Template {
    constructor(definition) {
        super("Type2", definition);
    }
}

const plugin1: GamePlugin = {
    id: "p1",
    templateTypes: {
        Type1: TestTemplate1
    },
    templates: {
        Type1: [
            { id: "T1", ui: "TestTemplate" },
            { id: "T2", ui: "TestTemplate" }
        ]
    },
    actions: {
        P1_A: gs => gs
    },
    conditions: {
        P1_C: () => true
    }
};

const plugin2: GamePlugin = {
    id: "p2",
    templateTypes: {
        Type2: TestTemplate2
    },
    templates: {
        Type2: [
            { id: "T1", ui: "TestTemplate" }
        ]
    },
    actions: {
        P2_A: gs => gs
    },
    conditions: {
        P2_C: () => false
    }
};

describe("mergePlugins", () => {

    it("Should register actions from all plugins", () => {
        const c = mergePlugins([plugin1, plugin2]);
        expect(c.actions.getAction("P1_A")).toBe(plugin1.actions.P1_A);
        expect(c.actions.getAction("P2_A")).toBe(plugin2.actions.P2_A);
    });

    it("Should register conditions from all plugins", () => {
        const c = mergePlugins([plugin1, plugin2]);
        expect(c.conditions.getCondition("P1_C")).toBe(plugin1.conditions.P1_C);
        expect(c.conditions.getCondition("P2_C")).toBe(plugin2.conditions.P2_C);
    });

    it("Should register templates from all plugins", () => {
        const c = mergePlugins([plugin1, plugin2]);

        expect(c.templates.getTemplate("Type1", "T1").id).toBe("T1");
        expect(c.templates.getTemplate("Type1", "T1").type).toBe("Type1");

        expect(c.templates.getTemplate("Type1", "T2").id).toBe("T2");
        expect(c.templates.getTemplate("Type1", "T2").type).toBe("Type1");

        expect(c.templates.getTemplate("Type2", "T1").id).toBe("T1");
        expect(c.templates.getTemplate("Type2", "T1").type).toBe("Type2");
    });

    it("Should register templates only after template types from all plugins are registered", () => {
        const dependentPlugin: GamePlugin = {
            id: "p_dep",
            templates: {
                Type1: [
                    { id: "TD", ui: "Dependent" }
                ]
            }
        };
        const c = mergePlugins([dependentPlugin, plugin1]);
        expect(c.templates.getTemplate("Type1", "TD").id).toBe("TD");
        expect(c.templates.getTemplate("Type1", "TD").type).toBe("Type1");
    });

    it("Should throw for duplicate actions", () => {
        expect(() => mergePlugins([
            plugin1,
            { id: "dup", actions: { P1_A: gs => gs } }
        ]))
            .toThrowError("Key P1_A is already registered");
    });

    it("Should throw for duplicate conditions", () => {
        expect(() => mergePlugins([
            plugin1,
            { id: "dup", conditions: { P1_C: () => false } }
        ]))
            .toThrowError("Key P1_C is already registered");
    });

    it("Should throw for duplicate template types", () => {
        expect(() => mergePlugins([
            plugin1,
            { id: "dup", templateTypes: { Type1: null } }
        ]))
            .toThrowError("Template type Type1 is already defined in the catalog!");
    });

    it("Should throw for duplicate templates", () => {
        expect(() => mergePlugins([
            plugin1,
            { id: "dup", templates: { Type1: [{ id: "T1", ui: null }] } }
        ]))
            .toThrowError("Template of type Type1 with ID T1 is already defined in the catalog!");
    });

});
