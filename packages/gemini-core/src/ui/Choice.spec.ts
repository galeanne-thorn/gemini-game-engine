import "jest";

import { isArray } from "lodash";
import {
    ChoiceGroup, ChoiceGroupDefinitionItem,
    Condition, ConditionCatalog,
    EmptyGameState, evaluateChoices, GameCatalogs
} from "../";

const Always: Condition = { type: "ALWAYS " };
const Never: Condition = { type: "NEVER" };

const NullAction = { type: "NULL" };

const conditions = new ConditionCatalog();
const catalogs: GameCatalogs = {
    actions: null,
    templates: null,
    conditions
};

conditions.add(Always.type, (gs, c) => true);
conditions.add(Never.type, (gs, c) => false);

describe("evaluateChoices", () => {

    const definitions: ChoiceGroupDefinitionItem[] = [
        {
            ui: { text: "1" },
            enabled: Never,
            visible: Never,
            choices: [
                {
                    ui: { text: "1-1" },
                    enabled: Always,
                    visible: Always,
                    action: NullAction
                }
            ]
        },
        {
            ui: { text: "2" },
            enabled: Never,
            visible: Always,
            choices: [
                {
                    ui: { text: "2-1" },
                    enabled: Always,
                    visible: Never,
                    action: NullAction
                },
                {
                    ui: { text: "2-2" },
                    enabled: Always,
                    visible: Always,
                    action: NullAction
                }
            ]
        }
    ];

    it("removes invisible choices", () => {
        const choices = evaluateChoices(EmptyGameState, catalogs, definitions).choices;

        expect(choices).toBeDefined();
        expect(isArray(choices)).toBeTruthy();

        expect(choices.length).toBe(1);
        expect(choices[0].ui.text).toBe("2");

        const level1 = choices[0] as ChoiceGroup;
        expect(level1.choices.length).toBe(1);
        expect(level1.choices[0].ui.text).toBe("2-2");
    });

    it("evaluates enabled", () => {
        const choices = evaluateChoices(EmptyGameState, catalogs, definitions).choices;

        expect(choices[0].enabled).toBe(false);
        const level1 = choices[0] as ChoiceGroup;
        expect(level1.choices[0].enabled).toBe(true);
    });

    it("evaluates undefined visible to true", () => {
        const choices = evaluateChoices(EmptyGameState, catalogs, [{ ui: {}, action: null }]).choices;
        expect(choices.length).toBe(1);
    });

    it("evaluates undefined enabled to true", () => {
        const choices = evaluateChoices(EmptyGameState, catalogs, [{ ui: {}, action: null }]).choices;
        expect(choices[0].enabled).toBe(true);
    });
});
