/**
 * Module Gemini Stage Subsystem
 *
 * Copyright © 2017 Galeanne Thorn
 * License: MIT
 */

import { ActionGroup, Condition } from "../Definitions";

/**
 * Generic UI component
 */
export interface UiComponent {
    /**
     * Condition determining whether the component is enabled.
     * If not specified, evaluates to true
     */
    enabled?: Condition;
    /**
     * Condition determining whether the component is visible.
     * If not specified, evaluates to true
     */
    visible?: Condition;
    /**
     * UI specific data (text, image, etc.)
     */
    ui: any;
}

/**
 * Choice definition - internal representation of the choice
 */
export interface ChoiceDefinition extends UiComponent {
    /**
     * Action(s) to perform when choice is selected
     */
    action: ActionGroup;
}

/**
 * Item in the ChoiceGroup. It can be either Choice or another ChoiceGroup
 */
export type ChoiceGroupDefinitionItem = ChoiceDefinition | ChoiceDefinitionGroup;

/**
 * Choice definitions provider
 */
export interface ChoicesProvider {
    /**
     * Choices
     */
    choices: ChoiceGroupDefinitionItem[];
}

/**
 * Group of choices with UI name
 */
export type ChoiceDefinitionGroup = UiComponent & ChoicesProvider;
