/**
 * Module Gemini Stage Subsystem
 *
 * Copyright © 2017 Galeanne Thorn
 * License: MIT
 */

import { hasIn } from "lodash";
import { ActionGroup, GameCatalogs } from "../Definitions";
import { GameState } from "../GameState";
import { ChoiceDefinition, ChoiceDefinitionGroup, ChoiceGroupDefinitionItem } from "./ChoiceDefinition";

/**
 * Choice - internal representation of the choice
 */
export interface Choice {
    /**
     * Action(s) to perform when choice is selected
     */
    action: ActionGroup;
    /**
     * Is Choice enabled?
     */
    enabled: boolean;
    /**
     * UI data (text, image, etc.)
     */
    ui: any;
}

/**
 * Item in the ChoiceGroup. It can be either Choice or another ChoiceGroup
 */
export type ChoiceGroupItem = Choice | ChoiceGroup;

/**
 * Group of choices
 */
export interface ChoiceGroup {
    /**
     * Choice items in the group.
     * Note each choice can be either Choice or other ChoiceGroup
     */
    choices: ChoiceGroupItem[];
    /**
     * Is Choice group enabled?
     */
    enabled: boolean;
    /**
     * UI data (text, image, etc.)
     */
    ui: any;
}

/**
 * Converts choice definitions to choices
 *
 * @param gs - Game State used for condition evaluation
 * @param conditions - Condition catalog to use
 * @param choiceDefinitions - Definition of choices to convert
 *
 * @returns Root choice group. Filters out choices that should not be visible and evaluates the enabled conditions
 */
export function evaluateChoices(
    gs: GameState,
    catalogs: GameCatalogs,
    choiceDefinitions: ChoiceGroupDefinitionItem[]): ChoiceGroup {

    const result = choiceDefinitions
        .filter(d => catalogs.conditions.evaluate(gs, catalogs, d.visible, true))
        .map(d => {
            if (hasIn(d, "action")) {
                const dd = d as ChoiceDefinition;
                return {
                    ui: dd.ui,
                    action: dd.action,
                    enabled: catalogs.conditions.evaluate(gs, catalogs, dd.enabled, true)
                };
            }
            else {
                const dd = d as ChoiceDefinitionGroup;
                return {
                    ui: dd.ui,
                    choices: evaluateChoices(gs, catalogs, dd.choices).choices,
                    enabled: catalogs.conditions.evaluate(gs, catalogs, dd.enabled, true)
                };
            }
        });
    return {
        ui: null,
        enabled: true,
        choices: result
    };
}
