import "jest";

import { GameCatalogs, Template } from "../";

describe("Template", () => {

    const TemplateType = "type";
    const TemplateId = "templateId";
    const StateId = "Id";

    const catalogs: GameCatalogs = {
        actions: null,
        conditions: null,
        templates: null
    };

    const StateTemplate = {
        text: "some data",
        data: {
            x: 1
        }
    };

    const template = new Template(TemplateType, { id: TemplateId, initialState: StateTemplate, ui: "UiData" });

    describe("createState", () => {

        it("creates correct object state", () => {

            const state = template.createState(catalogs, StateId);

            // State should have Id
            expect(state.id).toBe(StateId);
            // State should point to template
            expect(state.templateId).toBe(TemplateId);
            expect(state.type).toBe(TemplateType);
            // Data should be deep cloned
            expect(state.text).toBe(StateTemplate.text);
            expect(state.data).toEqual(StateTemplate.data);
            expect(state.data).not.toBe(StateTemplate.data);
        });

        it("uses UUID4 when id is not supplied", () => {
            const state = template.createState(catalogs);
            expect(state.id).toBeDefined();
            expect(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/.test(state.id)).toBeTruthy();
        });
    });

    describe("createChildState", () => {

        it("creates correct child state", () => {

            const state = template.createChildState(catalogs);

            // State should point to template
            expect(state.templateId).toBe(TemplateId);
            expect(state.type).toBe(TemplateType);
            // Data should be deep cloned
            expect(state.text).toBe(StateTemplate.text);
            expect(state.data).toEqual(StateTemplate.data);
            expect(state.data).not.toBe(StateTemplate.data);
        });

    });
});
