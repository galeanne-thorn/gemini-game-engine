import "jest";
import { Condition, ConditionCatalog, EmptyGameState, GameCatalogs } from "../";

const actionIdentityFunction = (gs, a) => gs;

describe("ConditionCatalog", () => {

    const conditions = new ConditionCatalog();
    const catalogs: GameCatalogs = {
        actions: null,
        templates: null,
        conditions
    };

    describe("get", () => {

        it("gets by type", () => {
            const byStringFunction = (gs, a) => gs;
            conditions.add("BY_STRING", byStringFunction);

            const condition = conditions.getCondition("BY_STRING");
            expect(condition).toBe(byStringFunction);
        });

        it("gets by condition ", () => {
            const byObjectFunction = (gs, a) => gs;
            conditions.add("BY_ACTION_OBJECT", byObjectFunction);

            const condition = conditions.getCondition({ type: "BY_ACTION_OBJECT" });
            expect(condition).toBe(byObjectFunction);
        });

        it("returns undefined if action is not registered", () => {
            const condition = conditions.getCondition("SOME_ACTION_THAT_IS_NOT_REGISTERED");
            expect(condition).toBe(undefined);
        });
    });

    describe("evaluate", () => {

        it("evaluates the correct condition", () => {
            conditions.add("ALWAYS", (gs, c) => true);
            const result = conditions.evaluate(EmptyGameState, catalogs, { type: "ALWAYS" });
            expect(result).toBe(true);
        });

        it("returns false if condition if not found and default result is not specified", () => {
            const result = conditions.evaluate(
                EmptyGameState,
                catalogs,
                { type: "SOME_CONDITION_THAT_IS_NOT_REGISTERED" });
            expect(result).toBe(false);
        });

        it("returns false if condition if not passed in and default result is not set", () => {
            const result = conditions.evaluate(EmptyGameState, catalogs, undefined);
            expect(result).toBe(false);
        });

        it("returns false if condition if not passed in and default result is not set", () => {
            const result = conditions.evaluate(EmptyGameState, catalogs, null);
            expect(result).toBe(false);
        });

        it("returns true if condition if not found and default result is set to true", () => {
            const result = conditions.evaluate(
                EmptyGameState,
                catalogs,
                { type: "SOME_CONDITION_THAT_IS_NOT_REGISTERED" },
                true);
            expect(result).toBe(true);
        });

        it("returns true if condition if not passed in and default result is set to true", () => {
            const result = conditions.evaluate(EmptyGameState, catalogs, undefined, true);
            expect(result).toBe(true);
        });

        it("returns true if condition if not passed in and default result is set to true", () => {
            const result = conditions.evaluate(EmptyGameState, catalogs, null, true);
            expect(result).toBe(true);
        });
    });
});
