/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { isArray } from "lodash";
import { SimpleCatalog } from "../Catalog";
import { Action, ActionFunction, ActionGroup, IActionCatalog } from "../Definitions";
import { GameState } from "../GameState";
import { ConditionCatalog } from "./ConditionCatalog";
import { TemplateCatalog } from "./TemplateCatalog";

/**
 * Dictionary of action functions
 */
export class ActionCatalog extends SimpleCatalog<ActionFunction> implements IActionCatalog {

    /**
     * Gets reducer for given action type
     *
     * @param actionOrActionType - Action or action type to get the action function for
     * @return Action function
     */
    public getAction(actionOrActionType: Action | string): ActionFunction {
        if (typeof (actionOrActionType) === "string") {
            return super.getItem(actionOrActionType);
        }
        return super.getItem(actionOrActionType.type);
    }
}
