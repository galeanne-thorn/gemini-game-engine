import "jest";

import { Template } from "./Template";
import { TemplateCatalog } from "./TemplateCatalog";

/**
 * Template implementation
 */
class TestTemplate extends Template {
    constructor(definition) {
        super("T", definition);
    }
}

describe("TemplateCatalog", () => {

    it("adds and Gets the objects correctly", () => {
        const catalog = new TemplateCatalog();

        const template1 = catalog.add(new Template("A", { id: "B", initialState: {} }));
        const template2 = catalog.add(new Template("B", { id: "C", initialState: {} }));

        const retrieved1 = catalog.getTemplate("A", "B");
        const retrieved2 = catalog.getTemplate("B", "C");

        expect(retrieved1).toBe(template1);
        expect(retrieved2).toBe(template2);
    });

    describe("add", () => {

        const catalog = new TemplateCatalog();

        it("throws an error if template is already registered", () => {
            const template = catalog.add(new Template("T", { id: "I", initialState: {} }));
            expect(
                () => { catalog.add(template); }
            ).toThrowError("Template of type T with ID I is already defined in the catalog!");
        });

        it("allows same ID for different types", () => {
            const template1 = catalog.add(new Template("T1", { id: "I", initialState: {} }));
            const template2 = catalog.add(new Template("T2", { id: "I", initialState: {} }));
        });

        it("returns the added template", () => {
            const template = new Template("T", { id: "A", initialState: {} });
            const addedTemplate = catalog.add(template);

            expect(addedTemplate).toBe(template);
        });
    });

    describe("get", () => {

        const catalog = new TemplateCatalog();

        it("accepts state object", () => {
            const template = catalog.add(new Template("A", { id: "C", initialState: {} }));
            const stateObject = { type: "A", templateId: "C", id: "1" };
            const retrieved = catalog.getTemplate(stateObject);
            expect(retrieved).toBe(template);
        });

        it("throws an error if called with type only", () => {
            expect(
                () => { catalog.getTemplate("X", undefined); }
            ).toThrowError("Parameter id must be specified when called as (type, id)");
        });

        it("throws an error if template catalog does not exist", () => {
            expect(
                () => { catalog.getTemplate("X", "A"); }
            ).toThrowError("There is no catalog for type 'X'");
        });

        it("throws an error if template is not found", () => {
            catalog.add(new Template("A", { id: "X", initialState: {} }));
            expect(
                () => { catalog.getTemplate("A", "Z"); }
            ).toThrowError("There is no template 'Z' in 'A' catalog!");
        });

    });

    describe("addType", () => {

        it("throws an error if template type is already registered", () => {
            const catalog = new TemplateCatalog();
            catalog.addType("T", TestTemplate);
            expect(
                () => { catalog.addType("T", TestTemplate); }
            ).toThrowError("Template type T is already defined in the catalog!");
        });

        it("registers the constructor function", () => {
            const catalog = new TemplateCatalog();
            catalog.addType("type1", TestTemplate);
            expect(catalog.templateTypes.type1).toBe(TestTemplate);
        });
    });

    describe("addTypes", () => {

        it("throws an error if template type is already registered", () => {
            const catalog = new TemplateCatalog();
            catalog.addType("T", TestTemplate);
            expect(
                () => { catalog.addTypes({ T: TestTemplate }); }
            ).toThrowError("Template type T is already defined in the catalog!");
        });

        it("registers the constructor functions", () => {

            // tslint:disable:max-classes-per-file
            class T1 extends TestTemplate { }
            class T2 extends TestTemplate { }

            const catalog = new TemplateCatalog();

            catalog.addTypes({ T1, T2 });
            expect(catalog.templateTypes.T1).toBe(T1);
            expect(catalog.templateTypes.T2).toBe(T2);
        });
    });

    describe("addTemplates", () => {

        it("creates templates from definitions", () => {
            const catalog = new TemplateCatalog();
            catalog.addType("T", TestTemplate);

            catalog.addTemplates({
                T: [{ id: "T1" }, { id: "T2" }]
            });

            expect(catalog.templates.T.T1).toBeDefined();
            expect(catalog.templates.T.T1.id).toBe("T1");
            expect(catalog.templates.T.T2).toBeDefined();
            expect(catalog.templates.T.T2.id).toBe("T2");
        });
    });
});
