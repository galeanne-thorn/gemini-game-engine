/**
 * Module Gemini Templates
 *
 * Copyright © 2017 Galeanne Thorn
 * License: MIT
 */

import { cloneDeep } from "lodash";
import { ChildState, GameCatalogs, ITemplate, ObjectState, TemplateDefinition, Typed, Unique } from "../Definitions";
import { uuid } from "../UUID";

/**
 * Template implementation
 */
export class Template<TPayload extends object = any> implements ITemplate<TPayload> {

    /** Type of the template */
    public type: string;

    /** Id of the template */
    public id: string;

    /** Template UI data */
    public ui?: any;

    /** Initial state for generated game objects */
    public initialState?: TPayload;

    /**
     * Creates new template
     *
     * @param type - template type
     * @param definition - template data, including type and id
     */
    public constructor(type: string, definition: TemplateDefinition<TPayload>) {
        Object.assign(this, definition);
        this.type = type;
    }

    /**
     * Creates new GameObject derived from this template with given object id
     * @param id - id of the GameObject  to create. If not supplied, UUID type 4 is used
     * @returns new GameObject based on this template, with given id
     */
    public createState(catalogs: GameCatalogs, id?: string): ObjectState<TPayload> {
        id = id || uuid();
        const result = Object.assign(
            {},
            cloneDeep(this.initialState),
            {
                id,
                type: this.type,
                templateId: this.id
            }
        );
        return result;
    }

    /**
     * Creates new ChildState derived from this template.
     * @returns new ChildState based on this template
     */
    public createChildState(catalogs: GameCatalogs): ChildState<TPayload> {
        const result = Object.assign(
            {},
            cloneDeep(this.initialState),
            {
                type: this.type,
                templateId: this.id
            }
        );
        return result;
    }
}

/**
 * Template constructor signature
 *
 * Notice that type parameter is missing - this is because we expect the
 * classed deriving from template will have this set constant
 * and will not need it to be passed in
 */
export interface TemplateConstructor<TPayload extends object = any> {
    new(definition: TemplateDefinition<TPayload>): Template<TPayload>;
}
