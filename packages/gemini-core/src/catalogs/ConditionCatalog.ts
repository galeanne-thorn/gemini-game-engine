/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { SimpleCatalog } from "../Catalog";
import { Condition, ConditionEvaluator, GameCatalogs, IConditionCatalog } from "../Definitions";
import { GameState } from "../GameState";
import { resolveReferences } from "../ReferenceResolver";

/**
 * Dictionary of condition functions
 */
export class ConditionCatalog extends SimpleCatalog<ConditionEvaluator> implements IConditionCatalog {

    /**
     * Gets reducer for given Condition type
     *
     * @param conditionOrConditionType - Condition or condition type to get the condition evaluator for
     */
    public getCondition(conditionOrConditionType: Condition | string): ConditionEvaluator {
        if (typeof (conditionOrConditionType) === "string") {
            return super.getItem(conditionOrConditionType);
        }
        return super.getItem(conditionOrConditionType.type);
    }

    /**
     * Evaluates condition
     *
     * @param gs            - Game state
     * @param catalogs      - Game catalogs to use in the evaluation
     * @param condition     - Condition to evaluate
     * @param defaultResult - Value to return if function is not registered or condition is undefined
     *
     * @returns Value returned from registered condition function or defaultResult
     *          if condition function is not specified or not registered
     */
    public evaluate(
        gs: GameState,
        catalogs: GameCatalogs,
        condition: Condition,
        defaultResult: boolean = false
    ): boolean {
        if (condition === null || condition === undefined) return defaultResult;
        const conditionFunction = this.getCondition(condition);
        if (conditionFunction) {
            return conditionFunction(gs, resolveReferences(gs, catalogs, condition), catalogs);
        }
        // console.log(`\n No condition ${condition.type}`);
        return defaultResult;
    }
}
