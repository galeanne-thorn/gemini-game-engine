import "jest";
import { Action, ActionCatalog } from "../";

const actionIdentityFunction = (gs, a) => gs;

describe("ActionCatalog", () => {

    describe("getAction", () => {

        const catalog = new ActionCatalog();

        it("gets action by type", () => {
            const byStringFunction = (gs, a) => gs;
            catalog.add("BY_STRING", byStringFunction);

            const action = catalog.getAction("BY_STRING");
            expect(action).toBe(byStringFunction);
        });

        it("gets action by action", () => {
            const byObjectFunction = (gs, a) => gs;
            catalog.add("BY_ACTION_OBJECT", byObjectFunction);

            const action = catalog.getAction({ type: "BY_ACTION_OBJECT" });
            expect(action).toBe(byObjectFunction);
        });

        it("returns undefined if action is not registered", () => {
            const action = catalog.getAction("SOME_ACTION_THAT_IS_NOT_REGISTERED");
            expect(action).toBe(undefined);
        });
    });

});
