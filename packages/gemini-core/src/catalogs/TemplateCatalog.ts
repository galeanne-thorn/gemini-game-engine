/**
 * Module Gemini Templates
 *
 * Copyright © 2017 Galeanne Thorn
 * License: MIT
 */
import { forOwn, isNull, isUndefined, values } from "lodash";
import { Catalog } from "../Catalog";
import { ITemplate, ITemplateCatalog, ObjectState, TemplateDefinition, TemplatePointer } from "../Definitions";
import { Template, TemplateConstructor } from "./Template";

/**
 * Template catalog is catalog of catalogs of templates
 */
export class TemplateCatalog implements ITemplateCatalog {

    /**
     * Template class catalog
     */
    public readonly templateTypes: Catalog<TemplateConstructor> = {};

    /**
     * Template catalog
     */
    public readonly templates: Catalog<Catalog<ITemplate>> = {};

    /**
     * Adds template type to the catalog
     * @param type - Type of template
     * @param construct - Default constructor for that template type
     */
    public addType(type: string, construct: TemplateConstructor): void {
        if (!isUndefined(this.templateTypes[type])) {
            throw new Error(`Template type ${type} is already defined in the catalog!`);
        }
        this.templates[type] = {};
        this.templateTypes[type] = construct;
    }

    /**
     * Adds multiple template types to the catalog
     * @param type - Type of template
     * @param construct - Default constructor for that template type
     */
    public addTypes(templateTypes: Catalog<TemplateConstructor> = {}): void {
        forOwn(templateTypes, (construct, type) => {
            this.addType(type, construct);
        });
    }

    /**
     * Adds template to the catalog
     *
     * @param template - Template to add to the catalog
     *
     * @returns - Template
     * @throws - Error if template with the same type and id already exists
     */
    public add<TTemplate extends ITemplate = Template>(template: TTemplate): TTemplate {

        const type = template.type;
        if (isUndefined(this.templates[type])) {
            this.templates[type] = {};
        }
        const subCatalog = this.templates[type];

        const id = template.id;
        if (!isUndefined(subCatalog[id])) {
            throw new Error(`Template of type ${type} with ID ${id} is already defined in the catalog!`);
        }
        subCatalog[id] = template;
        return template;
    }

    /**
     * Adds multiple template definitions to the catalog.
     * Note constructors for template types should be registered first
     * via addType or addTypes methods.
     *
     * @param templateDefinitions - Template definitions
     */
    public addTemplates(templateDefinitions: Catalog<TemplateDefinition[]> = {}) {
        forOwn(templateDefinitions, (list, type) => {
            const templateConstructor = this.templateTypes[type];
            if (isUndefined(templateConstructor)) {
                throw new Error(`Constructor for type ${type} is not defined`);
            }
            list.forEach(d => this.add(new templateConstructor(d)));
        });
    }

    /**
     * Gets template from the catalog
     *
     * @param type - Type of the template or template pointer
     * @param id   - Id of the template. Required for template type, ignored for template pointer
     * @param gameObject - Game object its template you want
     *
     * @returns Template
     * @throws Error if template type of id is not found
     */
    public getTemplate<TTemplate extends ITemplate = Template>(type: string, id: string): TTemplate;
    public getTemplate<TTemplate extends ITemplate = Template>(templatePointer: TemplatePointer): TTemplate;
    public getTemplate<TTemplate extends ITemplate = Template>(
        typeOrPointer: string | TemplatePointer, id?: string): TTemplate {

        let type: string;
        if (typeof typeOrPointer === "string") {
            type = typeOrPointer;
            if (isUndefined(id) || isNull(id)) {
                throw new Error("Parameter id must be specified when called as (type, id)");
            }
        }
        else {
            type = typeOrPointer.type;
            id = typeOrPointer.templateId;
        }

        if (isUndefined(this.templates[type])) {
            throw new Error(`There is no catalog for type '${type}'`);
        }
        if (isUndefined(this.templates[type][id])) {
            throw new Error(`There is no template '${id}' in '${type}' catalog!`);
        }
        return this.templates[type][id] as TTemplate;
    }

    /**
     * Gets all registered templates of given type
     *
     * @param type - Type of templates to get
     *
     * @returns Array of templates of given type
     */
    public getAllTemplates<TTemplate extends ITemplate = Template>(type: string): TTemplate[] {
        if (isUndefined(this.templates[type])) return [];
        return values<TTemplate>(this.templates[type] as Catalog<TTemplate>);
    }
}
