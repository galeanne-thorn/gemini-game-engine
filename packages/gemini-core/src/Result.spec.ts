/**
 * Module Gemini Game Engine Core - Tests
 *
 * Copyright © 2017 Galeanne Thorn
 * License: MIT
 */

import "jest";
import { Result, ResultSuccess, ResultFail, mergeResults } from "./Result";

describe("ResultSuccess", () => {

    it("should have success property set to true", () => {
        expect(ResultSuccess.success).toBeTruthy();
    });

    it("should have empty reason property", () => {
        expect(ResultSuccess.reason).toBeUndefined();
    });
});

describe("ResultFail", () => {

    it("should have success property set to false", () => {
        let result = ResultFail("");
        expect(result.success).toBeFalsy();
    });

    it("should have reason property set to given value", () => {
        let reason = "Some reason";
        let result = ResultFail(reason);
        expect(result.reason).toEqual(reason);
    });
});


describe("mergeResults", () => {

    it("should set success to logical and of the individual results", () => {

        function expectMergedResultSuccess(r1: Result, r2: Result, expected: boolean) {
            let result = mergeResults(r1, r2);
            expect(result.success).toEqual(expected);
        }

        expectMergedResultSuccess(ResultSuccess, ResultSuccess, true);
        expectMergedResultSuccess(ResultSuccess, ResultFail(""), false);
        expectMergedResultSuccess(ResultFail(""), ResultSuccess, false);
        expectMergedResultSuccess(ResultFail(""), ResultFail(""), false);
    });

    it("should concatenate the reasons correctly", () => {

        function expectMergedResultReason(r1: Result, r2: Result, expected: string[]) {
            let merged = mergeResults(r1, r2);
            expect(merged.reason).toEqual(expected);
        }

        expectMergedResultReason(ResultSuccess, ResultSuccess, []);
        expectMergedResultReason(ResultSuccess, ResultFail("reason"), ["reason"]);
        expectMergedResultReason(ResultFail("reason"), ResultSuccess, ["reason"]);
        expectMergedResultReason(ResultFail("reason"), ResultFail("other"), ["reason", "other"]);
    });

});