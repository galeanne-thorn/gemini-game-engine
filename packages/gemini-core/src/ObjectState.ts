/**
 * Module Gemini Templates
 *
 * Copyright © 2017 Galeanne Thorn
 * License: MIT
 */

import { isNull, isUndefined, omit } from "lodash";
import { Catalog } from "./Catalog";
import { ObjectState, StatePointer, TemplatePointer, Typed, Unique } from "./Definitions";
import { GameState } from "./GameState";

/**
 * Sets object state in GameState
 * @param gs - Game state
 * @param gameObject - Game object to set
 *
 * @returns New GameState
 */
export function setObjectState(gs: GameState, gameObject: ObjectState): GameState {
    return {
        ...gs,
        objects: {
            ...gs.objects,
            [gameObject.type]: {
                ...gs.objects[gameObject.type],
                [gameObject.id]: gameObject
            }
        }
    };
}

/**
 * Tries to get object state for given type and id from GameState. Returns null if object was not found
 *
 * @param gs - Game state
 * @param type - Type of the game object
 * @param id - Id of the game object
 *
 * @returns GameObject or null if not found
 */
export function tryGetObjectState<TGameObject extends ObjectState = ObjectState>(
    gs: GameState,
    type: string,
    id: string): TGameObject {

    const catalog = gs.objects[type] as Catalog<TGameObject>;
    if (catalog) {
        const state = catalog[id];
        if (state) return state;
    }
    return null;
}

/**
 * Gets object state for given type and id from GameState
 *
 * @param gs - Game state
 * @param type - Type of the game object
 * @param id - Id of the game object
 *
 * @returns GameObject
 * @throws Error if GameObject was not found
 */
export function getObjectState<TGameObject extends ObjectState = ObjectState>(
    gs: GameState, type: string, id: string): TGameObject;
/**
 * Gets object state for given type and id from GameState
 *
 * @param gs - Game state
 * @param statePointer - State pointer
 *
 * @returns GameObject
 * @throws Error if GameObject was not found
 */
export function getObjectState<TGameObject extends ObjectState = ObjectState>(
    gs: GameState, statePointer: StatePointer): TGameObject;
export function getObjectState<TGameObject extends ObjectState = ObjectState>(
    gs: GameState, typeOrPointer: string | StatePointer, id?: string): TGameObject {

    let type: string;
    if (typeof typeOrPointer === "string") {
        type = typeOrPointer;
        if (isUndefined(id) || isNull(id)) {
            throw new Error("Parameter id must be specified when called as (type, id)");
        }
    }
    else {
        type = typeOrPointer.type;
        id = typeOrPointer.id;
    }
    const result = tryGetObjectState<TGameObject>(gs, type, id);
    if (result === null) {
        throw new Error(`Game object ${id} of type ${type} not found.`);
    }
    return result;
}

/**
 * Creates new object state getter for given type
 */
export function ObjectStateGetter<TGameObject extends ObjectState = ObjectState>(type: string) {
    return (gs: GameState, id: string): TGameObject => getObjectState<TGameObject>(gs, type, id);
}

/**
 * Removes object state from game state
 *
 * @param gs - Game state
 * @param pointer - Pointer to object state
 *
 * @returns New game state
 */
export function deleteObjectState(gs: GameState, pointer: StatePointer): GameState;
/**
 * Removes object state from game state
 *
 * @param gs - Game state
 * @param type - Type of the object
 * @param id - Id of the object
 *
 * @returns New game state
 */
export function deleteObjectState(gs: GameState, type: string, id: string): GameState;
export function deleteObjectState(gs: GameState, typeOrPointer: string | StatePointer, id?: string): GameState {
    let type: string;
    if (typeof typeOrPointer === "string") {
        type = typeOrPointer;
        if (isUndefined(id) || isNull(id)) {
            throw new Error("Parameter id must be specified when called as (type, id)");
        }
    }
    else {
        type = typeOrPointer.type;
        id = typeOrPointer.id;
    }

    const catalog = gs.objects[type];
    if (catalog && !isUndefined(catalog[id])) {
        return {
            ...gs,
            objects: {
                ...gs.objects,
                [type]: omit(catalog, id)
            }
        };
    }
    else {
        return gs;
    }
}
