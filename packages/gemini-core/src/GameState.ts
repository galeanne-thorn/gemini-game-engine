/**
 * Gemini Game Engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { flatten, get, last } from "lodash";
import { Catalog } from "./Catalog";
import { ObjectState, StatePointer } from "./Definitions";
import { Result, ResultSuccess } from "./Result";

/**
 * Represents the current state of the game
 */
export interface GameState {

    /** Last action result */
    lastActionResult: Result;

    /** Active stage */
    activeStages: StatePointer[];

    /** Custom variables */
    variables: Catalog<any>;

    /** Object states */
    objects: Catalog<Catalog<ObjectState>>;

    /** Any other custom data */
    [key: string]: any;
}

/**
 * Empty game state
 */
export const EmptyGameState: GameState = {
    lastActionResult: ResultSuccess,
    activeStages: [],
    objects: {},
    variables: {}
};

/**
 * Gets value from custom variable in GameState
 */
export function getVariable<T>(gs: GameState, id: string | string[]): T {
    return get(gs.variables, flatten([id])) as T;
}

/**
 * Gets active stage pointer
 * @param gs - GameState
 * @returns Pointer to active stage object state or undefined
 */
export function getActiveStage(gs: GameState): StatePointer {
    const stage = last(gs.activeStages);
    if (!stage) return undefined;
    return stage;
}
