//
// UUID provider.
// Based on https://gist.github.com/LeverOne/1308368
//

// tslint:disable:max-line-length
// tslint:disable:no-bitwise

/**
 * Returns a random v4 UUID of the form xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx,
 * where each x is replaced with a random hexadecimal digit from 0 to f,
 * and y is replaced with a random hexadecimal digit from 8 to b.
 * @param a
 */
export function uuid() {
    return g();
}

// @ts-ignore
function g(a?: any, b?: any) { for (b = a = ""; a++ < 36; b += a * 51 & 52 ? (a ^ 15 ? 8 ^ Math.random() * (a ^ 20 ? 16 : 4) : 4).toString(16) : "-"); return b; }
