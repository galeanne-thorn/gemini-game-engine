import "jest";

import {
    deleteObjectState, EmptyGameState, GameState, getObjectState,
    ObjectState, ObjectStateGetter, setObjectState, tryGetObjectState
} from "./";

const gameObject: ObjectState = {
    id: "test",
    templateId: "template",
    type: "testType"
};

const testGs: GameState = {
    ...EmptyGameState,
    objects: {
        type: {
            test: {
                id: "test",
                templateId: "",
                type: "type"
            }
        },
        testType: {
            test: gameObject
        }
    }
};

describe("setObjectState", () => {

    it("Sets object to correct place in game state", () => {
        const gs = setObjectState(EmptyGameState, gameObject);

        expect(gs).not.toBe(EmptyGameState);
        expect(gs.objects).toBeDefined();
        expect(gs.objects.testType).toBeDefined();
        expect(gs.objects.testType.test).toBe(gameObject);
    });

    it("correctly updates whole path to object state", () => {

        const newObject = {
            id: "test",
            templateId: "template",
            type: "testType",
            data: "x"
        } as ObjectState<any>;
        const gs = setObjectState(testGs, newObject);

        expect(gs).not.toBe(testGs);
        expect(gs.objects).not.toBe(testGs.objects);
        expect(gs.objects.testType).not.toBe(testGs.objects.testType);
        expect(gs.objects.testType.test).toBe(newObject);
    });

});

describe("tryGetObjectState", () => {

    it("Returns null if type is not found in game state", () => {
        expect(tryGetObjectState(EmptyGameState, "type", "id")).toBeNull();
    });

    it("Returns null if id is not found in game state", () => {
        const gs = Object.assign({}, EmptyGameState, { type: {} });
        expect(tryGetObjectState(gs, "type", "id")).toBeNull();
    });

    it("Finds correct game object", () => {
        const found = tryGetObjectState(testGs, "testType", "test");
        expect(found).toEqual(gameObject);
    });
});

describe("getObjectState", () => {

    it("Throws if type is not found in game state", () => {
        expect(() => getObjectState(EmptyGameState, "type", "id"))
            .toThrowError("Game object id of type type not found.");
    });

    it("Throws if id is not found in game state", () => {
        const gs = Object.assign({}, EmptyGameState, { type: {} });
        expect(() => getObjectState(gs, "type", "id"))
            .toThrowError("Game object id of type type not found.");
    });

    it("Throws an error if called with type only", () => {
        const gs = Object.assign({}, EmptyGameState, { type: {} });
        expect(
            () => {
                getObjectState(gs, "type", undefined);
            }
        ).toThrowError("Parameter id must be specified when called as (type, id)");
    });

    it("Finds correct game object for type and id", () => {
        const found = getObjectState(testGs, "testType", "test");
        expect(found).toEqual(gameObject);
    });

    it("Finds correct game object for state pointer", () => {
        const found = getObjectState(testGs, { type: "testType", id: "test" });
        expect(found).toEqual(gameObject);
    });
});

describe("ObjectStateGetter", () => {

    it("Creates function that gets correct game object", () => {
        const getter = ObjectStateGetter("testType");
        expect(getter).toBeDefined();
        expect(typeof (getter)).toBe("function");

        const found = getter(testGs, "test");
        expect(found).toEqual(gameObject);
    });
});

describe("deleteObjectState", () => {

    it("returns same game state if there is no such object type", () => {
        const newGs = deleteObjectState(EmptyGameState, "type", "id");
        expect(newGs).toBe(EmptyGameState);
    });

    it("returns same game state if there is no such object id", () => {
        const gs = {
            ...EmptyGameState,
            objects: {
                type: {}
            }
        };
        const newGs = deleteObjectState(gs, "type", "id");
        expect(newGs).toBe(gs);
    });

    it("removes correct state by type and id", () => {
        const gs: GameState = {
            ...EmptyGameState,
            objects: {
                type: {
                    id1: { id: "id1", type: "type", templateId: "x" },
                    id2: { id: "id2", type: "type", templateId: "x" }
                }
            }
        };
        const newGs = deleteObjectState(gs, "type", "id1");
        expect(newGs).not.toBe(gs);
        expect(newGs.objects).not.toBe(gs.objects);
        expect(newGs.objects.type).not.toBe(gs.objects.type);
        expect(newGs.objects.type.id2).toBeDefined();
        expect(newGs.objects.type.hasOwnProperty("id1")).toBe(false);
    });

    it("removes correct state by state pointer", () => {
        const gs: GameState = {
            ...EmptyGameState,
            objects: {
                type: {
                    id1: { id: "id1", type: "type", templateId: "x" },
                    id2: { id: "id2", type: "type", templateId: "x" }
                }
            }
        };
        const newGs = deleteObjectState(gs, { type: "type", id: "id1" });
        expect(newGs).not.toBe(gs);
        expect(newGs.objects).not.toBe(gs.objects);
        expect(newGs.objects.type).not.toBe(gs.objects.type);
        expect(newGs.objects.type.id2).toBeDefined();
        expect(newGs.objects.type.hasOwnProperty("id1")).toBe(false);
    });
});
