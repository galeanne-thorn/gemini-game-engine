import "jest";
import { Catalog, SimpleCatalog } from "./Catalog";

describe("TypedCatalog", () => {

    describe("add", () => {

        it("requires the key to be defined", () => {
            const catalog = new SimpleCatalog();
            expect(() => catalog.add(undefined, {}))
                .toThrowError("Key must be specified");
            expect(() => catalog.add(null, {}))
                .toThrowError("Key must be specified");
        });

        it("requires the value to be defined", () => {
            const catalog = new SimpleCatalog();
            expect(() => catalog.add("TEST", undefined))
                .toThrowError("Value for TEST must be specified");
            expect(() => catalog.add("TEST", null))
                .toThrowError("Value for TEST must be specified");
        });

        it("does not allow registering same key twice", () => {
            const catalog = new SimpleCatalog();
            catalog.add("DUPLICATE", {});
            expect(() => catalog.add("DUPLICATE", {}))
                .toThrowError("Key DUPLICATE is already registered");
        });

        it("adds value under key", () => {
            const catalog = new SimpleCatalog();
            const value = {};
            catalog.add("KEY", value);

            expect(catalog.I.KEY).toBe(value);
        });
    });

    describe("addRange", () => {

        it("registers all objects in catalog", () => {
            const catalog = new SimpleCatalog();
            const otherCatalog = {
                custom1: {},
                custom2: {},
            }
            catalog.addRange(otherCatalog);

            expect(catalog.I.custom1).toBeDefined();
            expect(catalog.I.custom2).toBeDefined();
        });

        it("does not allow registering same key twice", () => {
            const catalog = new SimpleCatalog();
            catalog.add("DUPLICATE", {});
            const otherCatalog = {
                DUPLICATE: {}
            }
            expect(() => catalog.addRange(otherCatalog))
                .toThrowError("Key DUPLICATE is already registered");
        });

    });

    describe("getItem", () => {

        const value = {};

        it("gets value by key", () => {
            const catalog = new SimpleCatalog();
            catalog.add("BY_STRING", value);

            expect(catalog.getItem("BY_STRING")).toBe(value);
        });

        it("returns undefined if key is not registered", () => {
            const catalog = new SimpleCatalog();
            expect(catalog.getItem("SOME_ACTION_THAT_IS_NOT_REGISTERED")).toBe(undefined);
        });
    });

    describe("listActions", () => {

        it("returns array", () => {
            const catalog = new SimpleCatalog();
            let keys = catalog.listKeys();
            expect(keys).toBeDefined();
        });

        it("returns keys in alphabetical order", () => {
            const catalog = new SimpleCatalog();
            catalog.add("Z", {});
            catalog.add("A", {});
            let keys = catalog.listKeys();
            expect(keys).toEqual(['A', 'Z']);
        });
    });
});