/**
 * @galeanne-thorn/gemini-core
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Catalog } from "./Catalog";
import { Typed, Unique } from "./Definitions";

/**
 * Type of action or condition parameter
 */
export type FieldType =
    "string"      // Text
    | "number"    // Number
    | "boolean"   // Boolean
    | "template"  // Template pointer {type, templateId}
    | "type"      // Template/Object type (string)
    | "action"    // Action instance {type, ...}
    | "condition" // Condition instance {type, ...}
    | "state"     // State pointer {type, id}
    | "choice";   // UI Choice

/**
 * Named object - has name and description
 */
export interface Named {
    /**
     * Name of the data
     */
    name: string;
    /**
     * Description
     */
    description: string;
}

export interface FieldMetadata extends Named {
    /**
     * Type of the parameter
     */
    type: FieldType;
    /**
     * Is parameter optional?
     */
    optional?: boolean;
    /**
     * Is parameter array?
     */
    isArray?: boolean;
    /**
     * Template Type constraint
     */
    typeConstraint: string;
}

/**
 * Action/condition metadata
 */
export interface FunctionMetadata extends Named {
    /**
     * parameters metadata
     */
    parameters: Catalog<FieldMetadata>;
}

/**
 * Template type metadata
 */
export interface TemplateMetadata extends Named {
    /**
     * Template fields
     */
    fields: Catalog<FieldMetadata>;
}

/**
 * GamePlugin metadata
 */
export interface GamePluginMetadata {
    /**
     * Id of the plugin
     */
    id: string;
    /**
     * Title of the plugin
     */
    title: string;
    /**
     * Author of the plugin
     */
    author: string;
    /**
     * Template type metadata
     */
    templates: Catalog<TemplateMetadata>;
    /**
     * Actions metadata
     */
    actions: Catalog<FunctionMetadata>;
    /**
     * Conditions metadata
     */
    conditions: Catalog<FunctionMetadata>;
}
