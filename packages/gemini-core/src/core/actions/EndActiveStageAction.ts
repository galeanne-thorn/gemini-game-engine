/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Action } from "../../Definitions";
import { GameState } from "../../GameState";

/**
 * Type of the EndActiveStage action
 */
export const ACTION_END_STAGE = "END_STAGE";

/**
 * Creates the EndActiveStage action
 */
export function EndActiveStage(): Action {
    return {
        type: ACTION_END_STAGE
    };
}

/**
 * Ends current stage and returns to stage one level up
 *
 * @param gs - GameState
 * @returns GameState with the current stage set to next in stack
 */
export function endActiveStage(gs: GameState): GameState {
    const stages = gs.activeStages || [];
    return {
        ...gs,
        activeStages: [...stages.slice(0, -1)]
    };
}
