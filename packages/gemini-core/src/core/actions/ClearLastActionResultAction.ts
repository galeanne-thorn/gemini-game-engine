/**
 * Gemini Game Engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Action } from "../../Definitions";
import { GameState } from "../../GameState";
import { Result, ResultSuccess } from "../../Result";

/**
 * Type of the ClearLastActionResult action
 */
export const ACTION_CLEAR_ACTION_RESULT = "CLEAR_ACTION_RESULT";

/**
 * Creates the ClearLastActionResult action
 */
export function ClearLastActionResult(): Action {
    return {
        type: ACTION_CLEAR_ACTION_RESULT
    };
}

/**
 * Clears the last action result by setting lastActionResult to ResultSuccess
 *
 * @returns New game state
 */
export function clearLastActionResult(gs: GameState): GameState {
    return Object.assign(
        {},
        gs,
        {
            lastActionResult: ResultSuccess
        }
    );
}
