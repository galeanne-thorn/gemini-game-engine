/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { isNull, isUndefined, last } from "lodash";
import { Action, StatePointer } from "../../Definitions";
import { GameState } from "../../GameState";
import { ChoiceGroupItem } from "../../ui/Choice";

/**
 * Type of the SetActiveStage action
 */
export const ACTION_SET_STAGE = "SET_STAGE";

/**
 * Set Active Stage Action data
 */
export interface SetActiveStageAction extends Action {
    /**
     * Pointer to the stage to set
     */
    stage: StatePointer;
}

/**
 * Creates the Set Active stage action
 */
export function SetActiveStage(stageType: string, stageId: string): SetActiveStageAction {
    return {
        type: ACTION_SET_STAGE,
        stage: {
            type: stageType,
            id: stageId
        }
    };
}

/**
 * Sets active stage.
 *
 * Note stages are organized in stack by type.
 *
 * If the current active stage is of the same type as the new one stage, it is replaced with the new one.
 *
 * If the current active stage is of different type, the new stage is placed above it.
 *
 * Setting the stage to null or undefined removes the top-most active stage and moved to previous stage.
 * This is equivalent of calling PopActiveStage
 *
 * @param gs - GameState
 * @param action - Action parameters
 *
 * @returns GameState with the given stage set as active one
 */
export function setActiveStage(gs: GameState, action: SetActiveStageAction): GameState;
/**
 * Sets active stage.
 *
 * Note stages are organized in stack by type.
 *
 * If the current active stage is of the same type as the new one stage, it is replaced with the new one.
 *
 * If the current active stage is of different type, the new stage is placed above it.
 *
 * Setting the stage to null or undefined removes the top-most active stage and moved to previous stage.
 * This is equivalent of calling PopActiveStage
 *
 * @param gs - GameState
 * @param stageType - Type of the stage
 * @param stageId - Id of the stage
 *
 * @returns GameState with the given stage set as active one
 */
export function setActiveStage(gs: GameState, stageType: string, stageId: string): GameState;
export function setActiveStage(
    gs: GameState, actionOrStageType: SetActiveStageAction | string, stageId?: string): GameState {

    let stage: StatePointer;
    if (typeof actionOrStageType === "string") {
        if (isUndefined(stageId) || isNull(stageId)) {
            throw new Error("Parameter stageId must be specified when called as (gs, stageType, stageId)");
        }
        stage = {
            type: actionOrStageType,
            id: stageId
        };
    }
    else {
        stage = {
            type: actionOrStageType.stage.type,
            id: actionOrStageType.stage.id
        };
    }

    /**
     * Sets stage state as child stage to the game state. If the stage is null, removes the current stage
     */
    function replaceStage(currentStages: StatePointer[]): StatePointer[] {
        if (stage) {
            if (currentStages.length === 0) {
                return [stage];
            }
            else {
                return [...currentStages.slice(0, -1), stage];
            }
        }
        else {
            return [...currentStages.slice(0, - 1)];
        }
    }

    /**
     * Sets stage state as child stage to the game state. If the stage is null, removes the current stage
     */
    function setChildStage(currentStages: StatePointer[]): StatePointer[] {
        if (stage) {
            return [...currentStages, stage];
        }
        else {
            return [...currentStages.slice(0, -1)];
        }
    }

    const stages = gs.activeStages || [];
    const current = last<StatePointer>(stages);
    const newGs = Object.assign({}, gs);

    if (current && stage && (current.type === stage.type)) {
        newGs.activeStages = replaceStage(stages);
    }
    else {
        newGs.activeStages = setChildStage(stages);
    }

    return newGs;
}
