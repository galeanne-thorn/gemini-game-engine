/**
 * Module Gemini Game Engine Core
 *
 * Copyright © 2017 Galeanne Thorn
 * License: MIT
 */

import { flatten, get, set } from "lodash";
import { Action } from "../../Definitions";
import { GameState } from "../../GameState";

/**
 * Sets custom variable in the GameState
 *
 * @param gs - GameState to modify
 * @param key - Data key
 * @param data - Data value
 */
export function setVar<T>(gs: GameState, key: string, data: T): GameState {
    return {
        ...gs,
        variables: set(
            { ...gs.variables },
            key, data
        )
    };
}

/**
 * Type of the SetVariableAction action
 */
export const ACTION_SET_VARIABLE = "SET_VARIABLE";

/**
 *
 */
export interface SetVariableAction<T = any> extends Action {
    /**
     * Key to set the data to custom section of GameState
     */
    key: string;
    /**
     * Data to be set
     */
    data: T;
}

/**
 * Creates action for setting custom variable
 *
 * @param key - Key to set the variable to in GameState
 * @param data - New value of the variable
 *
 * @return New SetVariableAction
 */
export function SetVariable<T = any>(key: string, data: T): SetVariableAction<T> {
    return {
        type: ACTION_SET_VARIABLE,
        key,
        data
    };
}

/**
 * Sets custom variable in the GameState
 */
export function setVariable(gs: GameState, action: SetVariableAction): GameState {
    return setVar(gs, action.key, action.data);
}
