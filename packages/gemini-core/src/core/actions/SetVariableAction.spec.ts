/**
 * Gemini Game Engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import "jest";
import {
    ACTION_SET_VARIABLE, CorePlugin, EmptyGameState, GameState,
    setVar, SetVariable, setVariable
} from "../../";

describe("SetVariableAction", () => {

    it("creates correct action type", () => {
        const action = SetVariable("simpleKey", "data");
        expect(action.type).toBe(ACTION_SET_VARIABLE);
        expect(action.key).toBe("simpleKey");
        expect(action.data).toBe("data");
    });

    it("creates correct action type for complex key", () => {
        const key = "complex.key";
        const action = SetVariable(key, "data");
        expect(action.type).toBe(ACTION_SET_VARIABLE);
        expect(action.key).toBe(key);
        expect(action.data).toBe("data");
    });
});

describe("setVar", () => {

    it("sets data for simple key", () => {
        const gs = setVar(EmptyGameState, "key", "data");
        expect(gs.variables.key).toBe("data");
    });

    it("sets data for complex key", () => {
        const gs = setVar(EmptyGameState, "complex.key", "data");
        expect(gs.variables.complex.key).toBe("data");
    });
});

describe("module", () => {
    it("registers the action in module", () => {
        expect(CorePlugin.actions[ACTION_SET_VARIABLE]).toBe(setVariable);
    });
});
