/**
 * @galeanne-thorn/gemini-core
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import "jest";
import {
    Action, ACTION_IF, ConditionCatalog, CorePlugin, doIf, EmptyGameState,
    If, IGame
} from "../../";

describe("If", () => {
    const condition = { type: "C" };
    const ifAction = { type: "A1" };
    const elseAction = { type: "A2" };

    it("creates correct action with 'then' and 'else' actions", () => {
        const action = If(condition, ifAction, elseAction);

        expect(action.condition).toBe(condition);
        expect(action.then).toBe(ifAction);
        expect(action.else).toBe(elseAction);
    });

    it("create correct action with just 'then' action", () => {
        const action = If(condition, ifAction);

        expect(action.condition).toBe(condition);
        expect(action.then).toBe(ifAction);
        expect(action.else).toBe(null);
    });
});

describe("doIf", () => {

    const conditions = new ConditionCatalog();
    conditions.add("YES", gs => true);
    conditions.add("NO", gs => false);

    const game: IGame = {
        templates: null,
        actions: null,
        conditions,
        perform: (gs, a) => {
            return {
                ...gs,
                called: (a as Action).type
            };
        }
    };

    it("executes 'then' action if condition is true", () => {
        const action = If({ type: "YES" }, { type: "Y" }, { type: "N" });
        const result = doIf(EmptyGameState, action, game);
        expect(result.called).toBe("Y");
    });

    it("does not execute 'then' action if condition is false and no 'else' action is defined", () => {
        const action = If({ type: "YES" }, { type: "Y" });
        const result = doIf(EmptyGameState, action, game);
        expect(result.called).toBe("Y");
    });

    it("executes 'else' action if condition is false", () => {
        const action = If({ type: "NO" }, { type: "Y" }, { type: "N" });
        const result = doIf(EmptyGameState, action, game);
        expect(result.called).toBe("N");
    });
});

describe("module", () => {
    it("registers the action in module", () => {
        expect(CorePlugin.actions[ACTION_IF]).toBe(doIf);
    });
});
