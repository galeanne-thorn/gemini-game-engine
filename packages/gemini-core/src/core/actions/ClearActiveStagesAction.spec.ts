/**
 * Gemini Game Engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import "jest";
import {
    ACTION_CLEAR_STAGES, ClearActiveStages,
    clearActiveStages, CorePlugin, EmptyGameState, GameState
} from "../../";

const initialGs: GameState = {
    ...EmptyGameState,
    activeStages: [
        { id: "a", type: "s1" },
        { id: "b", type: "s2" },
        { id: "c", type: "s3" }
    ]
};

describe("ClearActiveStages", () => {
    it("creates correct action type", () => {
        const action = ClearActiveStages();
        expect(action.type).toBe(ACTION_CLEAR_STAGES);
    });
});

describe("clearActiveStages", () => {
    it("clears all active stages", () => {
        const gs = clearActiveStages(initialGs);
        expect(gs.activeStages).toEqual([]);
    });

    it("Should return new state", () => {
        const gs = {
            ...EmptyGameState,
            lastActionResult: null
        };

        const result = clearActiveStages(gs);
        expect(result).not.toBe(gs);
    });

    it("Should not fail if stages are empty", () => {
        const result = clearActiveStages(EmptyGameState);
        expect(result.activeStages).toEqual([]);
    });
});

describe("ClearActiveStageAction module", () => {
    it("registers the action in module", () => {
        expect(CorePlugin.actions[ACTION_CLEAR_STAGES]).toBe(clearActiveStages);
    });
});
