/**
 * Gemini Game Engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import "jest";
import {
    ACTION_SET_STAGE, CorePlugin,
    EmptyGameState, GameState, SetActiveStage, setActiveStage, SetActiveStageAction
} from "../../";

const action1: SetActiveStageAction = {
    type: ACTION_SET_STAGE,
    stage: {
        type: "stageType",
        id: "stageId"
    }
};
const expected1 = { type: "stageType", id: "stageId" };

describe("SetActiveStage", () => {

    it("creates correct action type", () => {
        const action = SetActiveStage("stageType", "stageId");
        expect(action.type).toBe(ACTION_SET_STAGE);
        expect(action.stage.type).toBe("stageType");
        expect(action.stage.id).toBe("stageId");
    });
});

describe("setActiveStage", () => {

    it("should add stage to game state with undefined active stages", () => {
        const result = setActiveStage(EmptyGameState, action1);
        expect(result.activeStages).toEqual([expected1]);
    });

    it("should add stage to empty game state", () => {
        const gs = {
            ...EmptyGameState,
            activeStages: []
        };

        const result = setActiveStage(gs, action1);
        expect(result.activeStages).toEqual([expected1]);
    });

    it("should add stage to top of stack if type is different", () => {
        const existing = { id: "1", type: "A" };
        const gs = Object.assign(
            {},
            EmptyGameState,
            {
                ...EmptyGameState,
                activeStages: [
                    existing
                ]
            }
        );

        const result = setActiveStage(gs, action1);
        expect(result.activeStages).toEqual([existing, expected1]);
    });

    it("should replace stage at top of stack if type is same", () => {
        const existing = { id: "1", type: "stageType" };
        const gs = {
            ...EmptyGameState,
            activeStages: [
                existing
            ]
        };

        const result = setActiveStage(gs, action1);
        expect(result.activeStages).toEqual([expected1]);
    });

});

describe("module", () => {
    it("registers the action in module", () => {
        expect(CorePlugin.actions[ACTION_SET_STAGE]).toBe(setActiveStage);
    });
});
