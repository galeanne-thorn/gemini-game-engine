/**
 * Gemini Game Engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import "jest";
import {
    ACTION_END_STAGE, CorePlugin,
    EmptyGameState, EndActiveStage, endActiveStage, GameState
} from "../../";

describe("EndActiveStage", () => {
    it("creates correct action type", () => {
        const action = EndActiveStage();
        expect(action.type).toBe(ACTION_END_STAGE);
    });
});

describe("endActiveStage", () => {
    it("Should return new state", () => {
        const result = endActiveStage(EmptyGameState);
        expect(result).not.toBe(EmptyGameState);
    });

    it("Should remove the top stage", () => {
        const expected = { id: "1", type: "A", templateId: "1" };
        const gs = {
            ...EmptyGameState,
            lastActionResult: null,
            activeStages: [
                expected,
                { id: "1", type: "B", templateId: "1" }
            ]
        };

        const result = endActiveStage(gs);
        expect(result.activeStages.length).toBe(1);
        expect(result.activeStages[0]).toBe(expected);
    });

    it("Should not fail if stages are empty", () => {
        const gs = {
            ...EmptyGameState,
            lastActionResult: null
        };

        const result = endActiveStage(gs);
        expect(result.activeStages).toEqual([]);
    });

});

describe("module", () => {
    it("registers the action in module", () => {
        expect(CorePlugin.actions[ACTION_END_STAGE]).toBe(endActiveStage);
    });
});
