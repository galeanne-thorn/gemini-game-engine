/**
 * @galeanne-thorn/gemini-core
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Action, ActionGroup, Condition, IGame } from "../../Definitions";
import { GameState } from "../../GameState";

/**
 * Type of the If action
 */
export const ACTION_IF = "IF";

/**
 * IfAction payload
 */
export interface IfAction extends Action {
    /**
     * Condition to test
     */
    condition: Condition;
    /**
     * Action to perform if condition is true
     */
    then: ActionGroup;
    /**
     * Optional Action to perform if condition is false
     */
    else?: ActionGroup;
}

/**
 * Creates new IfAction
 *
 * @param condition - Condition to test
 * @param thenAction - Action to perform if condition is true
 * @param elseAction - Optional action to perform if condition is false
 *
 * @returns New IfAction
 */
export function If(condition: Condition, thenAction: ActionGroup, elseAction: ActionGroup = null): IfAction {
    return {
        type: ACTION_IF,
        condition,
        then: thenAction,
        else: elseAction
    };
}

/**
 * Performs the IfAction
 *
 * @param gs - Current GameState
 * @param action - If action
 * @param game - Game object
 *
 * @returns New GameState
 */
export function doIf(gs: GameState, action: IfAction, game: IGame): GameState {
    if (game.conditions.evaluate(gs, game, action.condition)) {
        return game.perform(gs, action.then);
    }
    else if (action.else) {
        return game.perform(gs, action.else);
    }
    else {
        return gs;
    }
}
