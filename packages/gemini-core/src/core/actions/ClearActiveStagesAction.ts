/**
 * gemini-game-engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Action } from "../../Definitions";
import { GameState } from "../../GameState";

/**
 * Type of the ClearActiveStages action
 */
export const ACTION_CLEAR_STAGES = "CLEAR_STAGES";

/**
 * Creates the ClearActiveStages action
 */
export function ClearActiveStages(): Action {
    return {
        type: ACTION_CLEAR_STAGES
    };
}

/**
 * Clears all active stages for GameState
 * @param gs - GameState
 *
 * @returns GameState with no active stage
 */
export function clearActiveStages(gs: GameState): GameState {
    return {
        ...gs,
        activeStages: []
    };
}
