/**
 * Gemini Game Engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import "jest";
import {
    ACTION_CLEAR_ACTION_RESULT, ClearLastActionResult, clearLastActionResult,
    CorePlugin, EmptyGameState,
    GameState, ResultFail, ResultSuccess
} from "../../";

describe("ClearLastActionResult", () => {

    it("creates correct action type", () => {
        const action = ClearLastActionResult();
        expect(action.type).toBe(ACTION_CLEAR_ACTION_RESULT);
    });
});

describe("clearLastActionResult", () => {

    it("clears last action result", () => {
        const gs: GameState = {
            ...EmptyGameState,
            lastActionResult: { success: false, reason: "something" }
        };

        const result = clearLastActionResult(gs);
        expect(result.lastActionResult).toEqual(ResultSuccess);
    });
});

describe("module", () => {
    it("registers the action in module", () => {
        expect(CorePlugin.actions[ACTION_CLEAR_ACTION_RESULT]).toBe(clearLastActionResult);
    });
});
