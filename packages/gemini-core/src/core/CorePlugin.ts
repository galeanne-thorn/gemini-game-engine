/**
 * @galeanne-thorn/gemini-core
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { isArray } from "lodash";
import { Action, ActionFunction, ActionGroup, ActionPerformer, IGame } from "../Definitions";
import { GamePlugin } from "../GamePlugin";
import { GameState } from "../GameState";
import { ACTION_GAME_LOADED, ACTION_GAME_LOADING, ACTION_GAME_PERFORM } from "../InternalActions";
import { ACTION_CLEAR_STAGES, clearActiveStages } from "./actions/ClearActiveStagesAction";
import { ACTION_CLEAR_ACTION_RESULT, clearLastActionResult } from "./actions/ClearLastActionResultAction";
import { ACTION_END_STAGE, endActiveStage } from "./actions/EndActiveStageAction";
import { ACTION_IF, doIf } from "./actions/IfAction";
import { ACTION_SET_STAGE, setActiveStage } from "./actions/SetActiveStageAction";
import { ACTION_SET_VARIABLE, setVariable } from "./actions/SetVariableAction";
import { and, COND_AND } from "./conditions/AndCondition";
import { COND_IS_STAGE, isStage } from "./conditions/IsStageCondition";
import { COND_OR, or } from "./conditions/OrCondition";

/**
 * Identity Action Performer
 *
 * @param gs - Game state
 * @returns The unchanged game state
 */
export const IdentityActionPerformer: ActionPerformer = gs => gs;

/**
 * Identity Action Function
 *
 * @param gs - Game state
 * @returns The unchanged game state
 */
export const IdentityActionFunction: ActionFunction = gs => gs;

/**
 * Condition that is always satisfied
 */
export const AlwaysConditionFunction = () => true;

/**
 * Gemini Game engine Core module
 */
export const CorePlugin: GamePlugin = {

    id: "core",
    title: "Gemini Game Core Module",
    author: "Galeanne Thorn",

    actions: {
        [ACTION_CLEAR_STAGES]: clearActiveStages,
        [ACTION_CLEAR_ACTION_RESULT]: clearLastActionResult,
        [ACTION_END_STAGE]: endActiveStage,
        [ACTION_SET_STAGE]: setActiveStage,
        [ACTION_SET_VARIABLE]: setVariable,
        // Programmability
        [ACTION_IF]: doIf,
        // Special "event" actions
        [ACTION_GAME_LOADED]: IdentityActionFunction,
        [ACTION_GAME_LOADING]: IdentityActionFunction,
        [ACTION_GAME_PERFORM]: IdentityActionFunction
    },

    conditions: {
        ALWAYS: AlwaysConditionFunction,
        [COND_AND]: and,
        [COND_OR]: or,
        [COND_IS_STAGE]: isStage
    },

    /**
     * Core extender
     */
    extender: original => (gs, a, g) => performAction(original(gs, a, g), a, g)
};

/**
 * Internal action function processing.
 * Performs action against game state without utilizing the extenders.
 *
 * @param gs - Game state
 * @param action - Action to perform
 * @param game - Game catalogs (actions, templates, conditions)
 *
 * @returns New game state
 * @throws Error if action is not defined in catalogs
 */
function performAction(gs: GameState, action: ActionGroup, game: IGame): GameState {
    // If actions is null or undefined, or array with not actions, return current game state
    if (!action) return gs;
    // Perform all the actions
    const newState = (isArray(action) ? action : [action])
        .reduce<GameState>((s, a) => {
            const f = game.actions.getAction(a);
            if (f) return f(s, a, game);
            throw new Error(`Action "${a.type}" is not defined`);
        },
            gs);
    return newState;
}
