/**
 * Module Gemini Game Engine Core
 *
 * Copyright © 2017 Galeanne Thorn
 * License: MIT
 */

import { Condition } from "../../Definitions";
import { GameState, getActiveStage } from "../../GameState";

/**
 * IsStage condition ID
 */
export const COND_IS_STAGE = "IsStage";

/**
 * Parameters for the IsStage condition
 */
export interface IsStageCondition extends Condition {
    /** Expected type of stage */
    stageType: string;
}

/**
 * Creates new IsStage condition
 *
 * @param stageType - Stage type
 */
export function IsStage(stageType: string): IsStageCondition {
    return {
        type: COND_IS_STAGE,
        stageType
    };
}

/**
 * Evaluates the IsStageCondition
 */
export function isStage(gs: GameState, condition: IsStageCondition): boolean {
    return (getActiveStage(gs) || { type: null }).type === condition.stageType;
}
