/**
 * Gemini Game Engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import "jest";
import {
    and, And, AndCondition, COND_AND, ConditionCatalog, CorePlugin,
    EmptyGameState, GameCatalogs, GameState
} from "../../";

describe("And", () => {
    it("creates correct condition", () => {
        const conditions = [{ type: "TEST1" }, { type: "TEST2" }];
        const result = And(conditions);
        expect(result.type).toBe(COND_AND);
        expect(result.conditions).toEqual(conditions);
    });
});

describe("and", () => {

    const conditions = new ConditionCatalog();
    conditions.add("TEST_TRUE", (gs, c, cat) => true);
    conditions.add("TEST_FALSE", (gs, c, cat) => false);

    const TrueCond = { type: "TEST_TRUE" };
    const FalseCond = { type: "TEST_FALSE" };

    const catalogs: GameCatalogs = {
        conditions,
        actions: null,
        templates: null
    };

    it("returns true if all conditions evaluate to true", () => {
        const condition = And([TrueCond, TrueCond, TrueCond]);

        const result = and(EmptyGameState, condition, catalogs);
        expect(result).toBe(true);
    });

    it("returns false if any condition evaluates to false", () => {
        const condition = And([TrueCond, FalseCond, TrueCond]);

        const result = and(EmptyGameState, condition, catalogs);
        expect(result).toBe(false);
    });
});

describe("module", () => {
    it("registers the condition in module", () => {
        expect(CorePlugin.conditions[COND_AND]).toBe(and);
    });
});
