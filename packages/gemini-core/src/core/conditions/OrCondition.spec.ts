/**
 * Gemini Game Engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import "jest";
import {
    COND_OR, ConditionCatalog, CorePlugin, EmptyGameState, GameCatalogs, GameState,
    or, Or, OrCondition
} from "../../";

describe("Or", () => {
    it("creates correct condition", () => {
        const conditions = [{ type: "TEST1" }, { type: "TEST2" }];
        const result = Or(conditions);
        expect(result.type).toBe(COND_OR);
        expect(result.conditions).toEqual(conditions);
    });
});

describe("or", () => {

    const conditions = new ConditionCatalog();
    conditions.add("TEST_TRUE", (gs, c, cat) => true);
    conditions.add("TEST_FALSE", (gs, c, cat) => false);

    const TrueCond = { type: "TEST_TRUE" };
    const FalseCond = { type: "TEST_FALSE" };

    const catalogs: GameCatalogs = {
        conditions,
        actions: null,
        templates: null
    };

    it("returns true if any condition evaluates to true", () => {
        const condition = Or([FalseCond, FalseCond, TrueCond]);

        const result = or(EmptyGameState, condition, catalogs);
        expect(result).toBe(true);
    });

    it("returns false if all conditions evaluate to false", () => {
        const condition = Or([FalseCond, FalseCond, FalseCond]);

        const result = or(EmptyGameState, condition, catalogs);
        expect(result).toBe(false);
    });
});

describe("module", () => {
    it("registers the condition in module", () => {
        expect(CorePlugin.conditions[COND_OR]).toBe(or);
    });
});
