/**
 * Gemini Game Engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import "jest";
import { COND_IS_STAGE, CorePlugin, EmptyGameState, GameState, isStage, IsStage, IsStageCondition } from "../../";

describe("IsStageCondition", () => {
    it("Creates correct condition", () => {
        const result = IsStage("someType");
        expect(result.stageType).toEqual("someType");
        expect(result.type).toBe(COND_IS_STAGE);
    });
});

describe("isStageCondition", () => {
    it("returns true if current stage is of given type", () => {
        const gs: GameState = {
            ...EmptyGameState,
            activeStages: [{ type: "T", id: "X" }]
        };
        const result = isStage(gs, IsStage("T"));
        expect(result).toBe(true);
    });

    it("returns false if there is no stage", () => {
        const result = isStage(EmptyGameState, IsStage("T"));
        expect(result).toBe(false);
    });

    it("returns false if current stage is of different type", () => {
        const gs: GameState = {
            ...EmptyGameState,
            activeStages: [{ type: "T", id: "X" }]
        };
        const result = isStage(gs, IsStage("Z"));
        expect(result).toBe(false);
    });
});

describe("module", () => {
    it("registers the condition in module", () => {
        expect(CorePlugin.conditions[COND_IS_STAGE]).toBe(isStage);
    });
});
