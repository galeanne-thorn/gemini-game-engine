/**
 * Module Gemini Game Engine Core
 *
 * Copyright © 2017 Galeanne Thorn
 * License: MIT
 */

import { Condition, GameCatalogs } from "../../Definitions";
import { GameState } from "../../GameState";

/**
 * AND condition ID
 */
export const COND_AND = "AND";

/**
 * Parameters for the AND condition
 */
export interface AndCondition extends Condition {
    /** Conditions that must all be valid */
    conditions: Condition[];
}

/**
 * Creates new AND condition.
 * The condition succeeds if all its conditions succeeds.
 * The evaluation is short-circuited, i.e. ends on first condtion evaluated to false
 *
 * @param conditions - Conditions to combine
 */
export function And(conditions: Condition[]): AndCondition {
    return {
        type: COND_AND,
        conditions
    };
}

/**
 * Evaluates the AndCondition
 * The condition succeeds if all its conditions succeeds.
 * The evaluation is short-circuited, i.e. ends on first condtion evaluated to false
 */
export function and(gs: GameState, condition: AndCondition, catalogs: GameCatalogs): boolean {
    return condition.conditions.every(c => catalogs.conditions.evaluate(gs, catalogs, c));
}
