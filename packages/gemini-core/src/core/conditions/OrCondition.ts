/**
 * Module Gemini Game Engine Core
 *
 * Copyright © 2017 Galeanne Thorn
 * License: MIT
 */

import { Condition, GameCatalogs } from "../../Definitions";
import { GameState } from "../../GameState";

/**
 * OR condition ID
 */
export const COND_OR = "OR";

/**
 * Parameters for the OR condition
 */
export interface OrCondition extends Condition {
    /** Conditions from which at least one must be be valid */
    conditions: Condition[];
}

/**
 * Creates new OR condition
 * The condition succeeds if any of its conditions succeeds.
 * The evaluation is short-circuited, i.e. ends on first condtion evaluated to true
 *
 * @param conditions - Conditions to combine
 */
export function Or(conditions: Condition[]): OrCondition {
    return {
        type: COND_OR,
        conditions
    };
}

/**
 * Evaluates the IsStageCondition
 * The condition succeeds if any of its conditions succeeds.
 * The evaluation is short-circuited, i.e. ends on first condtion evaluated to true
 */
export function or(gs: GameState, condition: OrCondition, catalogs: GameCatalogs): boolean {
    return condition.conditions.some(c => catalogs.conditions.evaluate(gs, catalogs, c));
}
