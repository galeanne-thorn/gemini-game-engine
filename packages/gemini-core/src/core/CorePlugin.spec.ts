import "jest";

import {
    ActionCatalog, AlwaysConditionFunction,
    ConditionCatalog,
    CorePlugin, EmptyGameState, IdentityActionFunction,
    IdentityActionPerformer, IGame, TemplateCatalog
} from "../";

describe("IdentityActionFunction", () => {
    it("Returns the same game state", () => {
        const gs = EmptyGameState;
        expect(IdentityActionFunction(gs, null, null)).toBe(gs);
    });
});

describe("IdentityActionPerformer", () => {
    it("Returns the same game state", () => {
        const gs = EmptyGameState;
        expect(IdentityActionPerformer(gs, null, null)).toBe(gs);
    });
});

describe("AlwaysConditionFunction", () => {
    it("Returns true", () => {
        expect(AlwaysConditionFunction()).toBe(true);
    });
});

describe("CorePlugin", () => {
    it("Registers the ALWAYS condition", () => {
        expect(CorePlugin.conditions.ALWAYS).toBe(AlwaysConditionFunction);
    });
});

describe("core action performer", () => {

    const performAction = CorePlugin.extender(gs => gs);

    const actions = new ActionCatalog();
    const catalogs: IGame = {
        actions,
        conditions: new ConditionCatalog(),
        templates: new TemplateCatalog(),
        perform: (gs, a) => null
    };

    it("throws error on unknown action", () => {
        expect(() => performAction(EmptyGameState, { type: "unknown" }, catalogs))
            .toThrowError("Action \"unknown\" is not defined");
    });

    it("performs no action", () => {
        const gs = performAction(EmptyGameState, null, catalogs);
        expect(gs).toBe(EmptyGameState);
    });

    it("performs action", () => {
        const actionFunction = (s, a) => ({ ...s, variables: { x: 1 } });
        const action = { type: "ACT" };
        actions.add(action.type, actionFunction);

        const gs = performAction(EmptyGameState, action, catalogs);

        expect(gs.variables.x).toBe(1);
    });

    it("performs multiple action", () => {
        const actionFunction1 = (s, a) => ({ ...s, variables: { x: 1 } });
        const actionFunction2 = (s, a) => ({ ...s, variables: { x: s.variables.x * 10 } });
        const action1 = { type: "ACT1" };
        const action2 = { type: "ACT2" };
        actions.add(action1.type, actionFunction1);
        actions.add(action2.type, actionFunction2);

        const gs = performAction(EmptyGameState, [action1, action2], catalogs);

        expect(gs.variables.x).toBe(10);
    });
});
