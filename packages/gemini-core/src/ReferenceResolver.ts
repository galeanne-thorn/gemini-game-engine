/**
 * @galeanne-thorn/gemini-core
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { get, isArray } from "lodash";
import { Action, Condition, GameCatalogs } from "./Definitions";
import { GameState } from "./GameState";

/**
 * Resolves textual reference to the real value.
 * Currently it does not support recursive references
 *
 * The format of the reference is <Tag><Path>
 *
 * Allowed tags are:
 * * $ - references variables in GameState.variables
 * * @ - references object state in GameState.objects
 * * # - references templates in GameCatalogs.Templates. Note path must include template type
 *
 * Path is dotted path, ie. person.id.name
 *
 * @param gs - Game state
 * @param g - Game catalogs
 * @param reference - Reference text to resolve
 */
export function resolve(gs: GameState, g: GameCatalogs, reference: string): any {
    // State reference
    if (reference.startsWith("@")) {
        return get(gs.objects, reference.slice(1));
    }
    // Variable reference
    else if (reference.startsWith("$")) {
        return get(gs.variables, reference.slice(1));
    }
    // Template reference
    else if (reference.startsWith("#")) {
        const parts = reference.slice(1).split(".");
        const template = g.templates.getTemplate(parts[0], parts[1]);
        return get(template, parts.slice(2));
    }
    return reference;
}

/**
 * Resolves references in object
 * @param data - Data to resolve
 * @returns Resolved data
 */
export function resolveReferences(gs: GameState, g: GameCatalogs, data: any): any {
    if (typeof data === "string") {
        return resolve(gs, g, data);
    }
    // For objects and arrays, iterate over properties
    if (typeof data === "object") {
        if (data === null) return data;
        if (data instanceof Array) {
            return data.map(v => resolveReferences(gs, g, v));
        }
        else {
            const result = { ...data };
            for (const key in data) {
                // Skip type, id and templateId
                if (key === "type" || key === "id" || key === "templateId") continue;
                // Resolve rest of properties
                if (data.hasOwnProperty(key)) {
                    result[key] = resolveReferences(gs, g, data[key]);
                }
            }
            return result;
        }
    }
    // Otherwise
    return data;
}
