import "jest";
import { EmptyGameState, GameState, getActiveStage } from "./";

describe("getActiveStage", () => {

    it("Should return undefined for empty game state", () => {
        const result = getActiveStage(EmptyGameState);
        expect(result).toBeUndefined();
    });

    it("Should return the top-most stage", () => {
        const gs: GameState = {
            ...EmptyGameState,
            activeStages: [
                { id: "1", type: "A" },
                { id: "1", type: "B" },
                { id: "ID", type: "expected" }
            ]
        };

        const result = getActiveStage(gs);

        expect(result.id).toBe("ID");
        expect(result.type).toBe("expected");
    });
});
