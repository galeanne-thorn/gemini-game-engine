/**
 * Gemini Game Engine
 * Copyright © 2017 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Catalog } from "./Catalog";
import { ActionCatalog } from "./catalogs/ActionCatalog";
import { ConditionCatalog } from "./catalogs/ConditionCatalog";
import { TemplateConstructor } from "./catalogs/Template";
import { TemplateCatalog } from "./catalogs/TemplateCatalog";
import {
    ActionFunction, ActionPerformerExtender, ConditionEvaluator,
    GameCatalogs, GameStateInitializer, TemplateDefinition
} from "./Definitions";

/**
 * Configures plugin when it is added to the game
 *
 * @param plugins - Catalog of all plugins registered in the game
 * @param plugins - Game object catalogs (templates, actions and conditions)
 */
export type GamePluginConfigurator = (plugins: Catalog<GamePlugin>, catalogs: GameCatalogs) => void;

/**
 * Game definition
 */
export interface GamePlugin {
    /**
     * Id of the module
     */
    id: string;

    /**
     * Title of the module
     */
    title?: string;

    /**
     * Author of the module
     */
    author?: string;

    /**
     * Configures plugin when it is added to the game.
     * This is run once per lifetime to configure internal plugin data.
     */
    configure?: GamePluginConfigurator;

    /**
     * GameState initializer for the module
     * This is called every time game starts to update the initial state of game
     */
    initializer?: GameStateInitializer;

    /**
     * Template types defined in the module
     */
    templateTypes?: Catalog<TemplateConstructor>;

    /**
     * Default templates defined in the module
     */
    templates?: Catalog<TemplateDefinition[]>;

    /**
     * Actions defined in the module
     */
    actions?: Catalog<ActionFunction>;

    /**
     * Conditions defined in the module
     */
    conditions?: Catalog<ConditionEvaluator>;

    /**
     * GameStateReducerExtender defined in the module
     */
    extender?: ActionPerformerExtender;
}

/**
 * Merges multiple game plugins into one game catalog.
 *
 * @param plugins Plugins to merge
 */
export function mergePlugins(plugins: GamePlugin[]): GameCatalogs {
    const actions = new ActionCatalog();
    const conditions = new ConditionCatalog();
    const templates = new TemplateCatalog();

    // Register types and methods
    plugins.forEach(p => {
        // Register template types, actions and conditions from the plugin
        templates.addTypes(p.templateTypes);
        actions.addRange(p.actions);
        conditions.addRange(p.conditions);
    });

    // Register templates
    plugins.forEach(p => templates.addTemplates(p.templates));

    return {
        actions,
        conditions,
        templates
    };
}
