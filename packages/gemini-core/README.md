# Gemini Game Engine - core v.2

## Installation

```
npm install @galeanne-thorn/gemini-core
```

## Compilation

```
npm install
npm test
```

## Architecture

Game consists of the following main parts
- Game State
- Game Engine
  - Extendable via Game Plugins
- Definitions
  - Templates: Define the objects the game consists from
  - Conditions: Functions used to determine the game state 
  - Actions: Functions modifying the game state

Definitions are provided by plugins. The core plugin is always present.

### Game state

Game state represents the actual state of the game. It is pure-data object with no methods.

As a simple object, it can contain any data you want, but by default it consists of:

#### Last Action Result
#### List of Active Stages
#### Catalog of game object states
#### Catalog of custom variables

### Game Loop

1. UI displays current GameState
2. User selects Actions via UI Choices
3. Actions are performed against GameState using the ActionPerformers provided by plugins
4. Repeat from 1

### Actions

### Conditions

### Templates

### Core Plugin

Core plugin provides minimal set of actions and conditions to run the game.

#### Core Plugin Action Performer

Core plugin action performer does following:
1. Converts references specified in actions to values
2. Runs the action functions against Game state