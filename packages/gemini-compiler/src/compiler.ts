/*tslint:disable:no-console*/

/**
 * @galeanne-thorn/gemini-compiler
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import * as colors from "colors";
import * as fs from "fs";
import * as gm from "gray-matter";
import * as path from "path";

// Note the gray-matter.d.ts does not define sections but it is possible option
const grayMatterOptions: gm.GrayMatterOption<string, {}> = { sections: true }; // as gm.GrayMatterOption<string, {}>;

/**
 * Compiles gray-matter style markdown files from the input folder into one json file
 *
 * @param inputFolder - Folder with input files
 * @param outputFile - Name of the output file
 */
export function compile(inputFolder: string, outputFile: string = "game.json"): number {

    if (!isValidFolder(inputFolder)) {
        return 1;
    }

    console.info(`Reading input folder ${inputFolder}`);
    const result = {};
    process(result, inputFolder);

    console.info(`Writing output file ${outputFile}`);
    fs.writeFileSync(outputFile, JSON.stringify(result, null, 2));

    console.info(`Done`);

    return 0;
}

/**
 * Validates the folder
 *
 * @param folder Path to the folder to validate
 */
function isValidFolder(folder: string): boolean {
    if (!fs.existsSync(folder)) {
        console.error(colors.red(`Folder '${folder}' does not exist`));
        return false;
    }
    const stat = fs.statSync(folder);
    if (!stat.isDirectory) {
        console.error(colors.red(`'${folder}' is not folder`));
        return false;
    }
    return true;
}

/**
 *
 * @param folder - Input folder to process
 */
function process(result: object, folder: string) {

    const dirContent = fs.readdirSync(folder, { withFileTypes: true });
    dirContent.forEach(e => {
        if (e.isDirectory()) {
            console.debug(`Processing folder ${e.name}`);
            process(result, path.join(folder, e.name)); // Recursion
        }
        else if (e.isFile() && e.name.endsWith(".md")) {
            console.debug(`Processing file ${e.name}`);
            const data = processFile(path.join(folder, e.name));
            if (isValidData(e.name, data)) {
                if (!result[data.type]) {
                    result[data.type] = [];
                }
                result[data.type].push(data);
            }
        }
        else {
            console.debug(`Skipping ${e.name}`);
        }
    });
}

/**
 * Checks whether data is valid gemini game template
 *
 * @param filename Name of the processed file
 * @param data Data to validate
 *
 * @return True if data is valid otherwise false
 */
function isValidData(filename: string, data: any): boolean {
    if (typeof data.type === "undefined") {
        console.warn(`Missing 'type' in ${filename}`);
        return false;
    }
    if (typeof data.id === "undefined") {
        console.warn(`Missing 'id' in ${filename}`);
        return false;
    }
    return true;
}

/**
 * Converts the front matter, sections and markdown into gemini game template
 * @param fileName
 */
function processFile(fileName: string): any {
    const file = gm.read(fileName, grayMatterOptions);
    const data = file.data;
    // Add UI section to data if not present
    if (!data.ui) {
        data.ui = {};
    }
    // Set markdown text to UI
    data.ui.text = file.content;
    // If there are sections, do something
    if (file.sections.length > 0) {
        // TODO: Allow input of processing by data.type
        if (!data.sections) {
            data.sections = {};
        }
        data.ui.section = {};
        file.sections.forEach(s => {
            data.ui.sections[s.key] = s.content;
            data.sections[s.key] = s.data;
        });
    }
    return data;
}
