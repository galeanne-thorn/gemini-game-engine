#!/usr/bin/env node

/**
 * @galeanne-thorn/gemini-compiler
 * Copyright © 2018 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import * as program from "commander";
import { compile } from "./compiler";

let showHelp = true;

program
    .version("1.0.0")
    .description("Compiles gray-matter style markdown files from the input folder into one json file")
    .arguments("<folder> [output]")
    .action((f, o) => {
        showHelp = false;
        process.exit(compile(f, o));
    });

program.parse(process.argv).help();

if (showHelp) {
    program.help();
}
