Gemini Game Engine - Compiler
==========================================

Compiles md files with template definitions into one JSON file

Installation
------------

```
npm install @galeanne-thorn/gemini-compiler
```

Usage
-------

See gemini-demo for example

Assuming following folder structure

```
gamePackage
+-+- game
  |-+ rooms
  | |- kitchen.md
  | \- hallway.md
  |-+ scenes

```

>gemini-compiler <input-folder> [<output.json>]